//
//  DHArthritisViewController.m
//  DoctorHelp
//
//  Created by Dmitry Venikov on 21/10/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHGlazgoViewController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "VCRadioButton.h"

@interface DHGlazgoViewController ()

@property (weak, nonatomic) IBOutlet UITextField *resultTextField;
@property (weak, nonatomic) IBOutlet UITextField *bolSustTextField;
@property (weak, nonatomic) IBOutlet UITextField *vospSustTextField;
@property (weak, nonatomic) IBOutlet UITextField *skorOsedTextField;
@property (weak, nonatomic) IBOutlet UITextField *visShkalaTextField;

@end

@implementation DHGlazgoViewController

{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Glazgo calculator"];

    
    _btn1.groupName = @"group1";
    _btn2.groupName = @"group1";
    _btn3.groupName = @"group1";
    _btn4.groupName = @"group1";
    
    _btn1.selectedValue = @"4";
    _btn2.selectedValue = @"3";
    _btn3.selectedValue = @"2";
    _btn4.selectedValue = @"1";
    
    _btn2_1.groupName = @"group2";
    _btn2_2.groupName = @"group2";
    _btn2_3.groupName = @"group2";
    _btn2_4.groupName = @"group2";
    _btn2_5.groupName = @"group2";
    
    _btn2_1.selectedValue = @"5";
    _btn2_2.selectedValue = @"4";
    _btn2_3.selectedValue = @"3";
    _btn2_4.selectedValue = @"2";
    _btn2_5.selectedValue = @"1";
    
    _btn3_1.groupName = @"group3";
    _btn3_2.groupName = @"group3";
    _btn3_3.groupName = @"group3";
    _btn3_4.groupName = @"group3";
    _btn3_5.groupName = @"group3";
    _btn3_6.groupName = @"group3";
    
    _btn3_1.selectedValue = @"6";
    _btn3_2.selectedValue = @"5";
    _btn3_3.selectedValue = @"4";
    _btn3_4.selectedValue = @"3";
    _btn3_5.selectedValue = @"2";
    _btn3_6.selectedValue = @"1";
    
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

- (void)calculateForBolsSust:(double)bolSust vospSust:(double)vospSust skorOsed:(double)skorOsed visShkala:(double)visShkala
{
    double result;
    result = 0.56 * sqrt(bolSust) + 0.28 * sqrt(vospSust) + 0.70 * log(skorOsed) + 0.014 * visShkala;
    
    self.resultTextField.text = [NSString stringWithFormat:@"%.2f", result];
}

- (IBAction)calc:(UIButton *)sender
{
    NSInteger resultValue = [_btn1.selectedValueForGroup integerValue] + [_btn2_1.selectedValueForGroup integerValue] + [_btn3_1.selectedValueForGroup integerValue];
    
    NSString *numberEnding = [self getNumberEnding:resultValue];
    _resultField.text = [NSString stringWithFormat:@"%ld %@",(long)resultValue, numberEnding];
}

-(NSString *)getNumberEnding:(NSInteger)iNumber
{
    NSString *sEnding = @"";
    NSArray *aEndings = @[@"балл",@"балла",@"баллов"];
    iNumber = iNumber % 100;
    if (iNumber>=11 && iNumber<=19) {
        sEnding=aEndings[2];
    }
    else {
        int i = iNumber % 10;
        switch (i)
        {
            case (1): sEnding = aEndings[0]; break;
            case (2):
            case (3):
            case (4): sEnding = aEndings[1]; break;
            default: sEnding = aEndings[2];
        }
    }
    return sEnding;
}

- (IBAction)clear:(UIButton *)sender {
    [self.btn1 setSelected:NO];
    [self.btn2 setSelected:NO];
    [self.btn3 setSelected:NO];
    [self.btn4 setSelected:NO];
    
    [self.btn2_1 setSelected:NO];
    [self.btn2_2 setSelected:NO];
    [self.btn2_3 setSelected:NO];
    [self.btn2_4 setSelected:NO];
    [self.btn2_5 setSelected:NO];
    
    [self.btn3_1 setSelected:NO];
    [self.btn3_2 setSelected:NO];
    [self.btn3_3 setSelected:NO];
    [self.btn3_4 setSelected:NO];
    [self.btn3_5 setSelected:NO];
    [self.btn3_6 setSelected:NO];
   
    self.btn3_1.selectionBlock = nil;
    self.bolSustTextField.text = @"";
    self.vospSustTextField.text = @"";
    self.skorOsedTextField.text = @"";
    self.visShkalaTextField.text = @"";
    self.resultTextField.text = @"";
    self.resultField.text = @"";
}

- (BOOL)showAlert
{
    DHAlert *alert = [DHAlert new];
    NSArray *array = [[NSArray alloc] initWithObjects:self.bolSustTextField.text, self.vospSustTextField.text, self.skorOsedTextField.text, self.visShkalaTextField.text, nil];
    [alert initWithViewController:self andArray:array];
    return alert.alertShown;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

@end
