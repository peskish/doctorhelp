//
//  DHPASIObject.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 25.05.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DHPASIObject : NSObject
@property NSString *name;
@property NSInteger affectedArea;
@property NSInteger itch;
@property NSInteger hyperemia;
@property NSInteger peeling;
@property NSInteger thickening;
@property BOOL needToShowParams;
+(NSArray *)setPASIObjects;

@end
