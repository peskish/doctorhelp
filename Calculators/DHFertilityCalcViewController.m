//
//  DHFertilityCalcViewController.m
//  DoctorHelp
//
//  Created by Aliona on 03/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHFertilityCalcViewController.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface DHFertilityCalcViewController ()

@property (weak, nonatomic) IBOutlet UITextField *resultTextField;

@end

@implementation DHFertilityCalcViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    _result.hidden = YES;
    _type.selectedSegmentIndex = 0;
    _cycle.selectedSegmentIndex = 0;
    _operations.selectedSegmentIndex = 0;
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"Fertility calculator"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Fertility calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

    self.mainScroll.contentSize = self.contentView.bounds.size;

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
//    [self.mainScroll layoutIfNeeded];
}

#pragma mark - TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    // Venikov added
    //move the main view, so that the keyboard does not hide it.
    if  (self.view.frame.origin.y >= 0)
    {
//        [self setViewMovedUp:YES];
    }
    
//    if (_mainScroll.frame.size.height != keyBoardView.size.height) {
//        _mainScroll.frame = keyBoardView;
//    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Venikov added
//    [self setViewMovedUp:NO];
    
//    _mainScroll.frame = fullScreenView;
}

- (void)calculateResultForAge:(double)age duration:(double)duration percent:(double)percent secondary:(int)secondary unnormalCycle:(int)unnormalCycle operations:(int)operations
{
    double value = 0.0822 + 0.0836 * percent - 0.1075 * age + 0.8971 * secondary - 0.1623 * duration - 0.9775 * operations + 0.8554 * unnormalCycle;
    double exponent = exp(value);
    double result = exponent / (1 + exponent) * 100;
    self.resultTextField.text = [NSString stringWithFormat:@"%.2f %%", result];
//    NSString *resultString = [NSString stringWithFormat:@"Вероятность зачатия в течение 36 месяцев %.2f %%", result];
//    _result.text = resultString;
//    _result.hidden = NO;
}

- (IBAction)clear:(UIButton *)sender {
    _age.text = @"";
    _duration.text = @"";
    _percent.text = @"";
    _result.text = @"";
    self.resultTextField.text = @"";
    _result.hidden = YES;
    _type.selectedSegmentIndex = 0;
    _cycle.selectedSegmentIndex = 0;
    _operations.selectedSegmentIndex = 0;
}

- (IBAction)calc:(UIButton *)sender {
    int age = [[_age.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    int duration = [[_duration.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    double percent = [[_percent.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    int secondary = (int)_type.selectedSegmentIndex;
    int unnormalCycle = (int)_cycle.selectedSegmentIndex;
    int operations = (int)_operations.selectedSegmentIndex;
    if (![self showAlert]) {
        [self calculateResultForAge:age duration:duration percent:percent secondary:secondary unnormalCycle:unnormalCycle operations:operations];
    }
}

- (BOOL)showAlert
{
    DHAlert *alert = [DHAlert new];
    NSArray *array = [[NSArray alloc] initWithObjects:_age.text, _duration.text, _percent.text, nil];
    [alert initWithViewController:self andArray:array];
    return alert.alertShown;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#define kOFFSET_FOR_KEYBOARD 40.0

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSLog(@"keyboard frame raw %@", NSStringFromCGRect(keyboardFrame));
    self.keyboardHeight = keyboardFrame.size.height;
    CGRect rect = fullScreenView;
    rect.size.height -= (self.keyboardHeight);
    
    self.mainScroll.frame = rect;
    
}

-(void)keyboardWillHide
{

    
    self.mainScroll.frame = fullScreenView;
    
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.mainScroll.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
//        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
//        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    NSLog(@"%f",rect.size.height);
    self.mainScroll.frame = rect;
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

@end
