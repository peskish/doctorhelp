//
//  DHOtpuskViewController.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 18.01.16.
//  Copyright © 2016 Ptenster. All rights reserved.
//

#import "DHOtpuskViewController.h"
#import "DHOtpuskViewModel.h"
#import "DHOtpuskSectionHeaderTableViewCell.h"
#import "DHOtpuskTableViewCell.h"

@interface DHOtpuskViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property  NSMutableArray *datesArray;
@end

@implementation DHOtpuskViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datesArray = [[NSMutableArray alloc]initWithArray:[DHOtpuskViewModel setDatesArray]];
}

//=========================-----------------
#pragma mark - TableView Delegate
//=========================-----------------

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    DHOtpuskViewModel *headerModel = [DHOtpuskViewModel new];
    switch (section) {
        case 0:
            headerModel = self.datesArray[0];
            break;
            
        default:
            break;
    }
    
    static NSString *CellIdentifier = @"otpuskSectionHeader";
    DHOtpuskSectionHeaderTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    
    [headerView setCell:headerModel];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    DHOtpuskViewModel *question = [DHOtpuskViewModel new];
    switch (section) {
        case 0:
            question = self.datesArray[0];
            break;
            
        default:
            break;
    }
    
    return question.cellHeight;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return self.datesArray.count-1;
            break;
            
        default:
            break;
    }
    
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DHOtpuskViewModel *question = [DHOtpuskViewModel new];
    switch (indexPath.section) {
        case 0:
            question = self.datesArray[indexPath.row + 1];
            break;
            
        default:
            break;
    }
    
    NSString *cellIdentifier = question.cellName;
    
    Class theClass = NSClassFromString(question.cellType);
    id cell = [[theClass alloc] init];
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell setCell:question];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DHOtpuskViewModel *question = [DHOtpuskViewModel new];
    switch (indexPath.section) {
        case 0:
            question = self.datesArray[indexPath.row];
            break;
            
        default:
            break;
    }
    
    return question.cellHeight;
}

@end
