//
//  DHPASIObject.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 25.05.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHPASIObject.h"

@implementation DHPASIObject

+(NSArray *)setPASIObjects
{
    DHPASIObject *head = [[DHPASIObject alloc]init];
    head.name = @"Голова";
    head.affectedArea = 99;
    head.itch = 0;
    head.hyperemia = 0;
    head.peeling = 0;
    head.thickening = 0;
    head.needToShowParams = NO;
    
    DHPASIObject *body = [[DHPASIObject alloc]init];
    body.name = @"Туловище";
    body.affectedArea = 99;
    body.itch = 0;
    body.hyperemia = 0;
    body.peeling = 0;
    body.thickening = 0;
    body.needToShowParams = NO;

    DHPASIObject *hands = [[DHPASIObject alloc]init];
    hands.name = @"Руки";
    hands.affectedArea = 99;
    hands.itch = 0;
    hands.hyperemia = 0;
    hands.peeling = 0;
    hands.thickening = 0;
    hands.needToShowParams = NO;

    DHPASIObject *legs = [[DHPASIObject alloc]init];
    legs.name = @"Ноги (включая ягодицы)";
    legs.affectedArea = 99;
    legs.itch = 0;
    legs.hyperemia = 0;
    legs.peeling = 0;
    legs.thickening = 0;
    legs.needToShowParams = NO;

    NSArray *PASIobjects = [[NSArray alloc]initWithObjects:head,body,hands,legs, nil];
    return PASIobjects;
}
@end
