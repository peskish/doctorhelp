//
//  DHEuroScoreCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 11.03.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHEuroScoreCell.h"


@implementation DHEuroScoreCell

-(void)setCell:(DHEuroScoreModel *)question
{
    
    RadioButtonControlSelectionBlock selectionBlock = ^(VCRadioButton *radioButton){
        if (radioButton.groupName)
        {
            NSLog(@"RadioButton from group:%@ was:%@ and has a value of:%@",
                  radioButton.groupName,
                  (radioButton.selected)? @"selected" : @"deselected",
                  radioButton.selectedValue);
            if ((radioButton.selected)&&([radioButton.selectedValue isEqualToString:@"yes"]))
            {
                self.question.answer = self.question.maxMark;
            } else {
                self.question.answer = 0;
            }
        }
        else {
            NSLog(@"RadioButton with value of:%@ was:%@",
                  radioButton.selectedValue,
                  (radioButton.selected)? @"selected" : @"deselected");
        }
    };
    
    self.yesButton.selectionBlock = selectionBlock;
    self.noButton.selectionBlock = selectionBlock;
    
    self.yesButton.selectedValue = @"yes";
    self.noButton.selectedValue = @"no";
    
    self.yesButton.groupName = question.question;
    self.noButton.groupName = question.question;
    
    self.question = question;
    
    self.questionLabel.numberOfLines = 0;
    self.questionLabel.text = question.question;
    
    self.ageField.delegate = self;
    
    if (question.help.length)
    {
        self.helpButton.hidden = NO;
    } else {
        self.helpButton.hidden = YES;
    }
    
    if  ([question.question isEqualToString:@"Пол"])
    {
        self.yesLabel.text = @"Женский";
        self.noLabel.text = @"Мужской";
    } else {
        self.yesLabel.text = @"Да";
        self.noLabel.text = @"Нет";
    }
    
    if ([question.question isEqualToString:@"Возраст"])
    {
        self.yesLabel.hidden = YES;
        self.yesButton.hidden = YES;
        self.noLabel.hidden = YES;
        self.noButton.hidden = YES;
        self.ageField.hidden = NO;
    } else {
        self.yesLabel.hidden = NO;
        self.yesButton.hidden = NO;
        self.noLabel.hidden = NO;
        self.noButton.hidden = NO;
        self.ageField.hidden = YES;
    }
    

    if (question.answer == 0)
    {
        [self.noButton setSelected:YES];
        [self.yesButton setSelected:NO];
    } else {
        [self.yesButton setSelected:YES];
        [self.noButton setSelected:NO];
    }
}
- (IBAction)helpButtonTapped:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"showEuroScoreHelp" object:nil userInfo:@{@"question":self.question}];
}
//- (IBAction)yesButtonTapped:(id)sender
//{
//    [self.noButton setSelected:NO];
//    [self.yesButton setSelected:YES];
//    self.question.answer = self.question.maxMark;
//}
//- (IBAction)noButtonTapped:(id)sender
//{
//    [self.noButton setSelected:YES];
//    [self.yesButton setSelected:NO];
//    self.question.answer = 0;
//}
- (IBAction)yesButtonValueChanged:(id)sender
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.question.answer = [self.ageField.text intValue];
}
@end
