//
//  DHBasdaiQuestionTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 29.05.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHBasdaiQuestionTableViewCell.h"
#import "Patient.h"
#import "BASDAI.h"

@implementation DHBasdaiQuestionTableViewCell

-(void)awakeFromNib
{
    self.leftButton.layer.cornerRadius = 5;
    self.rightButton.layer.cornerRadius = 5;
}

-(void)setCell:(DHBASDAIModel *)basdaiObject index:(NSIndexPath *)indexPath array:(NSArray *)questionsArray
{
    self.nameLabel.hidden = NO;
    self.answerLabel.hidden = NO;
    self.leftButton.hidden = YES;
    self.rightButton.hidden = YES;
    self.leftButton.tag = indexPath.section;
    self.rightButton.tag = indexPath.section;
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == questionsArray.count)
        {

            [self.leftButton setTitle:@"Очистить" forState:UIControlStateNormal];
            [self.rightButton setTitle:@"Рассчитать" forState:UIControlStateNormal];
            self.nameLabel.hidden = YES;
            self.answerLabel.hidden = YES;
            self.leftButton.hidden = NO;
            self.rightButton.hidden = NO;
        } else
        {
            if ([basdaiObject.question isEqualToString:@"Индекс BASDAI"])
            {
                self.nameLabel.text = basdaiObject.question;
                NSString *activityString;
                activityString = basdaiObject.answer < 4 ? @"низкая активн." : @"высокая активн.";

                self.answerLabel.text = [NSString stringWithFormat:@"%.1f %@, %@", basdaiObject.answer,[self formatMarks:(int)basdaiObject.answer],activityString];
            } else {
                self.nameLabel.text = basdaiObject.question;
                self.answerLabel.text = [NSString stringWithFormat:@"%i",(int)basdaiObject.answer];
            }
            
            
        }
    } else {
        
        Patient *patient = self.patientsArray[indexPath.section-1];
        NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]];
        
        NSArray *array = [[patient.basdai allObjects] sortedArrayUsingDescriptors:sortDescriptors];
        
            if (indexPath.row == array.count)
            {

                [self.leftButton setTitle:@"Новый пациент" forState:UIControlStateNormal];
                [self.rightButton setTitle:@"Добавить результат" forState:UIControlStateNormal];
                self.nameLabel.hidden = YES;
                self.answerLabel.hidden = YES;
                self.leftButton.hidden = NO;
                self.rightButton.hidden = NO;
            } else {
                

                BASDAI *basdaiObject = array[indexPath.row];
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"dd.MM.yyyy"];
                
                NSString *stringFromDate = [formatter stringFromDate:basdaiObject.date];
                
                self.nameLabel.text = stringFromDate;
                self.answerLabel.text = [NSString stringWithFormat:@"%.1f",[basdaiObject.ball doubleValue]];
            }
        
    }
    


}
- (IBAction)leftButtonTapped:(id)sender {
    if (self.leftButton.tag == 0)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"clearBASDAI" object:nil];
    } else {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"newPatientBASDAI" object:nil];
    }
}
- (IBAction)rightButtonTapped:(id)sender
{
    if (self.rightButton.tag == 0)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"calcBASDAI" object:nil];
    } else
    {
        NSDictionary *userInfo = [[NSDictionary alloc]initWithObjectsAndKeys:[NSString stringWithFormat:@"%i",self.rightButton.tag],@"index", nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:@"addResultBASDAI" object:nil userInfo:userInfo];
    }
}

-(NSString *)formatMarks:(NSInteger)marks
{
    NSString *sEnding;
    NSArray *aEndings = [[NSArray alloc]initWithObjects:@"балл",@"балла",@"баллов", nil];
    marks = marks % 100;
    if (marks>=11 && marks<=19) {
        sEnding=aEndings[2];
    }
    else {
        int i = marks % 10;
        switch (i)
        {
            case (1): sEnding = aEndings[0]; break;
            case (2):
            case (3):
            case (4): sEnding = aEndings[1]; break;
            default: sEnding = aEndings[2];
        }
    }
    return sEnding;
}


@end
