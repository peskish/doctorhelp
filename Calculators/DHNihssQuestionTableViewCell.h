//
//  DHNihssQuestionTableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 14.11.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHNihssModel.h"

@interface DHNihssQuestionTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *questionTitle;
@property (weak, nonatomic) IBOutlet UIButton *questionButton;
@property DHNihssModel *question;
@property (weak, nonatomic) IBOutlet UIButton *showSentenceButton;
@property (weak, nonatomic) IBOutlet UIButton *showPictureButton;

-(void)setCell:(DHNihssModel *)question;

@end
