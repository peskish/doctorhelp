//
//  DHOtpuskTableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 18.01.16.
//  Copyright © 2016 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHOtpuskViewModel.h"

@interface DHOtpuskTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;

-(void)setCell:(DHOtpuskViewModel *)question;

@end
