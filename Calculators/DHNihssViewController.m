//
//  DHNihssViewController.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 14.11.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import "DHNihssViewController.h"
#import "MONPromptView.h"
#import "UIColor+DHfromHex.h"

@interface DHNihssViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSArray *dataArray;
@property (strong, nonatomic) IBOutlet UIView *pictureView;
@property (weak, nonatomic) IBOutlet UIButton *closePictureViewButton;
@property (weak, nonatomic) IBOutlet UIView *resultView;
@property NSInteger sum;
@property (weak, nonatomic) IBOutlet UILabel *sumLabel;
@property (weak, nonatomic) IBOutlet UILabel *sumDescriptionLabel;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *hideHeaderButton;
@property (strong, nonatomic) IBOutlet UIView *sentencesView;
@property (weak, nonatomic) IBOutlet UIButton *hideSentencesViewButton;
@property (strong, nonatomic) IBOutlet UIView *wordsView;
@property (weak, nonatomic) IBOutlet UIButton *closeWordsViewButton;

@end

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@implementation DHNihssViewController
- (IBAction)hideHeaderButtonTapped:(id)sender {
    
    [[NSUserDefaults standardUserDefaults]setObject:@"YES" forKey:@"hideNIHSSHelp"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    CGRect frame = self.headerView.frame;
    frame.size.height = 145;
    self.headerView.frame = frame;
    
    self.tableView.tableHeaderView = self.headerView;
}

- (IBAction)closePictureViewButtonTapped:(id)sender
{
    [self.pictureView removeFromSuperview];
}
- (IBAction)hideSentencesViewButtonTapped:(id)sender {
    [self.sentencesView removeFromSuperview];
}
- (IBAction)closeWordsViewButtonPressed:(id)sender {
    [self.wordsView removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showHelpForQuestion:) name:@"showNihssQuestionInfo" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showSentences:) name:@"showSentences" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showWords:) name:@"showWords" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showPicture) name:@"showPicture" object:nil];
    
    self.navigationItem.title = @"Калькулятор";
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"NIHSS calculator"];
    self.hideHeaderButton.layer.cornerRadius = 4;
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"hideNIHSSHelp"])
    {
        CGRect frame = self.headerView.frame;
        frame.size.height = 143;
        self.headerView.frame = frame;
        self.tableView.tableHeaderView = self.headerView;

    }
    self.wordsView.layer.cornerRadius = 4;
    self.calcButton.layer.cornerRadius = 4;
    self.clearButton.layer.cornerRadius = 4;
    
    self.hideSentencesViewButton.layer.cornerRadius = 4;
    self.closePictureViewButton.layer.cornerRadius = 4;
    self.closeWordsViewButton.layer.cornerRadius = 4;
    self.resultView.hidden = YES;
    self.dataArray = [DHNihssModel setQuestions];
    [self.tableView reloadData];
}

-(void)showWords:(NSNotification *)notification
{
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    self.wordsView.frame = self.view.frame;
    [currentWindow addSubview:self.wordsView];
}

-(void)showSentences:(NSNotification *)notification
{
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    self.sentencesView.frame = self.view.frame;
    [currentWindow addSubview:self.sentencesView];
}

-(void)showPicture
{
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    self.pictureView.frame = self.view.frame;
    [currentWindow addSubview:self.pictureView];
}

-(void)showHelpForQuestion:(NSNotification *)notification
{
    NSDictionary *attributes = @{ kMONPromptViewAttribDismissButtonBackgroundColor: [UIColor colorwithHexString:@"4CD964" alpha:1.0f],
                                  kMONPromptViewAttribDismissButtonTextColor : [UIColor whiteColor] };
    MONPromptView *promptView = [[MONPromptView alloc] initWithTitle:@"Информация"
                                                             message:notification.object
                                                  dismissButtonTitle:@"ОК"
                                                          attributes:attributes];
    [promptView showInView:self.navigationController.view];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dataArray.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    static NSString *CellIdentifier = @"nihssHeaderCell";
    DHNihssQuestionTableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (headerView == nil){
        [NSException raise:@"headerView == nil.." format:@"No cells with matching CellIdentifier loaded from your storyboard"];
    }
    
    DHNihssModel *question = self.dataArray[section];
    
    [headerView setCell:question];
    

    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    DHNihssModel *question = self.dataArray[section];
    
    float height = [self findHeightForText:question.title havingWidth:259 andFont:[UIFont boldSystemFontOfSize:17]] + 23;
    
    if (question.additionalInfo.length > 0)
    {
        height += 30;
    }
    
    return height;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DHNihssModel *question = self.dataArray[section];
    return question.answers.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"nihssAnswerCell";
    
    DHNihssAnswerTableViewCell *cell = (DHNihssAnswerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

    DHNihssModel *question = self.dataArray[indexPath.section];
    
    BOOL chosen = NO;
    if (question.chosenAnswer == indexPath.row)
    {
        chosen = YES;
    }
    
    [cell setCell:question.answers[indexPath.row] Chosen:chosen];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DHNihssModel *question = self.dataArray[indexPath.section];
    return [self findHeightForText:question.answers[indexPath.row] havingWidth:271 andFont:[UIFont systemFontOfSize:14]]+23;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DHNihssModel *question = self.dataArray[indexPath.section];
    NSInteger oldAnswer = question.chosenAnswer;
    question.chosenAnswer = indexPath.row;

    [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:oldAnswer inSection:indexPath.section],indexPath] withRowAnimation:UITableViewRowAnimationFade];
}

- (CGFloat)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font
{
    CGFloat result = font.pointSize + 4;
    if (text)
    {
        CGSize textSize = { widthValue, CGFLOAT_MAX };       //Width and height of text area
        CGSize size;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            //iOS 7
            CGRect frame = [text boundingRectWithSize:textSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{ NSFontAttributeName:font }
                                              context:nil];
            size = CGSizeMake(frame.size.width, frame.size.height+1);
        }
        else
        {
            //iOS 6.0
            size = [text sizeWithFont:font constrainedToSize:textSize lineBreakMode:NSLineBreakByWordWrapping];
        }
        result = MAX(size.height, result); //At least one row
    }
    return result;
}

- (IBAction)clear:(id)sender
{
    for (DHNihssModel *question in self.dataArray)
    {
        question.chosenAnswer = 0;
    }
    self.resultView.hidden = YES;

    CGRect frame = self.footerView.frame;
    frame.size.height = 60;
    self.footerView.frame = frame;
    
    self.tableView.tableFooterView = self.footerView;
    
    [self.tableView reloadData];
}

- (IBAction)calc:(id)sender
{
    self.sum = 0;
    for (DHNihssModel *question in self.dataArray)
    {
        if (([question.answers[question.chosenAnswer] isEqualToString:@"ампутация, поражение сустава"])||([question.answers[question.chosenAnswer] isEqualToString:@"интубация или другой физический барьер"]))
        {
            
        } else {
            self.sum += question.chosenAnswer;
        }
    }
    self.resultView.hidden = NO;
    
    [self.sumLabel setText:[NSString stringWithFormat:@"Значение: %i",self.sum]];
    
    if (self.sum <= 2)
    {
        [self.sumDescriptionLabel setText:@"Интерпретация: прогностически значимого неврологического дефицита не выявлено"];
    } else if (self.sum <= 10)
    {
        [self.sumDescriptionLabel setText:@"Интерпретация: неврологический дефицит и тяжесть инсульта умеренно выражены, показана тромболитическая терапия, прогноз выживаемости благоприятный (выживаемость 60-70% в течение года)"];
    } else if (self.sum <= 19)
    {
        [self.sumDescriptionLabel setText:@"Интерпретация: умеренно выраженный неврологический дефицит, инсульт средней тяжести, прогноз выживаемости не определен"];
    } else if (self.sum <= 25)
    {
        [self.sumDescriptionLabel setText:@"Интерпретация: тяжелые неврологические нарушения, прогноз выживаемости неблагоприятный (выживаемость 4-16% в течение года)"];
    } else
    {
        [self.sumDescriptionLabel setText:@"Интерпретация: неврологические нарушения крайней степени тяжести, тромболитическая терапия противопоказана, прогноз выживаемости неблагоприятный (выживаемость 4-16% в течение года)"];
    }
    
    CGRect labelFrame = self.sumDescriptionLabel.frame;
    labelFrame.size.height = [self findHeightForText:self.sumDescriptionLabel.text havingWidth:self.sumDescriptionLabel.frame.size.width andFont:self.sumDescriptionLabel.font];
    self.sumDescriptionLabel.frame = labelFrame;
    
    CGRect frame = self.footerView.frame;
    frame.size.height = [self findHeightForText:self.sumDescriptionLabel.text havingWidth:self.sumDescriptionLabel.frame.size.width andFont:self.sumDescriptionLabel.font] + 85;
    self.footerView.frame = frame;
    
    self.tableView.tableFooterView = self.footerView;
}


@end
