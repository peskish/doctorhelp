//
//  DHApacheSwitcherTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import "DHApacheSwitcherTableViewCell.h"

@implementation DHApacheSwitcherTableViewCell

-(void)setCellWithQuestion:(DHApacheQuestionModel *)question
{
    self.question = question;
    
    [self.questionTitleLabel setText:question.title];
    [self.switcher setOn:question.answer.isSelected];
}

- (IBAction)valueChanged:(id)sender {
    UISwitch *switcher = sender;
    
    self.question.answer.isSelected = switcher.isOn;
    
//    if ([self.question.title isEqualToString:@"Известен pH артериальной крови?"])
//    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"updatePHQuestions" object:self.question];
//    }
}

@end
