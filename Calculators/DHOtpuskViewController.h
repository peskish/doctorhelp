//
//  DHOtpuskViewController.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 18.01.16.
//  Copyright © 2016 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"

@interface DHOtpuskViewController : DHViewController <UITableViewDataSource,UITableViewDelegate>

@end
