//
//  DHTempDisabilityViewController.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 16.01.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"
#import <EventKitUI/EventKitUI.h>

@interface DHTempDisabilityViewController : DHViewController <UITableViewDataSource,UITableViewDelegate,EKEventEditViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;

@property (weak, nonatomic) IBOutlet UILabel *divider;
@property (weak, nonatomic) IBOutlet UITableView *dateTableView;

@property UIDatePicker *datePicker;
- (IBAction)clear:(UIButton *)sender;
@end
