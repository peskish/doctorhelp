//
//  DHBASDAIViewController.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 28.05.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHBASDAIModel.h"
#import "DHViewController.h"

@interface DHBASDAIViewController : DHViewController <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;

@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;


@end
