//
//  DHApacheTextFieldTableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPFloatingPlaceholderTextField.h"

@interface DHApacheTextFieldTableViewCell : UITableViewCell <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *textField;
@property DHApacheQuestionModel *question;
-(void)setCellWithQuestion:(DHApacheQuestionModel *)question;


@end
