//
//  DHApacheAnswerModel.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 17.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DHApacheAnswerModel : NSObject

@property NSString *title;
@property NSInteger points;
@property BOOL isSelected;
@end
