//
//  DHApacheSimpleTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import "DHApacheSimpleTableViewCell.h"

@implementation DHApacheSimpleTableViewCell

-(void)setCellWithQuestion:(DHApacheQuestionModel *)question
{
    [self.questionTitleLabel setText:question.title];
    
    if (question.answer)
    {
        [self.answerTitleLabel setText:question.answer.title];
    } else {
        [self.answerTitleLabel setText:@"Нажмите для выбора значения"];
    }
}

@end
