//
//  DHOtpuskTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 18.01.16.
//  Copyright © 2016 Ptenster. All rights reserved.
//

#import "DHOtpuskTableViewCell.h"

@implementation DHOtpuskTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)setCell:(DHOtpuskViewModel *)question
{
    [self.questionLabel setText:question.text];
    [self.answerLabel setText:question.answer];
}

@end
