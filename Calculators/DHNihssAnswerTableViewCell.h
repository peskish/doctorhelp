//
//  DHNihssAnswerTableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 14.11.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHNihssModel.h"

@interface DHNihssAnswerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *chosenImageView;

- (void)setCell:(NSString *)answer Chosen:(BOOL)chosen;

@end
