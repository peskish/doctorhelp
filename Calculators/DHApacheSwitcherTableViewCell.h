//
//  DHApacheSwitcherTableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DHApacheSwitcherTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISwitch *switcher;
@property (weak, nonatomic) IBOutlet UILabel *questionTitleLabel;
@property DHApacheQuestionModel *question;

-(void)setCellWithQuestion:(DHApacheQuestionModel *)question;

@end
