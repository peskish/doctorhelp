//
//  DHIbsRiskCalcViewController.m
//  DoctorHelp
//
//  Created by Aliona on 02/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHIbsRiskCalcViewController.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface DHIbsRiskCalcViewController ()

@property (weak, nonatomic) IBOutlet UITextField *holesterinTextField;
@property (weak, nonatomic) IBOutlet UITextField *lpnpTextField;
@property (weak, nonatomic) IBOutlet UITextField *ibsTextField;

@end

@implementation DHIbsRiskCalcViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    _noRisk.selectedSegmentIndex = 0;
    _gender.selectedSegmentIndex = 0;
    _lpnpUnits.selectedSegmentIndex = 0;
    _lpvpUnits.selectedSegmentIndex = 0;
    _cholesterinumUnits.selectedSegmentIndex = 0;
    _result.hidden = YES;
    _lpnpResult.hidden = YES;
    _cholesterinumResult.hidden = YES;
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"Ibs risk calculator"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Ibs risk calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (IBAction)switchCholesterinumUnits:(UISegmentedControl *)sender {
//    if (_cholesterinumUnits.selectedSegmentIndex) {
//        _cholesterinum.placeholder = @"Общий холестерин, мкмоль/л";
//    } else {
//        _cholesterinum.placeholder = @"Общий холестерин, мг/дл";
//    }
}

- (IBAction)switchLpvpUnits:(UISegmentedControl *)sender {
//    if (_lpvpUnits.selectedSegmentIndex) {
//        _lpvp.placeholder = @"ЛПВП, мкмоль/л";
//    } else {
//        _lpvp.placeholder = @"ЛПВП, мг/дл";
//    }
}
- (IBAction)switchLpnpUnits:(UISegmentedControl *)sender {
//    if (_lpnpUnits.selectedSegmentIndex) {
//        _lpnp.placeholder = @"ЛПНП, мкмоль/л";
//    } else {
//        _lpnp.placeholder = @"ЛПНП, мг/дл";
//    }
}

-(void)calculateRiskForFemale:(BOOL)female noRisk:(BOOL)noRisk cholesterinum:(double)cholesterinum inMkm:(BOOL)cholesterinumMkm lpnp:(double)lpnp inMkm:(BOOL)lpnpMkm lpvp:(double)lpvp inMkm:(BOOL)lpvpMkm
{
    if (cholesterinumMkm) {
        cholesterinum = cholesterinum / 1000 * 386.654 / 10;
    }
    if (lpnpMkm) {
        lpnp = lpnp / 1000 * 233.2 / 10;
    }
    if (lpvpMkm) {
        lpvp = lpvp / 1000 * 387 / 10;
    }
    
    NSString *cholesterinumMeaning;
    if (cholesterinum < 200) {
        cholesterinumMeaning = @"желательный";
    } else if (cholesterinum >= 200 && cholesterinum < 239) {
        cholesterinumMeaning = @"пограничный";
    } else if (cholesterinum >= 240) {
        cholesterinumMeaning = @"высокий";
    }
    cholesterinumMeaning = [@"" stringByAppendingString:cholesterinumMeaning];
    
    NSString *lpnpMeaning;
    if(noRisk) {
        if (lpnp < 130) {
            lpnpMeaning = @"желательный";
        } else if (lpnp >= 130 && lpnp < 160) {
            lpnpMeaning = @"пограничный";
        } else if (lpnp >= 160) {
            lpnpMeaning = @"высокий";
        }
    } else {
        if (lpnp <= 100) {
            lpnpMeaning = @"желательный";
        } else if (lpnp > 100) {
            lpnpMeaning = @"высокий";
        }
    }
    lpnpMeaning = [@"" stringByAppendingString:lpnpMeaning];

    NSString *lpvpMeaning;
    if(female) {
        if (lpvp < 40) {
            lpvpMeaning = @"очень высокий";
        } else if (lpvp >= 40 && lpvp < 70) {
            lpvpMeaning = [NSString stringWithFormat: @"%.2f", ((0.0008095 * pow(lpvp, 2) - 0.136905 * lpvp) + 6.1)];
        } else if (lpvp >= 70) {
            lpvpMeaning = @"риск минимален";
        }
    } else {
        if (lpvp < 25) {
            lpvpMeaning = @"очень высокий";
        } else if (lpvp >= 25 && lpvp < 65) {
            lpvpMeaning = [NSString stringWithFormat: @"%.2f", ((0.000658 * pow(lpvp, 2) - 0.0979 * lpvp) + 4.085)];
        } else if (lpvp >= 65) {
                lpvpMeaning = @"риск минимален";
        }
    }
    lpvpMeaning = [@"" stringByAppendingString:lpvpMeaning];
    
    _result.hidden = NO;
    _lpnpResult.hidden = NO;
    _cholesterinumResult.hidden = NO;
    
    self.holesterinTextField.text = cholesterinumMeaning;
    self.lpnpTextField.text = lpnpMeaning;
    self.ibsTextField.text = lpvpMeaning;
//    _result.text = lpvpMeaning;
//    _lpnpResult.text = lpnpMeaning;
//    _cholesterinumResult.text = cholesterinumMeaning;
}

- (IBAction)calc:(UIButton *)sender {
    double cholesterinum = [[_cholesterinum.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    double lpnp = [[_lpnp.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    double lpvp = [[_lpvp.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    BOOL female = (BOOL)_gender.selectedSegmentIndex;
    BOOL noRisk = (BOOL)_noRisk.selectedSegmentIndex;
    BOOL cholesterinumMkm = (BOOL)_cholesterinumUnits.selectedSegmentIndex;
    BOOL lpnpMkm = (BOOL)_lpnpUnits.selectedSegmentIndex;
    BOOL lpvpMkm = (BOOL)_lpvpUnits.selectedSegmentIndex;
    if(![self showAlert]) {
        [self calculateRiskForFemale:female noRisk:noRisk cholesterinum:cholesterinum inMkm:cholesterinumMkm lpnp:lpnp inMkm:lpnpMkm lpvp:lpvp inMkm:lpvpMkm];
    }
}

- (IBAction)clear:(UIButton *)sender {
    _lpnp.text = @"";
    _lpvp.text = @"";
    _cholesterinum.text = @"";
    self.holesterinTextField.text = @"";
    self.lpnpTextField.text = @"";
    self.ibsTextField.text = @"";
    _noRisk.selectedSegmentIndex = 0;
    _gender.selectedSegmentIndex = 0;
    _lpnpUnits.selectedSegmentIndex = 0;
    _lpvpUnits.selectedSegmentIndex = 0;
    _cholesterinumUnits.selectedSegmentIndex = 0;
    _lpnp.placeholder = @"ЛПНП";
    _lpvp.placeholder = @"ЛПВП";
    _cholesterinum.placeholder = @"Общий холестерин";
    _result.hidden = YES;
    _lpnpResult.hidden = YES;
    _cholesterinumResult.hidden = YES;
}

- (BOOL)showAlert
{
    DHAlert *alert = [DHAlert new];
    NSArray *array = [[NSArray alloc] initWithObjects:_lpnp.text, _lpvp.text, _cholesterinum.text, nil];
    [alert initWithViewController:self andArray:array];
    return alert.alertShown;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#define kOFFSET_FOR_KEYBOARD 40.0

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSLog(@"keyboard frame raw %@", NSStringFromCGRect(keyboardFrame));
    self.keyboardHeight = keyboardFrame.size.height;
    CGRect rect = fullScreenView;
    rect.size.height -= (self.keyboardHeight);
    
    self.mainScroll.frame = rect;
    
}

-(void)keyboardWillHide
{
    self.mainScroll.frame = fullScreenView;
    
}


//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

@end
