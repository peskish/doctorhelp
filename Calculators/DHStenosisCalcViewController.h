//
//  DHStenosisCalcViewController.h
//  DoctorHelp
//
//  Created by Aliona on 03/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"
@interface DHStenosisCalcViewController : DHViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *age;
@property (weak, nonatomic) IBOutlet UITextField *duration;
@property (weak, nonatomic) IBOutlet UITextField *imt;
@property (weak, nonatomic) IBOutlet UITextField *creatininum;
@property (weak, nonatomic) IBOutlet UITextField *cholesterinum;
@property (weak, nonatomic) IBOutlet UISegmentedControl *smoking;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *vascularSymptoms;
@property (weak, nonatomic) IBOutlet UISegmentedControl *vascularNoise;
@property (weak, nonatomic) IBOutlet UISegmentedControl *medicines;
@property (weak, nonatomic) IBOutlet UILabel *result;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIButton *calcButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;

- (IBAction)calc:(UIButton *)sender;
- (IBAction)clear:(UIButton *)sender;

@end
