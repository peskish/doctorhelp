//
//  DHEuroScoreModel.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 11.03.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DHEuroScoreModel : NSObject
@property NSString* question;
@property NSInteger answer;
@property NSString* help;
@property NSInteger maxMark;
@property double beta;
+(NSArray *)fillEuroScoreModel;
@end
