//
//  DHNihssViewController.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 14.11.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"
#import "DHNihssModel.h"
#import "DHNihssQuestionTableViewCell.h"
#import "DHNihssAnswerTableViewCell.h"

@interface DHNihssViewController : DHViewController <UITableViewDataSource,UITableViewDelegate>

@end
