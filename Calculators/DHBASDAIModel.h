//
//  DHBASDAIModel.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 29.05.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DHBASDAIModel : NSObject
@property NSString *question;
@property double answer;

+(NSArray *)setBasdaiObjects;
@end
