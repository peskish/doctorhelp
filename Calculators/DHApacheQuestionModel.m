//
//  DHApacheQuestionModel.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 17.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import "DHApacheQuestionModel.h"

@implementation DHApacheQuestionModel

+(NSArray *)setQuestions
{
    NSMutableArray *blocksArray = [[NSMutableArray alloc] init];
    
    DHApacheQuestionModel* firstBlock = [[DHApacheQuestionModel alloc]init];
    firstBlock.title = @"Блок А. Состояние различных систем";
    firstBlock.cellIdentifier = @"blockNameCell";
    firstBlock.cellClass = @"DHBlockNameTableViewCell";
    firstBlock.isHidden = NO;
    
    [blocksArray addObject:firstBlock];
    
    //1.1  Возраст, лет
    DHApacheQuestionModel* ageQuestion = [[DHApacheQuestionModel alloc]init];
    ageQuestion.title = @"Возраст, лет";
    ageQuestion.cellIdentifier = @"simpleQuestionCell";
    ageQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    ageQuestion.isHidden = NO;
    
    DHApacheAnswerModel* a1 = [[DHApacheAnswerModel alloc]init];
    a1.title = @"Менее 44";
    a1.points = 0;
    
    DHApacheAnswerModel* a2 = [[DHApacheAnswerModel alloc]init];
    a2.title = @"45 – 54";
    a2.points = 2;
    
    DHApacheAnswerModel* a3 = [[DHApacheAnswerModel alloc]init];
    a3.title = @"55 – 64";
    a3.points = 3;
    
    DHApacheAnswerModel* a4 = [[DHApacheAnswerModel alloc]init];
    a4.title = @"65 – 74";
    a4.points = 5;
    
    DHApacheAnswerModel* a5 = [[DHApacheAnswerModel alloc]init];
    a5.title = @"75 и более";
    a5.points = 6;
    
    ageQuestion.answers = @[a1,a2,a3,a4,a5];
    
    [blocksArray addObject:ageQuestion];
    
    //1.2 Систолическое АД и Диастолическое АД
    DHApacheQuestionModel* sadQuestion = [[DHApacheQuestionModel alloc]init];
    sadQuestion.title = @"Систолическое АД";
    sadQuestion.cellIdentifier = @"textFieldCell";
    sadQuestion.cellClass = @"DHApacheTextFieldTableViewCell";
    sadQuestion.isHidden = NO;
    [blocksArray addObject:sadQuestion];
    
    DHApacheQuestionModel* dadQuestion = [[DHApacheQuestionModel alloc]init];
    dadQuestion.title = @"Диастолическое АД";
    dadQuestion.cellIdentifier = @"textFieldCell";
    dadQuestion.cellClass = @"DHApacheTextFieldTableViewCell";
    dadQuestion.isHidden = NO;
    [blocksArray addObject:dadQuestion];
    
    //1.3 Температура ректальная
    DHApacheQuestionModel* tempQuestion = [[DHApacheQuestionModel alloc]init];
    tempQuestion.title = @"Температура ректальная, С0";
    tempQuestion.cellIdentifier = @"simpleQuestionCell";
    tempQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    tempQuestion.isHidden = NO;
    [blocksArray addObject:tempQuestion];
    
    DHApacheAnswerModel* b1 = [[DHApacheAnswerModel alloc]init];
    b1.title = @"30 – 31,9";
    b1.points = 3;
    
    DHApacheAnswerModel* b2 = [[DHApacheAnswerModel alloc]init];
    b2.title = @"32 - 33,9";
    b2.points = 2;
    
    DHApacheAnswerModel* b3 = [[DHApacheAnswerModel alloc]init];
    b3.title = @"34 – 35,9";
    b3.points = 1;
    
    DHApacheAnswerModel* b4 = [[DHApacheAnswerModel alloc]init];
    b4.title = @"36 – 38,4";
    b4.points = 0;
    
    DHApacheAnswerModel* b5 = [[DHApacheAnswerModel alloc]init];
    b5.title = @"38,5 – 38,9";
    b5.points = 1;
    
    DHApacheAnswerModel* b6 = [[DHApacheAnswerModel alloc]init];
    b6.title = @"39 – 40,9";
    b6.points = 3;
    
    DHApacheAnswerModel* b7 = [[DHApacheAnswerModel alloc]init];
    b7.title = @"41 и выше";
    b7.points = 4;

    tempQuestion.answers = @[b1,b2,b3,b4,b5,b6,b7];
    
    //1.4 ЧСС в 1 мин
    DHApacheQuestionModel *cssQuestion = [[DHApacheQuestionModel alloc]init];
    cssQuestion.title = @"ЧСС в 1 мин";
    cssQuestion.cellIdentifier = @"simpleQuestionCell";
    cssQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    cssQuestion.isHidden = NO;
    [blocksArray addObject:cssQuestion];
    
    DHApacheAnswerModel* c1 = [[DHApacheAnswerModel alloc]init];
    c1.title = @"40 – 54";
    c1.points = 3;
    
    DHApacheAnswerModel* c2 = [[DHApacheAnswerModel alloc]init];
    c2.title = @"55 – 69";
    c2.points = 2;
    
    DHApacheAnswerModel* c3 = [[DHApacheAnswerModel alloc]init];
    c3.title = @"70 – 109";
    c3.points = 0;
    
    DHApacheAnswerModel* c4 = [[DHApacheAnswerModel alloc]init];
    c4.title = @"110 – 139";
    c4.points = 2;
    
    DHApacheAnswerModel* c5 = [[DHApacheAnswerModel alloc]init];
    c5.title = @"140 – 179";
    c5.points = 3;
    
    DHApacheAnswerModel* c6 = [[DHApacheAnswerModel alloc]init];
    c6.title = @"180 и выше";
    c6.points = 4;
    
    cssQuestion.answers = @[c1,c2,c3,c4,c5,c6];
    
    //1.5 Частота дыхания в 1 мин
    DHApacheQuestionModel *timeQuestion = [[DHApacheQuestionModel alloc]init];
    timeQuestion.title = @"Частота дыхания в 1 мин";
    timeQuestion.cellIdentifier = @"simpleQuestionCell";
    timeQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    timeQuestion.isHidden = NO;
    [blocksArray addObject:timeQuestion];
    
    DHApacheAnswerModel* d1 = [[DHApacheAnswerModel alloc]init];
    d1.title = @"6 – 9";
    d1.points = 2;
    
    DHApacheAnswerModel* d2 = [[DHApacheAnswerModel alloc]init];
    d2.title = @"10 – 11";
    d2.points = 1;
    
    DHApacheAnswerModel* d3 = [[DHApacheAnswerModel alloc]init];
    d3.title = @"12 – 24";
    d3.points = 0;
    
    DHApacheAnswerModel* d4 = [[DHApacheAnswerModel alloc]init];
    d4.title = @"25 – 34";
    d4.points = 1;
    
    DHApacheAnswerModel* d5 = [[DHApacheAnswerModel alloc]init];
    d5.title = @"35 – 49";
    d5.points = 3;
    
    DHApacheAnswerModel* d6 = [[DHApacheAnswerModel alloc]init];
    d6.title = @"более 50";
    d6.points = 4;
    
    timeQuestion.answers = @[d1,d2,d3,d4,d5,d6];

//    1.6 «Оксигенация. Градиент (А-а)О2»
//    Переключатель «Выберите единицы измерения: mm Hg –  KPa»

//    Переключатель «Выберите FI02: >=0,5 - <0,5»
    DHApacheQuestionModel *chooseFIQuestion = [DHApacheQuestionModel new];
    chooseFIQuestion.title = @"Выберите FI02:";
    chooseFIQuestion.cellIdentifier = @"segmentedControlCell";
    chooseFIQuestion.cellClass = @"DHApacheSegmentedTableViewCell";
    chooseFIQuestion.isHidden = NO;
    [blocksArray addObject:chooseFIQuestion];
    
    DHApacheAnswerModel *cf1 = [DHApacheAnswerModel new];
    cf1.title = @">=0,5";
    
    DHApacheAnswerModel *cf2 = [DHApacheAnswerModel new];
    cf2.title = @"<0,5";
    
    chooseFIQuestion.answers = @[cf1,cf2];

//    Переключатель «Выберите единицы измерения: mm Hg –  KPa»

    DHApacheQuestionModel *chooseEIQuestion = [DHApacheQuestionModel new];
    chooseEIQuestion.title = @"Выберите единицы измерения:";
    chooseEIQuestion.cellIdentifier = @"segmentedControlCell";
    chooseEIQuestion.cellClass = @"DHApacheSegmentedTableViewCell";
    chooseEIQuestion.isHidden = NO;
    [blocksArray addObject:chooseEIQuestion];
    
    DHApacheAnswerModel *ei1 = [DHApacheAnswerModel new];
    ei1.title = @"mm Hg";
    
    DHApacheAnswerModel *ei2 = [DHApacheAnswerModel new];
    ei2.title = @"KPa";
    
    chooseEIQuestion.answers = @[ei1,ei2];

//    Чекбокс «Если Вы не знаете значений градиента, можете рассчитать его здесь» (см.1.6.1)

    DHApacheQuestionModel *calcQuestion = [DHApacheQuestionModel new];
    calcQuestion.title = @"Если Вы не знаете значений градиента,\nможете рассчитать его здесь";
    calcQuestion.cellIdentifier = @"buttonCell";
    calcQuestion.cellClass = @"DHApacheButtonTableViewCell";
    calcQuestion.isHidden = NO;
    [blocksArray addObject:calcQuestion];
    
    DHApacheQuestionModel *ox1Question = [[DHApacheQuestionModel alloc]init];
    ox1Question.title = @"мм.рт.ст и >=0,5";
    ox1Question.cellIdentifier = @"simpleQuestionCell";
    ox1Question.cellClass = @"DHApacheSimpleTableViewCell";
    ox1Question.isHidden = YES;
    [blocksArray addObject:ox1Question];
    
    DHApacheAnswerModel*  e1 = [[DHApacheAnswerModel alloc]init];
     e1.title = @"<200";
     e1.points = 0;
    
    DHApacheAnswerModel*  e2 = [[DHApacheAnswerModel alloc]init];
     e2.title = @"200 - 349";
     e2.points = 2;
    
    DHApacheAnswerModel*  e3 = [[DHApacheAnswerModel alloc]init];
     e3.title = @"350 - 499";
     e3.points = 3;
    
    DHApacheAnswerModel*  e4 = [[DHApacheAnswerModel alloc]init];
     e4.title = @"500 и выше";
     e4.points = 4;
    
    ox1Question.answers = @[e1,e2,e3,e4];
    
//    Если выбраны мм.рт.ст и <0,5, то
    
    DHApacheQuestionModel *ox2Question = [[DHApacheQuestionModel alloc]init];
    ox2Question.title = @"мм.рт.ст и <0,5";
    ox2Question.cellIdentifier = @"simpleQuestionCell";
    ox2Question.cellClass = @"DHApacheSimpleTableViewCell";
    ox2Question.isHidden = YES;

    [blocksArray addObject:ox2Question];
    
    DHApacheAnswerModel* f1 = [[DHApacheAnswerModel alloc]init];
    f1.title = @"<55";
    f1.points = 4;
    
    DHApacheAnswerModel* f2 = [[DHApacheAnswerModel alloc]init];
    f2.title = @"55 - 60";
    f2.points = 3;
    
    DHApacheAnswerModel* f3 = [[DHApacheAnswerModel alloc]init];
    f3.title = @"61 - 70";
    f3.points = 1;
    
    DHApacheAnswerModel* f4 = [[DHApacheAnswerModel alloc]init];
    f4.title = @">70";
    f4.points = 0;
    
    ox2Question.answers = @[f1,f2,f3,f4];
    
//    PaO2 и >=0,5, то

    DHApacheQuestionModel *ox3Question = [[DHApacheQuestionModel alloc]init];
    ox3Question.title = @"PaO2 и >=0,5";
    ox3Question.cellIdentifier = @"simpleQuestionCell";
    ox3Question.cellClass = @"DHApacheSimpleTableViewCell";
    ox3Question.isHidden = YES;
    [blocksArray addObject:ox3Question];
    
    DHApacheAnswerModel* g1 = [[DHApacheAnswerModel alloc]init];
    g1.title = @"<26,6";
    g1.points = 0;
    
    DHApacheAnswerModel* g2 = [[DHApacheAnswerModel alloc]init];
    g2.title = @"26,6 – 46,4";
    g2.points = 2;
    
    DHApacheAnswerModel* g3 = [[DHApacheAnswerModel alloc]init];
    g3.title = @"46,5 – 66,3";
    g3.points = 3;
    
    DHApacheAnswerModel* g4 = [[DHApacheAnswerModel alloc]init];
    g4.title = @"более 66,3";
    g4.points = 4;
    
    ox3Question.answers = @[g1,g2,g3,g4];
    
    
//    PaO2 и <0,5, то
    
    DHApacheQuestionModel *ox4Question = [[DHApacheQuestionModel alloc]init];
    ox4Question.title = @"PaO2 и <0,5";
    ox4Question.cellIdentifier = @"simpleQuestionCell";
    ox4Question.cellClass = @"DHApacheSimpleTableViewCell";
    ox4Question.isHidden = YES;

    [blocksArray addObject:ox4Question];
    
    DHApacheAnswerModel* h1 = [[DHApacheAnswerModel alloc]init];
    h1.title = @"<7,3";
    h1.points = 4;
    
    DHApacheAnswerModel* h2 = [[DHApacheAnswerModel alloc]init];
    h2.title = @"7,4 – 8";
    h2.points = 3;
    
    DHApacheAnswerModel* h3 = [[DHApacheAnswerModel alloc]init];
    h3.title = @"8,1 – 9,3";
    h3.points = 1;
    
    DHApacheAnswerModel* h4 = [[DHApacheAnswerModel alloc]init];
    h4.title = @"более 9,3";
    h4.points = 0;
    
    ox4Question.answers = @[h1,h2,h3,h4];
    
//  Известен ли pH артериальной крови
    DHApacheQuestionModel *knowPHQuestion = [DHApacheQuestionModel new];
    knowPHQuestion.title = @"Известен pH артериальной крови?";
    knowPHQuestion.cellIdentifier = @"switcherCell";
    knowPHQuestion.cellClass = @"DHApacheSwitcherTableViewCell";
    [blocksArray addObject:knowPHQuestion];
    
    DHApacheAnswerModel *knowPh1 = [DHApacheAnswerModel new];
    knowPh1.title = @"0";

    knowPHQuestion.answers = @[knowPh1];
    knowPHQuestion.answer = knowPh1;
//    «HC03, ммоль/л»
    
    DHApacheQuestionModel *phQuestion = [[DHApacheQuestionModel alloc]init];
    phQuestion.title = @"HC03, ммоль/л";
    phQuestion.cellIdentifier = @"simpleQuestionCell";
    phQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    phQuestion.isHidden = NO;
    [blocksArray addObject:phQuestion];
    
    DHApacheAnswerModel* j1 = [[DHApacheAnswerModel alloc]init];
    j1.title = @"15 – 17,9";
    j1.points = 3;
    
    DHApacheAnswerModel* j2 = [[DHApacheAnswerModel alloc]init];
    j2.title = @"18 – 21,9";
    j2.points = 2;
    
    DHApacheAnswerModel* j3 = [[DHApacheAnswerModel alloc]init];
    j3.title = @"22 – 31,9";
    j3.points = 0;
    
    DHApacheAnswerModel* j4 = [[DHApacheAnswerModel alloc]init];
    j4.title = @"32 – 40,9";
    j4.points = 1;
    
    DHApacheAnswerModel* j5 = [[DHApacheAnswerModel alloc]init];
    j5.title = @"41 – 51,9";
    j5.points = 3;
    
    DHApacheAnswerModel* j6 = [[DHApacheAnswerModel alloc]init];
    j6.title = @"52 и выше";
    j6.points = 4;
    
    phQuestion.answers = @[j1,j2,j3,j4,j5,j6];
    
//    «pH артериальной крови»

    DHApacheQuestionModel *phBloodQuestion = [[DHApacheQuestionModel alloc]init];
    phBloodQuestion.title = @"pH артериальной крови";
    phBloodQuestion.cellIdentifier = @"simpleQuestionCell";
    phBloodQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    phBloodQuestion.isHidden = YES;
    [blocksArray addObject:phBloodQuestion];
    
    DHApacheAnswerModel* k1 = [[DHApacheAnswerModel alloc]init];
    k1.title = @"7,15 – 7,24";
    k1.points = 3;
    
    DHApacheAnswerModel* k2 = [[DHApacheAnswerModel alloc]init];
    k2.title = @"7,25 – 7,32";
    k2.points = 2;
    
    DHApacheAnswerModel* k3 = [[DHApacheAnswerModel alloc]init];
    k3.title = @"7,33 – 7,49";
    k3.points = 0;
    
    DHApacheAnswerModel* k4 = [[DHApacheAnswerModel alloc]init];
    k4.title = @"7,50 – 7,59";
    k4.points = 1;
    
    DHApacheAnswerModel* k5 = [[DHApacheAnswerModel alloc]init];
    k5.title = @"7,60 – 7,69";
    k5.points = 3;
    
    DHApacheAnswerModel* k6 = [[DHApacheAnswerModel alloc]init];
    k6.title = @"7,7 и выше";
    k6.points = 4;
    
    phBloodQuestion.answers = @[k1,k2,k3,k4,k5,k6];
    
//    Na сыворотки, ммоль/л

    DHApacheQuestionModel *naQuestion = [[DHApacheQuestionModel alloc]init];
    naQuestion.title = @"Na сыворотки, ммоль/л";
    naQuestion.cellIdentifier = @"simpleQuestionCell";
    naQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    naQuestion.isHidden = NO;
    [blocksArray addObject:naQuestion];
    
    DHApacheAnswerModel* l1 = [[DHApacheAnswerModel alloc]init];
    l1.title = @"110 – 119";
    l1.points = 3;
    
    DHApacheAnswerModel* l2 = [[DHApacheAnswerModel alloc]init];
    l2.title = @"120 – 129";
    l2.points = 2;
    
    DHApacheAnswerModel* l3 = [[DHApacheAnswerModel alloc]init];
    l3.title = @"130 – 150";
    l3.points = 0;
    
    DHApacheAnswerModel* l4 = [[DHApacheAnswerModel alloc]init];
    l4.title = @"151 – 155";
    l4.points = 1;
    
    DHApacheAnswerModel* l5 = [[DHApacheAnswerModel alloc]init];
    l5.title = @"156 – 160";
    l5.points = 2;
    
    DHApacheAnswerModel* l6 = [[DHApacheAnswerModel alloc]init];
    l6.title = @"161 – 179";
    l6.points = 3;
    
    DHApacheAnswerModel* l7 = [[DHApacheAnswerModel alloc]init];
    l7.title = @"180 и выше";
    l7.points = 4;
    
    naQuestion.answers = @[l1,l2,l3,l4,l5,l6,l7];
    
//    Калий сыворотки, ммоль/л

    DHApacheQuestionModel *kaQuestion = [[DHApacheQuestionModel alloc]init];
    kaQuestion.title = @"Калий сыворотки, ммоль/л";
    kaQuestion.cellIdentifier = @"simpleQuestionCell";
    kaQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    kaQuestion.isHidden = NO;
    [blocksArray addObject:kaQuestion];
    
    DHApacheAnswerModel* m1 = [[DHApacheAnswerModel alloc]init];
    m1.title = @"2,5 – 2,9";
    m1.points = 2;
    
    DHApacheAnswerModel* m2 = [[DHApacheAnswerModel alloc]init];
    m2.title = @"3,0 – 3,4";
    m2.points = 1;
    
    DHApacheAnswerModel* m3 = [[DHApacheAnswerModel alloc]init];
    m3.title = @"3,5 – 5,4";
    m3.points = 0;
    
    DHApacheAnswerModel* m4 = [[DHApacheAnswerModel alloc]init];
    m4.title = @"5,5 – 5,9";
    m4.points = 1;
    
    DHApacheAnswerModel* m5 = [[DHApacheAnswerModel alloc]init];
    m5.title = @"6,0 – 6,9";
    m5.points = 3;
    
    DHApacheAnswerModel* m6 = [[DHApacheAnswerModel alloc]init];
    m6.title = @"7,0 и выше";
    m6.points = 4;
    
    kaQuestion.answers = @[m1,m2,m3,m4,m5,m6];
    
//    Гематокрит, %

    DHApacheQuestionModel *gemQuestion = [[DHApacheQuestionModel alloc]init];
    gemQuestion.title = @"Гематокрит, %";
    gemQuestion.cellIdentifier = @"simpleQuestionCell";
    gemQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    gemQuestion.isHidden = NO;
    [blocksArray addObject:gemQuestion];
    
    DHApacheAnswerModel* n1 = [[DHApacheAnswerModel alloc]init];
    n1.title = @"20 – 29,9";
    n1.points = 2;
    
    DHApacheAnswerModel* n2 = [[DHApacheAnswerModel alloc]init];
    n2.title = @"30 – 45,5";
    n2.points = 0;
    
    DHApacheAnswerModel* n3 = [[DHApacheAnswerModel alloc]init];
    n3.title = @"46 – 49,9";
    n3.points = 1;
    
    DHApacheAnswerModel* n4 = [[DHApacheAnswerModel alloc]init];
    n4.title = @"50 – 59,9";
    n4.points = 2;
    
    DHApacheAnswerModel* n5 = [[DHApacheAnswerModel alloc]init];
    n5.title = @"60 и выше";
    n5.points = 4;
    
    gemQuestion.answers = @[n1,n2,n3,n4,n5];
    
    //    Лейкоциты, 103/мл

    DHApacheQuestionModel *leikQuestion = [[DHApacheQuestionModel alloc]init];
    leikQuestion.title = @"Лейкоциты, 10^3/мл";
    leikQuestion.cellIdentifier = @"simpleQuestionCell";
    leikQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    leikQuestion.isHidden = NO;
    [blocksArray addObject:leikQuestion];
    
    DHApacheAnswerModel* o1 = [[DHApacheAnswerModel alloc]init];
    o1.title = @"1 – 2,9";
    o1.points = 2;
    
    DHApacheAnswerModel* o2 = [[DHApacheAnswerModel alloc]init];
    o2.title = @"3 – 14,9";
    o2.points = 0;
    
    DHApacheAnswerModel* o3 = [[DHApacheAnswerModel alloc]init];
    o3.title = @"15 – 19,9";
    o3.points = 1;
    
    DHApacheAnswerModel* o4 = [[DHApacheAnswerModel alloc]init];
    o4.title = @"20 – 39,9";
    o4.points = 2;
    
    DHApacheAnswerModel* o5 = [[DHApacheAnswerModel alloc]init];
    o5.title = @"40 и выше";
    o5.points = 4;
    
    leikQuestion.answers = @[o1,o2,o3,o4,o5];
    
    DHApacheQuestionModel* secondBlock = [[DHApacheQuestionModel alloc]init];
    secondBlock.title = @"Блок B. Оценка состояния нервной системы";
    secondBlock.cellIdentifier = @"blockNameCell";
    secondBlock.cellClass = @"DHBlockNameTableViewCell";
    secondBlock.isHidden = NO;
    
    [blocksArray addObject:secondBlock];
//    2.1 «Открывание глаз»
//    ниже выпадающий список, баллы не показываем, но учитываем
    
    DHApacheQuestionModel *openEyeQuestion = [[DHApacheQuestionModel alloc]init];
    openEyeQuestion.title = @"Открывание глаз";
    openEyeQuestion.cellIdentifier = @"simpleQuestionCell";
    openEyeQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    openEyeQuestion.isHidden = NO;
    [blocksArray addObject:openEyeQuestion];
    
    DHApacheAnswerModel* p1 = [[DHApacheAnswerModel alloc]init];
    p1.title = @"Произвольное";
    p1.points = 4;
    
    DHApacheAnswerModel* p2 = [[DHApacheAnswerModel alloc]init];
    p2.title = @"На обращенную речь";
    p2.points = 3;
    
    DHApacheAnswerModel* p3 = [[DHApacheAnswerModel alloc]init];
    p3.title = @"На болевой стимул";
    p3.points = 2;
    
    DHApacheAnswerModel* p4 = [[DHApacheAnswerModel alloc]init];
    p4.title = @"Отсутствует";
    p4.points = 1;
    
    openEyeQuestion.answers = @[p1,p2,p3,p4];
    
//    «Двигательная реакция»
    
    DHApacheQuestionModel *movementQuestion = [[DHApacheQuestionModel alloc]init];
    movementQuestion.title = @"Двигательная реакция";
    movementQuestion.cellIdentifier = @"simpleQuestionCell";
    movementQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    movementQuestion.isHidden = NO;
    [blocksArray addObject:movementQuestion];
    
    DHApacheAnswerModel* r1 = [[DHApacheAnswerModel alloc]init];
    r1.title = @"Выполняет команды";
    r1.points = 6;
    
    DHApacheAnswerModel* r2 = [[DHApacheAnswerModel alloc]init];
    r2.title = @"Целенаправленная на болевой стимул";
    r2.points = 5;
    
    DHApacheAnswerModel* r3 = [[DHApacheAnswerModel alloc]init];
    r3.title = @"Ненаправленная на болевой стимул";
    r3.points = 4;
    
    DHApacheAnswerModel* r4 = [[DHApacheAnswerModel alloc]init];
    r4.title = @"Тоническое сгибание на болевой стимул";
    r4.points = 3;
    
    DHApacheAnswerModel* r5 = [[DHApacheAnswerModel alloc]init];
    r5.title = @"Тоническое разгибание на болевой стимул";
    r5.points = 2;
    
    DHApacheAnswerModel* r6 = [[DHApacheAnswerModel alloc]init];
    r6.title = @"Реакция отсутствует";
    r6.points = 1;
    
    movementQuestion.answers = @[r1,r2,r3,r4,r5,r6];
    
    DHApacheQuestionModel *inturbationQuestion = [DHApacheQuestionModel new];
    inturbationQuestion.title = @"На интурбации?";
    inturbationQuestion.cellIdentifier = @"switcherCell";
    inturbationQuestion.cellClass = @"DHApacheSwitcherTableViewCell";
    inturbationQuestion.isHidden = NO;
    [blocksArray addObject:inturbationQuestion];
    
    
    DHApacheAnswerModel *inturbationAnswer = [DHApacheAnswerModel new];
    inturbationAnswer.title = @"0";
    inturbationQuestion.answers = @[inturbationAnswer];
    inturbationQuestion.answer = inturbationAnswer;
//    2.3.1 «Без интубации»
    
    DHApacheQuestionModel *withoutInturbationQuestion = [[DHApacheQuestionModel alloc]init];
    withoutInturbationQuestion.title = @"Без интубации";
    withoutInturbationQuestion.cellIdentifier = @"simpleQuestionCell";
    withoutInturbationQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    withoutInturbationQuestion.isHidden = NO;
    [blocksArray addObject:withoutInturbationQuestion];
    
    DHApacheAnswerModel* s1 = [[DHApacheAnswerModel alloc]init];
    s1.title = @"Ориентирован, способен говорить";
    s1.points = 5;
    
    DHApacheAnswerModel* s2 = [[DHApacheAnswerModel alloc]init];
    s2.title = @"Дезориентирован, может говорить";
    s2.points = 4;
    
    DHApacheAnswerModel* s3 = [[DHApacheAnswerModel alloc]init];
    s3.title = @"Бессвязная речь";
    s3.points = 3;
    
    DHApacheAnswerModel* s4 = [[DHApacheAnswerModel alloc]init];
    s4.title = @"Нечленораздельные звуки";
    s4.points = 2;
    
    DHApacheAnswerModel* s5 = [[DHApacheAnswerModel alloc]init];
    s5.title = @"Речь отсутствует ";
    s5.points = 1;
    
    withoutInturbationQuestion.answers = @[s1,s2,s3,s4,s5];
    
//    «На интубации»

    DHApacheQuestionModel *withInturbationQuestion = [[DHApacheQuestionModel alloc]init];
    withInturbationQuestion.title = @"На интубации";
    withInturbationQuestion.cellIdentifier = @"simpleQuestionCell";
    withInturbationQuestion.cellClass = @"DHApacheSimpleTableViewCell";
    withInturbationQuestion.isHidden = YES;
    [blocksArray addObject:withInturbationQuestion];
    
    DHApacheAnswerModel* t1 = [[DHApacheAnswerModel alloc]init];
    t1.title = @"Вероятно, способен говорить";
    t1.points = 5;
    
    DHApacheAnswerModel* t2 = [[DHApacheAnswerModel alloc]init];
    t2.title = @"Сомнительная возможность говорить";
    t2.points = 3;
    
    DHApacheAnswerModel* t3 = [[DHApacheAnswerModel alloc]init];
    t3.title = @"Нет реакции";
    t3.points = 1;
    
    withInturbationQuestion.answers = @[t1,t2,t3];
    
    DHApacheQuestionModel* thirdBlock = [[DHApacheQuestionModel alloc]init];
    thirdBlock.title = @"Блок С. Хронические заболевания";
    thirdBlock.cellIdentifier = @"blockNameCell";
    thirdBlock.cellClass = @"DHBlockNameTableViewCell";
    thirdBlock.isHidden = NO;
    
    [blocksArray addObject:thirdBlock];
    
    DHApacheQuestionModel *liverQuestion = [DHApacheQuestionModel new];
    liverQuestion.title = @"Печень: подтвержденный цирроз с портальной гипертензией или эпизоды печеночной недостаточности/энцефалопатии/комы";
    liverQuestion.cellIdentifier = @"checkBoxCell";
    liverQuestion.cellClass = @"DHApacheCheckBoxTableViewCell";
    liverQuestion.isHidden = NO;
    
    DHApacheAnswerModel *liverAns = [DHApacheAnswerModel new];
    liverAns.isSelected = NO;
    
    liverQuestion.answer = liverAns;
    [blocksArray addObject:liverQuestion];
    
    
    DHApacheQuestionModel *hrtQuestion = [DHApacheQuestionModel new];
    hrtQuestion.title = @"Сердечно-сосудистая система: IV класс по Нью-Йоркской классификации";
    hrtQuestion.cellIdentifier = @"checkBoxCell";
    hrtQuestion.cellClass = @"DHApacheCheckBoxTableViewCell";
    hrtQuestion.isHidden = NO;
    
    DHApacheAnswerModel *hrtAns = [DHApacheAnswerModel new];
    hrtAns.isSelected = NO;
    
    hrtQuestion.answer = hrtAns;
    [blocksArray addObject:hrtQuestion];
    
    
    DHApacheQuestionModel *loungQuestion = [DHApacheQuestionModel new];
    loungQuestion.title = @"Легкие: хронические обструктивные/рестриктивные заболевания; документированная хроническая гипоксемия /гиперкапния /полицитемия/легочная гипертензия (>40мм рт ст); ИВЛ";
    loungQuestion.cellIdentifier = @"checkBoxCell";
    loungQuestion.cellClass = @"DHApacheCheckBoxTableViewCell";
    loungQuestion.isHidden = NO;
    
    DHApacheAnswerModel *loungAns = [DHApacheAnswerModel new];
    loungAns.isSelected = NO;
    
    loungQuestion.answer = loungAns;
    [blocksArray addObject:loungQuestion];
    
    DHApacheQuestionModel *pckQuestion = [DHApacheQuestionModel new];
    pckQuestion.title = @"Почки: хронический перитонеальный или гемодиализ";
    pckQuestion.cellIdentifier = @"checkBoxCell";
    pckQuestion.cellClass = @"DHApacheCheckBoxTableViewCell";
    pckQuestion.isHidden = NO;
    
    DHApacheAnswerModel *pckAns = [DHApacheAnswerModel new];
    pckAns.isSelected = NO;
    
    pckQuestion.answer = pckAns;
    [blocksArray addObject:pckQuestion];
    
    DHApacheQuestionModel *immQuestion = [DHApacheQuestionModel new];
    immQuestion.title = @"Иммунная система: иммуноподавляющая/ химио-/ лучевая терапия; длительный прием высоких доз стероидов; иммунодефицитные заболевания: лимфома, лейкемия, СПИД и др.";
    immQuestion.cellIdentifier = @"checkBoxCell";
    immQuestion.cellClass = @"DHApacheCheckBoxTableViewCell";
    immQuestion.isHidden = NO;
    
    DHApacheAnswerModel *immAns = [DHApacheAnswerModel new];
    immAns.isSelected = NO;
    
    immQuestion.answer = immAns;
    [blocksArray addObject:immQuestion];
    
    DHApacheQuestionModel *chooseCategory = [[DHApacheQuestionModel alloc]init];
    chooseCategory.title = @"Выберите нужную категорию";
    chooseCategory.cellIdentifier = @"simpleQuestionCell";
    chooseCategory.cellClass = @"DHApacheSimpleTableViewCell";
    chooseCategory.isHidden = NO;
    [blocksArray addObject:chooseCategory];
    
    DHApacheAnswerModel* z1 = [[DHApacheAnswerModel alloc]init];
    z1.title = @"Не оперирован";
    z1.points = 5;
    
    DHApacheAnswerModel* z2 = [[DHApacheAnswerModel alloc]init];
    z2.title = @"Неотложная операция";
    z2.points = 3;
    
    DHApacheAnswerModel* z3 = [[DHApacheAnswerModel alloc]init];
    z3.title = @"Плановая операция";
    z3.points = 1;
    
    chooseCategory.answers = @[z1,z2,z3];
    
    return blocksArray;
}

@end
