//
//  DHTireoCalcViewController.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 10.02.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHTireoCalcViewController.h"

@interface DHTireoCalcViewController ()

@end

@implementation DHTireoCalcViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Tireo calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    if ([self.resultTextView.text isEqualToString:@""])
    {
        self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, 515);
    } else {
          self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, 575);
    }
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

- (IBAction)clearButtonTapped:(id)sender
{
    [self.rightWidthField setText:@""];
    [self.rightDepthField setText:@""];
    [self.rightLengthField setText:@""];
    
    [self.leftWidthField setText:@""];
    [self.leftDepthField setText:@""];
    [self.leftLengthField setText:@""];
    
    [self.resultTextView setText:@""];
    self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, 515);
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

- (IBAction)calcButtonTapped:(id)sender {
    double rightVolume = [self.rightWidthField.text doubleValue]*[self.rightLengthField.text doubleValue]*[self.rightDepthField.text doubleValue]*0.479;
    double leftVolume = [self.leftWidthField.text doubleValue]*[self.leftLengthField.text doubleValue]*[self.leftDepthField.text doubleValue]*0.479;
    double sumVolume = rightVolume + leftVolume;
    self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, 575);
    self.mainScroll.contentSize = self.contentView.bounds.size;
    [self.resultTextView setText:[NSString stringWithFormat:@"Объем правой доли - %.3f мм3\nОбъем левой доли - %.3f мм3\nОбъем щитовидной железы - %.3f мм3", rightVolume,leftVolume,sumVolume]];
}


-(void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSLog(@"keyboard frame raw %@", NSStringFromCGRect(keyboardFrame));
    self.keyboardHeight = keyboardFrame.size.height;
    CGRect rect = fullScreenView;
    rect.size.height -= (self.keyboardHeight);
    
    self.mainScroll.frame = rect;
    
}

-(void)keyboardWillHide
{

    
    self.mainScroll.frame = fullScreenView;
    
}


@end
