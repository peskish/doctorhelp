//
//  DHBasdaiQuestionTableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 29.05.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHBASDAIModel.h"

@interface DHBasdaiQuestionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

@property NSArray *patientsArray;
-(void)setCell:(DHBASDAIModel *)basdaiObject index:(NSIndexPath *)indexPath array:(NSArray *)questionsArray;
@end
