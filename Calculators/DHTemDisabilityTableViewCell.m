//
//  DHTemDisabilityTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 16.01.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHTemDisabilityTableViewCell.h"

@implementation DHTemDisabilityTableViewCell

- (void)awakeFromNib {

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setCell
{
    if (!self.datePicker)
    {
        self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
        self.datePicker.datePickerMode = UIDatePickerModeDate;
    //    self.datePicker.hidden = YES;
        [self addSubview:self.datePicker];
    } else {
        self.datePicker.hidden = NO;
    }
}
@end
