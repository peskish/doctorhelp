//
//  DHNihssAnswerTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 14.11.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import "DHNihssAnswerTableViewCell.h"

@implementation DHNihssAnswerTableViewCell


-(void)setCell:(NSString *)answer Chosen:(BOOL)chosen
{
    [self.answerLabel setText:answer];
    if (chosen)
    {
        [self.chosenImageView setImage:[UIImage imageNamed:@"check_filled.png"]];
    } else {
        [self.chosenImageView setImage:[UIImage imageNamed:@"check.png"]];
    }
}

@end
