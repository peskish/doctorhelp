//
//  DHApacheButtonTableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DHApacheButtonTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *questionButton;


-(void)setCellWithQuestion:(DHApacheQuestionModel *)question;

@end
