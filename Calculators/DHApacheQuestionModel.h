//
//  DHApacheQuestionModel.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 17.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DHApacheAnswerModel.h"

@interface DHApacheQuestionModel : NSObject

@property NSString *title;
@property NSArray *answers;
@property DHApacheAnswerModel *answer;
@property NSString *type;
@property NSString *cellIdentifier;
@property NSString *cellClass;
@property BOOL isHidden;
+(NSArray *)setQuestions;

@end
