//
//  DHEuroScoreCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 11.03.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHEuroScoreModel.h"
#import "VCRadioButton.h"

@interface DHEuroScoreCell : UITableViewCell <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UIButton *helpButton;
@property (weak, nonatomic) IBOutlet VCRadioButton *yesButton;
@property (weak, nonatomic) IBOutlet VCRadioButton *noButton;
@property (weak, nonatomic) IBOutlet UILabel *yesLabel;
@property (weak, nonatomic) IBOutlet UILabel *noLabel;
@property (weak, nonatomic) IBOutlet UITextField *ageField;
@property DHEuroScoreModel *question;
-(void)setCell:(DHEuroScoreModel *)question;
@end
