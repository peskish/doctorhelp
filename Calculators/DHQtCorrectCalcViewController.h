//
//  DHQtCorrectCalcViewController.h
//  DoctorHelp
//
//  Created by Aliona on 02/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"

@interface DHQtCorrectCalcViewController : DHViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *qt;
@property (weak, nonatomic) IBOutlet UITextField *time;
@property (weak, nonatomic) IBOutlet UILabel *result;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *switcher;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;

- (IBAction)switchValue:(id)sender;
- (IBAction)calc:(UIButton *)sender;
- (IBAction)clear:(UIButton *)sender;

@end
