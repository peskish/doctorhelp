//
//  DHRankinViewController.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 20.05.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCRadioButton.h"
#import "DHViewController.h"

@interface DHRankinViewController : DHViewController
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;

@property (weak, nonatomic) IBOutlet VCRadioButton *btn1;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn4;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn5;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn6;

@end
