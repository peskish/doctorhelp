//
//  DHVolchOcenkaViewController.m
//  DoctorHelp
//
//  Created by Dmitry Venikov on 21/10/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHVolchOcenkaViewController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

#define IS_OS_7  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

@interface DHVolchOcenkaViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *resultTextField;
@property (strong, nonatomic) NSArray *contentsArray;
@property (strong, nonatomic) NSArray *multiplesArray;
@property (strong, nonatomic) NSMutableArray *pointsArray;
@property (strong, nonatomic) NSMutableArray *switchesArray;
@property (strong, nonatomic) NSMutableArray *descriptionArray;
@property int tableHeight;
@end

@implementation DHVolchOcenkaViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
    int hegoht;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    hegoht = 0;
    self.tableHeight = 0;
    self.navigationItem.title = @"Калькулятор";
    self.contentsArray = @[@{@"value":@"Судорожные приступы",@"description":@" (исключить метаболические, инфекционные, лекарственные причины)"},
                           @{@"value":@"Психоз",@"description":@" (исключить уремию, лекарственные причины)"},
                           @{@"value":@"Синдром органического поражения головного мозга",@"description":@" (нарушение ориентации, памяти, интеллектуальных показателей, внимания, речи, бессонница, сонливость). Исключить метаболические, инфекционные, лекарственные причины "},
                           @{@"value":@"Нарушения зрения",@"description":@" (патология сетчатки в рамках СКВ, кровоизлияния в сетчатку или сосудистую оболочку глаза, неврит зрительного нерва). Исключить гипертонию, метаболические, инфекционные, лекарственные причины"},
                           @{@"value":@"Поражение черепных нервов",@"description":@" (сенсорно-моторная нейропатия черепных нервов)"},
                           @{@"value":@"Головная боль",@"description":@" (персистирующая/часто возникающая головная боль, может быть мигренозного типа, не должна отвечать на терапию наркотическими анальгетиками)"},
                           @{@"value":@"ОНМК",@"description":@" (исключить атеросклероз как причину)"},
                           @{@"value":@"Васкулит",@"description":@" (язвы, гангрены, инфаркты ногтевого ложа, кровоизлияния, данные биопсии, ангиографии)"},
                           @{@"value":@"Артрит",@"description":@" (2 или более сустава с признаками артрита)"},
                           @{@"value":@"Миозит",@"description":@" (боль или слабость в проксимальных группах мышц, ассоциированные с повышением КФК/альдолазы изменения по данным ЭНМГ, данные биопсии)"},
                           @{@"value":@"Цилиндрурия",@"description":@""},
                           @{@"value":@"Гематурия",@"description":@" (>5 эритроцитов в п.з.). Исключить другие причины"},
                           @{@"value":@"Протеинурия",@"description":@" (>0.5 г/сутки)"},
                           @{@"value":@"Лейкоцитурия",@"description":@" (>5 лейкоцитов в п.з.). Исключить инфекцию"},
                           @{@"value":@"Сыпь",@"description":@""},
                           @{@"value":@"Облысение",@"description":@""},
                           @{@"value":@"Язвы слизистых оболочек",@"description":@" (изъязвления слизистых носа, рта)"},
                           @{@"value":@"Плеврит",@"description":@""},
                           @{@"value":@"Снижение уровня комплемента",@"description":@""},
                           @{@"value":@"Повышение АТкДНК",@"description":@" >25% (по методу Farr или в сравнении с нормой лаборатории)"},
                           @{@"value":@"Лихорадка",@"description":@" (>38°C). Исключить инфекции"},
                           @{@"value":@"Тромбоцитопения",@"description":@" (<100,000 трц/мм3)"},
                           @{@"value":@"Лейкопения",@"description":@" (<3,000 лейкоцитов/мм3). Исключить лекарственные причины"},
                           @{@"value":@"Перикардит",@"description":@""}];
    
                              self.multiplesArray = @[@8, @8, @8, @8, @8, @8, @8, @8, @4, @4, @4, @4, @4, @4, @2, @2, @2, @2, @2, @2, @2, @1, @1, @1, @2];
    self.pointsArray = [[NSMutableArray alloc] initWithObjects:@0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, nil];
    self.switchesArray = [NSMutableArray array];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"Kalium calculator"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Volch Ocenka calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

  
    [self.tableView reloadData];

    CGRect frame = self.tableView.frame;
    frame.size.height = self.tableHeight;

    self.tableView.frame = frame;
    
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

- (IBAction)calc:(UIButton *)sender
{
    float result = 0.0;
    for (int index = 0; index < [self.contentsArray count]; index++) {
        result += [self.multiplesArray[index] floatValue] * [self.pointsArray[index] floatValue];
    }
    NSString *resultText = @"";
    if (result < 3) {
        resultText = @"Нет активности";
    } else if (result < 7) {
        resultText = @"Малая активность";
    } else if (result < 13) {
        resultText = @"Умеренная активность";
    } else {
        resultText = @"Высокая активность";
    }
    self.resultTextField.text = resultText;
}

- (IBAction)clear:(UIButton *)sender
{
    for (int index = 0; index < [self.switchesArray count]; index++) {
        UISwitch *switchView = self.switchesArray[index];
        [switchView setOn:NO animated:YES];
        self.pointsArray[index] = @0;
    }
    self.resultTextField.text = @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

//    NSString *cellText = [self getStringDesaes:indexPath.row];
//    CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
//    CGSize labelSize = [cellText sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14.0] constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];

    NSDictionary* currentDeseas = self.contentsArray[indexPath.row];

    UILabel *labelToGetHeight = [[UILabel alloc]init];
    labelToGetHeight.numberOfLines = 0;
    labelToGetHeight.lineBreakMode = NSLineBreakByWordWrapping;
    labelToGetHeight.frame = CGRectMake(0, 0, 280, 999);
    
    UIFont *arialFont = [UIFont fontWithName:@"Arial-BoldMT" size:14.0];
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject: arialFont forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:currentDeseas[@"value"] attributes: arialDict];
    
    UIFont *VerdanaFont = [UIFont fontWithName:@"verdana" size:12.0];
    NSDictionary *verdanaDict = [NSDictionary dictionaryWithObject:VerdanaFont forKey:NSFontAttributeName];
    NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc]initWithString: currentDeseas[@"description"] attributes:verdanaDict];
    
    [aAttrString appendAttributedString:vAttrString];

    labelToGetHeight.attributedText = aAttrString;
    
    [labelToGetHeight sizeToFit];
//

    
    
    CGFloat height = labelToGetHeight.frame.size.height + 20;
    
    switch (indexPath.row) {
        case 2:
            height += 20;
            break;
        case 3:
            height += 20;
            break;
        default:
            break;
    }
    
    hegoht += height;
    if ([currentDeseas[@"value"] isEqualToString:@"Перикардит"])
    {
        if (!self.tableHeight)
        {
            self.tableHeight = hegoht + 5;
        }
    }
    
    return height;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contentsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *reusableString = [NSString stringWithFormat:@"OcenVolch %ld", (long)indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableString];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
        [self.switchesArray addObject:switchView];
        switchView.tag = indexPath.row;
        [switchView addTarget:self action:@selector(switchWasChangedFromEnabled:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = switchView;
        
        NSDictionary* currentDeseas = self.contentsArray[indexPath.row];
        
       // cell.textLabel.font = [UIFont systemFontOfSize:15.0];
        
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:17.0];
        
        UIFont *arialFont = [UIFont fontWithName:@"Arial-BoldMT" size:14.0];
        NSDictionary *arialDict = [NSDictionary dictionaryWithObject: arialFont forKey:NSFontAttributeName];
        NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:currentDeseas[@"value"] attributes: arialDict];
        
        UIFont *VerdanaFont = [UIFont fontWithName:@"verdana" size:12.0];
        NSDictionary *verdanaDict = [NSDictionary dictionaryWithObject:VerdanaFont forKey:NSFontAttributeName];
        NSMutableAttributedString *vAttrString = [[NSMutableAttributedString alloc]initWithString: currentDeseas[@"description"] attributes:verdanaDict];
        //[vAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:(NSMakeRange(0, 15))];
        [aAttrString appendAttributedString:vAttrString];
        cell.textLabel.attributedText = aAttrString;
        
        
    }
    return cell;
}

- (void)switchWasChangedFromEnabled:(id)sender
{
    UISwitch *switchView = sender;
    NSInteger tag = switchView.tag;
    self.pointsArray[tag] = [NSNumber numberWithBool:switchView.on];
}


-(NSString*)getStringDesaes:(int)index
{
    NSDictionary* currentDeseas = self.contentsArray[index];

    NSString *result  = [NSString stringWithFormat:@"%@%@",  currentDeseas[@"value"],currentDeseas[@"description"]];
    
    return result;
}

@end
