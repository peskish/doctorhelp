//
//  DHBASDAIModel.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 29.05.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHBASDAIModel.h"

@implementation DHBASDAIModel



+(NSArray *)setBasdaiObjects
{
    DHBASDAIModel *first = [[DHBASDAIModel alloc]init];
    first.question = @"Как бы вы расценили уровень общей слабости (утомляемости) за последнюю неделю?";
    first.answer = 0;
    
    DHBASDAIModel *second = [[DHBASDAIModel alloc]init];
    second.question = @"Как бы вы расценили степень боли в шее, спине или тазобедренных суставах за последнюю неделю?";
    second.answer = 0;
    
    DHBASDAIModel *third = [[DHBASDAIModel alloc]init];
    third.question = @"Как бы вы расценили уровень боли (или степень припухлости) в суставах (помимо шеи, спины или тазобедренных суставов) за последнюю  неделю?";
    third.answer = 0;
    
    DHBASDAIModel *fourth = [[DHBASDAIModel alloc]init];
    fourth.question = @"Как бы вы расценили степень неприятных ощущений, возникающих при дотрагивании до каких - либо областей или давлении на них за последнюю неделю?";
    fourth.answer = 0;
    
    DHBASDAIModel *fith = [[DHBASDAIModel alloc]init];
    fith.question = @"Как бы вы расценили степень выраженности утренней скованности, возникающей после пробуждения за последнюю неделю?";
    fith.answer = 0;
    
    DHBASDAIModel *sixth = [[DHBASDAIModel alloc]init];
    sixth.question = @"Как долго длится утренняя скованность, возникающая после пробуждения за последнюю неделю?";
    sixth.answer = 0;
    
    NSArray *basdaiArray = [[NSArray alloc]initWithObjects:first,second,third,fourth,fith,sixth, nil];
    
    return basdaiArray;
}
@end
