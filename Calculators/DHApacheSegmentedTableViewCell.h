//
//  DHApacheSegmentedTableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DHApacheSegmentedTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *questionTitleLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *answersSegmentedControl;
@property DHApacheQuestionModel *question;
-(void)setCellWithQuestion:(DHApacheQuestionModel *)question;

@end
