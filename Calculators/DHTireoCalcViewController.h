//
//  DHTireoCalcViewController.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 10.02.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"

@interface DHTireoCalcViewController : DHViewController
@property (weak, nonatomic) IBOutlet UITextField *rightWidthField;
@property (weak, nonatomic) IBOutlet UITextField *rightDepthField;
@property (weak, nonatomic) IBOutlet UITextField *rightLengthField;

@property (weak, nonatomic) IBOutlet UITextField *leftWidthField;
@property (weak, nonatomic) IBOutlet UITextField *leftDepthField;
@property (weak, nonatomic) IBOutlet UITextField *leftLengthField;

@property (weak, nonatomic) IBOutlet UITextView *resultTextView;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;

@end
