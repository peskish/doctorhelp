//
//  DHGRACEViewController.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 16.01.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHGRACEViewController.h"
#import "MONPromptView.h"
#import "UIColor+DHfromHex.h"

@interface DHGRACEViewController ()

@end

@implementation DHGRACEViewController
{
    CGRect fullScreenView;

}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.title = @"Калькулятор";
    
    fullScreenView = _mainScroll.frame;
    
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Grace calculator"];
    
    
    _btn1.groupName = @"group1";
    _btn2.groupName = @"group1";
    _btn3.groupName = @"group1";
    _btn4.groupName = @"group1";
    _btn5.groupName = @"group1";
    _btn6.groupName = @"group1";
    _btn7.groupName = @"group1";
    _btn8.groupName = @"group1";
    
    _btn1.selectedValue = @"0";
    _btn2.selectedValue = @"8";
    _btn3.selectedValue = @"25";
    _btn4.selectedValue = @"41";
    _btn5.selectedValue = @"58";
    _btn6.selectedValue = @"75";
    _btn7.selectedValue = @"91";
    _btn8.selectedValue = @"100";

    
    _btn2_1.groupName = @"group2";
    _btn2_2.groupName = @"group2";
    _btn2_3.groupName = @"group2";
    _btn2_4.groupName = @"group2";
    _btn2_5.groupName = @"group2";
    _btn2_6.groupName = @"group2";
    _btn2_7.groupName = @"group2";

    
    _btn2_1.selectedValue = @"0";
    _btn2_2.selectedValue = @"3";
    _btn2_3.selectedValue = @"9";
    _btn2_4.selectedValue = @"15";
    _btn2_5.selectedValue = @"24";
    _btn2_6.selectedValue = @"38";
    _btn2_7.selectedValue = @"46";

    
    _btn3_1.groupName = @"group3";
    _btn3_2.groupName = @"group3";
    _btn3_3.groupName = @"group3";
    _btn3_4.groupName = @"group3";
    _btn3_5.groupName = @"group3";
    _btn3_6.groupName = @"group3";
    _btn3_7.groupName = @"group3";

    
    _btn3_1.selectedValue = @"58";
    _btn3_2.selectedValue = @"53";
    _btn3_3.selectedValue = @"43";
    _btn3_4.selectedValue = @"34";
    _btn3_5.selectedValue = @"24";
    _btn3_6.selectedValue = @"10";
    _btn3_7.selectedValue = @"0";
    
    _btn4_1.groupName = @"group4";
    _btn4_2.groupName = @"group4";
    _btn4_3.groupName = @"group4";
    _btn4_4.groupName = @"group4";
    _btn4_5.groupName = @"group4";
    _btn4_6.groupName = @"group4";
    _btn4_7.groupName = @"group4";
    
    
    _btn4_1.selectedValue = @"1";
    _btn4_2.selectedValue = @"4";
    _btn4_3.selectedValue = @"7";
    _btn4_4.selectedValue = @"10";
    _btn4_5.selectedValue = @"13";
    _btn4_6.selectedValue = @"21";
    _btn4_7.selectedValue = @"28";
    
    _btn5_1.groupName = @"group5";
    _btn5_2.groupName = @"group5";
    _btn5_3.groupName = @"group5";
    _btn5_4.groupName = @"group5";
    
    _btn5_1.selectedValue = @"0";
    _btn5_2.selectedValue = @"20";
    _btn5_3.selectedValue = @"39";
    _btn5_4.selectedValue = @"59";

    _btn6_1.groupName = @"group6";
    _btn6_2.groupName = @"group6";
    
    _btn6_1.selectedValue = @"39";
    _btn6_2.selectedValue = @"0";
    
    _btn7_1.groupName = @"group7";
    _btn7_2.groupName = @"group7";
    
    _btn7_1.selectedValue = @"28";
    _btn7_2.selectedValue = @"0";
    
    _btn8_1.groupName = @"group8";
    _btn8_2.groupName = @"group8";
    
    _btn8_1.selectedValue = @"14";
    _btn8_2.selectedValue = @"0";


    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

- (IBAction)calc:(UIButton *)sender
{
    NSInteger resultValue = [_btn1.selectedValueForGroup integerValue] + [_btn2_1.selectedValueForGroup integerValue] + [_btn3_1.selectedValueForGroup integerValue] + [_btn4_1.selectedValueForGroup integerValue] + [_btn5_1.selectedValueForGroup integerValue] + [_btn6_1.selectedValueForGroup integerValue] + [_btn7_1.selectedValueForGroup integerValue] + [_btn8_1.selectedValueForGroup integerValue] ;
    
//    NSString *numberEnding = [self getNumberEnding:resultValue];
    NSString *riskString = @"";
    NSString *lethalString = @"";
    if (resultValue < 109) { riskString = @"Низкий риск"; lethalString = @"средняя вероятность летального исхода менее 1%";}
    else if (resultValue < 140) {riskString = @"Средний риск"; lethalString = @"средняя вероятность летального исхода 1-3%";}
    else {riskString = @"Высокий риск";lethalString = @"средняя вероятность летального исхода более 3%";}
    _resultField.text = [NSString stringWithFormat:@"%@ (количество баллов по шкале - %ld, %@)",riskString, (long)resultValue,lethalString];
    
    self.resultLabel.hidden = NO;
    self.resultLabel.text = _resultField.text;
    _resultField.hidden = YES;
}


-(NSString *)getNumberEnding:(NSInteger)iNumber
{
    NSString *sEnding = @"";
    NSArray *aEndings = @[@"балл",@"балла",@"баллов"];
    iNumber = iNumber % 100;
    if (iNumber>=11 && iNumber<=19) {
        sEnding=aEndings[2];
    }
    else {
        int i = iNumber % 10;
        switch (i)
        {
            case (1): sEnding = aEndings[0]; break;
            case (2):
            case (3):
            case (4): sEnding = aEndings[1]; break;
            default: sEnding = aEndings[2];
        }
    }
    return sEnding;
}

- (IBAction)clear:(UIButton *)sender {
    [self.btn1 setSelected:NO];
    [self.btn2 setSelected:NO];
    [self.btn3 setSelected:NO];
    [self.btn4 setSelected:NO];
    [self.btn5 setSelected:NO];
    [self.btn6 setSelected:NO];
    [self.btn7 setSelected:NO];
    [self.btn8 setSelected:NO];

    [self.btn2_1 setSelected:NO];
    [self.btn2_2 setSelected:NO];
    [self.btn2_3 setSelected:NO];
    [self.btn2_4 setSelected:NO];
    [self.btn2_5 setSelected:NO];
    [self.btn2_6 setSelected:NO];
    [self.btn2_7 setSelected:NO];
    
    [self.btn3_1 setSelected:NO];
    [self.btn3_2 setSelected:NO];
    [self.btn3_3 setSelected:NO];
    [self.btn3_4 setSelected:NO];
    [self.btn3_5 setSelected:NO];
    [self.btn3_6 setSelected:NO];
    [self.btn3_7 setSelected:NO];

    [self.btn4_1 setSelected:NO];
    [self.btn4_2 setSelected:NO];
    [self.btn4_3 setSelected:NO];
    [self.btn4_4 setSelected:NO];
    [self.btn4_5 setSelected:NO];
    [self.btn4_6 setSelected:NO];
    [self.btn4_7 setSelected:NO];
    
    [self.btn5_1 setSelected:NO];
    [self.btn5_2 setSelected:NO];
    [self.btn5_3 setSelected:NO];
    [self.btn5_4 setSelected:NO];
    
    [self.btn6_1 setSelected:NO];
    [self.btn6_2 setSelected:NO];
    
    [self.btn7_1 setSelected:NO];
    [self.btn7_2 setSelected:NO];
    
    [self.btn8_1 setSelected:NO];
    [self.btn8_2 setSelected:NO];
    
//    self.resultField.text = @"";
    self.resultLabel.text = @"";
    self.resultLabel.hidden = YES;
    
    [_resultField setText:@""];
    _resultField.hidden = NO;
}

- (IBAction)showHelp:(id)sender {
    NSString *title = @"Классификация Killip";
    NSString *message = @"Класс I – отсутствие признаков застойной сердечной недостаточности;\nКласс II – наличие хрипов в легких и/или повышенного давления в югулярных венах;\nКласс III – наличие отека легких;\nКласс IV – наличие кардиогенного шока.";
    NSString *dismissTitle = @"OK";
    NSDictionary *attributes = @{ kMONPromptViewAttribDismissButtonBackgroundColor: [UIColor colorwithHexString:@"4CD964" alpha:1.0f],
                                  kMONPromptViewAttribDismissButtonTextColor : [UIColor whiteColor] };
    MONPromptView *promptView = [[MONPromptView alloc] initWithTitle:title
                                                             message:message
                                                  dismissButtonTitle:dismissTitle
                                                          attributes:attributes];
    [promptView showInView:self.navigationController.view];
}


@end
