//
//  DHStenosisCalcViewController.m
//  DoctorHelp
//
//  Created by Aliona on 03/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHStenosisCalcViewController.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface DHStenosisCalcViewController ()

@property (weak, nonatomic) IBOutlet UITextField *resultTextField;

@end

@implementation DHStenosisCalcViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    _result.hidden = YES;
    _smoking.selectedSegmentIndex = 0;
    _gender.selectedSegmentIndex = 0;
    _medicines.selectedSegmentIndex = 0;
    _vascularNoise.selectedSegmentIndex = 0;
    _vascularSymptoms.selectedSegmentIndex = 0;
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"Stenosis calculator"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Stenosis calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

#pragma mark - TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{

}

- (void)calculateRisk:(float)cholesterinum smoking:(BOOL)smoking age:(float)age bmi:(float)bmi hypertensionDuration:(float)hypertensionDuration creatininum:(float)creatininum female:(BOOL)female vascularSymptoms:(BOOL)vascularSymptoms medicines:(BOOL)medicines vascularNoise:(BOOL)vascularNoise
{
    int value = 0;
    if (smoking) {
        if (age < 30) {
            value = 3;
        } else if (age >= 30 && age < 50) {
            value = 4;
        } else if (age >= 50 && age < 70) {
            value = 5;
        } else if (age >= 70) {
            value = 6;
        }
    } else {
        if (age < 30) {
            value = 0;
        } else if (age >= 30 && age < 40) {
            value = 1;
        } else if (age >= 40 && age < 50) {
            value = 2;
        } else if (age >= 50 && age < 60) {
            value = 3;
        } else if (age >= 60 && age < 70) {
            value = 4;
        } else if (age >= 70) {
            value = 5;
        }
    }
    
    if (female) {
        value += 2;
    }
    
    if (vascularSymptoms) {
        value += 1;
    }
    
    if (hypertensionDuration < 2) {
        value += 1;
    }
    
    if (bmi < 25) {
        value += 2;
    }
    
    if (vascularNoise) {
        value += 3;
    }
    
    if (creatininum >= 60 && creatininum < 80) {
        value += 1;
    } else if (creatininum >= 80 && creatininum < 100) { value += 2;
    } else if (creatininum >= 100 && creatininum < 150) {
        value += 3;
    } else if (creatininum >= 150 && creatininum < 200) {
        value += 6;
    } else if (creatininum == 200) {
        value += 9;
    }
    
    if (cholesterinum >= 6.5 || medicines) {
        value += 1;
    }

    NSArray *resultArray = [NSArray arrayWithObjects:@(0),@(0),@(0),@(0.5),@(1),@(2),@(3),@(5),@(8),@(12),@(18),@(25),@(38),@(50),@(62),@(72),@(82),@(88),@(92),@(95),@(97),@(98),@(99),@(99.5),@(100),@(100), nil];
    if (value < resultArray.count) {
//        NSString *result = [NSString stringWithFormat:@"Вероятность стеноза почечных артерий: %@ %%",[resultArray objectAtIndex:value]];
//        _result.text = result;
//        _result.hidden = NO;
        self.resultTextField.text = [NSString stringWithFormat:@"%@", [resultArray objectAtIndex:value]];
    }
}
- (IBAction)calc:(UIButton *)sender {
    float cholesterinum = [[_cholesterinum.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    float creatininum = [[_creatininum.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    float age = [[_age.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    float bmi = [[_imt.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    float hypertensionDuration = [[_duration.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    BOOL smoking = (BOOL)_smoking.selectedSegmentIndex;
    BOOL female = (BOOL)_gender.selectedSegmentIndex;
    BOOL medicines = (BOOL)_medicines.selectedSegmentIndex;
    BOOL vascularNoise = (BOOL)_vascularNoise.selectedSegmentIndex;
    BOOL vascularSymptoms = (BOOL)_vascularSymptoms.selectedSegmentIndex;
    if (![self showAlert]) {
        [self calculateRisk:cholesterinum smoking:smoking age:age bmi:bmi hypertensionDuration:hypertensionDuration creatininum:creatininum female:female vascularSymptoms:vascularSymptoms medicines:medicines vascularNoise:vascularNoise];
    }
}

- (IBAction)clear:(UIButton *)sender {
    _age.text = @"";
    _duration.text = @"";
    _imt.text = @"";
    _creatininum.text = @"";
    _cholesterinum.text = @"";
    _result.text = @"";
    _result.hidden = YES;
    self.resultTextField.text = @"";
    _smoking.selectedSegmentIndex = 0;
    _gender.selectedSegmentIndex = 0;
    _medicines.selectedSegmentIndex = 0;
    _vascularNoise.selectedSegmentIndex = 0;
    _vascularSymptoms.selectedSegmentIndex = 0;
}

- (BOOL)showAlert
{
    DHAlert *alert = [DHAlert new];
    NSArray *array = [[NSArray alloc] initWithObjects:_age.text, _duration.text, _cholesterinum.text, _creatininum.text, _imt.text, nil];
    [alert initWithViewController:self andArray:array];
    return alert.alertShown;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#define kOFFSET_FOR_KEYBOARD 40.0

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSLog(@"keyboard frame raw %@", NSStringFromCGRect(keyboardFrame));
    self.keyboardHeight = keyboardFrame.size.height;
    CGRect rect = fullScreenView;
    rect.size.height -= (self.keyboardHeight);
    
    self.mainScroll.frame = rect;
    
}

-(void)keyboardWillHide
{

    
    self.mainScroll.frame = fullScreenView;
    
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

@end
