//
//  DHVolchankaViewController.m
//  DoctorHelp
//
//  Created by Dmitry Venikov on 21/10/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHVolchankaViewController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "UIColor+DHfromHex.h"

@interface DHVolchankaViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *resultTextField;
@property (strong, nonatomic) NSArray *contentsArray;
@property (strong, nonatomic) NSMutableArray *pointsArray;
@property (strong, nonatomic) NSMutableArray *switchesArray;
@property (strong, nonatomic) NSArray *alertArray;

@end

@implementation DHVolchankaViewController

{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    self.contentsArray = @[@"Высыпания в скуловой области",
                           @"Дискоидные высыпания",
                           @"Фотосенсибилизация",
                           @"Язвы полости рта",
                           @"Артрит",
                           @"Серозит",
                           @"Поражение почек",
                           @"Неврологические нарушения",
                           @"Гематологические нарушения",
                           @"Иммунные нарушения",
                           @"Антинуклеарные антитела"];
    _alertArray = @[@"Фиксированная эритема, плоская или приподнимающаяся на скуловых дугах, с тенденцией к распространению на назолабиальные складки",
                    @"Эритематозные приподнятые пятна с прилегающими чешуйками и фолликулярными пробками либо атрофические рубцы, развивающиеся со временем",
                    @"Кожные высыпания в результате необычной реакции на облучение солнцем (по данным анамнеза или наблюдения врача)",
                    @"Изъязвления в полости рта или носоглоточной области, обычно безболезненные, должны наблюдаться врачом",
                    @"Артрит. Неэрозивный артрит двух и более периферических суставов, характеризующийся болезненностью, припухлостью или выпотом",
                    @"Плеврит (подтвержденные анамнестически плевральные боли или выслушиваемый шум трения плевры) либо перикардит, документированный ЭКГ или шум трения перикарда, выпот в перикарде",
                    @"Персистирующая протеинурия (выделение белка с мочой 500 мг/сут или более) или цилиндурия (эритроцитарные, зернистые, смешанные)",
                    @"Судороги, не связанные с приемом лекарств или с метаболическими нарушениями вследствие уремии, кетоацидоза, электролитного дисбаланса. Психоз, не связанный с приемом лекарств или с метаболическими нарушениями вследствие уремии, кетоацидоза, электролитного дисбаланса",
                    @"Гемолитическая анемия, лейкопения (лейкоцитов менее 4х 10'/л) при двух определениях или более. Лимфопения (лимфоцитов менее 1,5х10'/л) при 20 исследованиях и более. Тромбоцитопения (менее 100 х 10'/л), не связанная с приемом лекарств",
                    @"Положительный 1_Е-клеточный тест, антитела к ДНК и нативной ДНК в повышенных титрах, анти-5т-антитела. Ложноположительная реакция на сифилис в течение 6 месяцев по реакции иммобилизации или в тесте абсорбции флюоресцирующих антитрепонемных антител",
                    @"Повышение титра антинуклеарных антител в тесте имму- нофлюоресценции или в каком-либо другом, связанное или не связанное с лекарствами, способными вызывать лекарственную волчанку"
                    ];
    self.pointsArray = [[NSMutableArray alloc] initWithObjects:@0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, nil];
    self.switchesArray = [NSMutableArray array];
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"Kalium calculator"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Volchanka calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

- (IBAction)calc:(UIButton *)sender
{
    float result = 0.0;
    for (int index = 0; index < [self.contentsArray count]; index++) {
        result += [self.pointsArray[index] floatValue];
    }
    NSString *resultText = @"";
    if (result < 4) {
        resultText = @"Диагноз СКВ сомнителен";
    } else {
        resultText = @"Диагноз СКВ подтвержден";
    }
    self.resultTextField.text = resultText;
}

- (IBAction)clear:(UIButton *)sender
{
    for (int index = 0; index < [self.switchesArray count]; index++) {
        UISwitch *switchView = self.switchesArray[index];
        [switchView setOn:NO animated:YES];
        self.pointsArray[index] = @0;
    }
    self.resultTextField.text = @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contentsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reusableString = [NSString stringWithFormat:@"VolchCell %d", indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableString];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
        [self.switchesArray addObject:switchView];
        switchView.tag = indexPath.row;
        [switchView addTarget:self action:@selector(switchWasChangedFromEnabled:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = switchView;
        cell.textLabel.font = [UIFont systemFontOfSize:13.0];
        cell.textLabel.text = [NSString stringWithFormat:@"        %@",self.contentsArray[indexPath.row]];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button addTarget:self action:@selector(showAlert:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"?" forState:UIControlStateNormal];
        button.frame = CGRectMake(5.0, 3.0, 35.0, 40.0);
        button.tintColor = [UIColor colorwithHexString:@"4CD964" alpha:1.0f];
        button.tag = indexPath.row;
        button.backgroundColor = [UIColor clearColor];
        [cell addSubview:button];
    }
    return cell;
}

-(void)showAlert:(UIButton *)btn
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:_contentsArray[[btn tag]] message:_alertArray[[btn tag]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

- (void)switchWasChangedFromEnabled:(id)sender
{
    UISwitch *switchView = sender;
    NSInteger tag = switchView.tag;
    self.pointsArray[tag] = [NSNumber numberWithBool:switchView.on];
}

@end
