//
//  DHIbsRiskCalcViewController.h
//  DoctorHelp
//
//  Created by Aliona on 02/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"
@interface DHIbsRiskCalcViewController : DHViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *cholesterinum;
@property (weak, nonatomic) IBOutlet UITextField *lpnp;
@property (weak, nonatomic) IBOutlet UITextField *lpvp;
@property (weak, nonatomic) IBOutlet UISegmentedControl *cholesterinumUnits;
@property (weak, nonatomic) IBOutlet UISegmentedControl *lpnpUnits;
@property (weak, nonatomic) IBOutlet UISegmentedControl *lpvpUnits;
@property (weak, nonatomic) IBOutlet UISegmentedControl *noRisk;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gender;
@property (weak, nonatomic) IBOutlet UILabel *result;
@property (weak, nonatomic) IBOutlet UILabel *cholesterinumResult;
@property (weak, nonatomic) IBOutlet UILabel *lpnpResult;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIButton *calcButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;


- (IBAction)switchCholesterinumUnits:(UISegmentedControl *)sender;
- (IBAction)switchLpnpUnits:(UISegmentedControl *)sender;
- (IBAction)switchLpvpUnits:(UISegmentedControl *)sender;

- (IBAction)calc:(UIButton *)sender;
- (IBAction)clear:(UIButton *)sender;

@end
