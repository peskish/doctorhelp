//
//  DHOtpuskSectionHeaderTableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 18.01.16.
//  Copyright © 2016 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHOtpuskViewModel.h"

@interface DHOtpuskSectionHeaderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *headerTitleLabel;
-(void)setCell:(DHOtpuskViewModel *)headerModel;

@end
