//
//  DHFertilityCalcViewController.h
//  DoctorHelp
//
//  Created by Aliona on 03/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"
@interface DHFertilityCalcViewController : DHViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *age;
@property (weak, nonatomic) IBOutlet UITextField *duration;
@property (weak, nonatomic) IBOutlet UITextField *percent;
@property (weak, nonatomic) IBOutlet UISegmentedControl *type;
@property (weak, nonatomic) IBOutlet UISegmentedControl *cycle;
@property (weak, nonatomic) IBOutlet UISegmentedControl *operations;
@property (weak, nonatomic) IBOutlet UILabel *result;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;

- (IBAction)clear:(UIButton *)sender;
- (IBAction)calc:(UIButton *)sender;

@end
