//
//  DHApacheViewController.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import "DHApacheViewController.h"
#import "DHApacheButtonTableViewCell.h"
#import "DHApacheSegmentedTableViewCell.h"
#import "DHApacheSwitcherTableViewCell.h"
#import "DHApacheSimpleTableViewCell.h"
#import "RPFloatingPlaceholderTextField.h"
#import "UIColor+DHfromHex.h"

@interface DHApacheViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSArray *questionsArray;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property DHApacheQuestionModel *selectedQuestion;

@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (weak, nonatomic) IBOutlet UILabel *gradientResultLabel;
@property (weak, nonatomic) IBOutlet UILabel *gradientSegmentLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gradientSegmentedControl;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *gradientPaO2Field;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *gradientPaCO2Field;
@property (weak, nonatomic) IBOutlet RPFloatingPlaceholderTextField *gradientFIO2Field;
@property (weak, nonatomic) IBOutlet UIButton *gradientCalcButton;
@property (weak, nonatomic) IBOutlet UIButton *gradientClearButton;

@property (weak, nonatomic) IBOutlet UILabel *apacheLabel;
@property (weak, nonatomic) IBOutlet UILabel *deathLabel;
@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;

@end

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@implementation DHApacheViewController

- (void)viewDidLoad {
    
    self.clearButton.layer.cornerRadius = 4;
    self.calcButton.layer.cornerRadius = 4;
    
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateGradient) name:@"updateGradient" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updatePHQuestions:) name:@"updatePHQuestions" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showGradient) name:@"showGradient" object:nil];
    
    self.questionsArray = [DHApacheQuestionModel setQuestions];
    [self.tableView reloadData];
    self.picker.hidden = YES;
    
    self.gradientPaO2Field.floatingLabelActiveTextColor = [UIColor colorwithHexString:@"4CD964" alpha:1.0f];
    self.gradientPaO2Field.floatingLabelInactiveTextColor = [UIColor lightGrayColor];
    self.gradientPaO2Field.defaultPlaceholderColor = [UIColor lightGrayColor];
    
    self.gradientPaCO2Field.floatingLabelActiveTextColor = [UIColor colorwithHexString:@"4CD964" alpha:1.0f];
    self.gradientPaCO2Field.floatingLabelInactiveTextColor = [UIColor lightGrayColor];
    self.gradientPaCO2Field.defaultPlaceholderColor = [UIColor lightGrayColor];
    
    self.gradientFIO2Field.floatingLabelActiveTextColor = [UIColor colorwithHexString:@"4CD964" alpha:1.0f];
    self.gradientFIO2Field.floatingLabelInactiveTextColor = [UIColor lightGrayColor];
    self.gradientFIO2Field.defaultPlaceholderColor = [UIColor lightGrayColor];
    
    [self clearFooter];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissGradientView)];
    
    [self.gradientView addGestureRecognizer:tap];
}

-(void)dismissGradientView
{
    [self.gradientView removeFromSuperview];
}

- (void)clearFooter
{
    self.deathLabel.hidden = YES;
    self.apacheLabel.hidden = YES;
    
    CGRect frame = self.footerView.frame;
    frame.size.height = 60;
    self.footerView.frame = frame;
    
    self.tableView.tableFooterView = self.footerView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


-(void)showPicker
{
    CGRect frame = self.picker.frame;
    frame.origin.y = self.view.frame.size.height;
    self.picker.frame = frame;
    
    self.picker.hidden = NO;
    CGRect frame2 = self.contentView.frame;
    frame2.size.height = self.view.frame.size.height - self.picker.frame.size.height;
    
    frame.origin.y = self.view.frame.size.height-self.picker.frame.size.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.contentView.frame = frame2;
        self.picker.frame = frame;
    }];
}

-(void)hidePicker
{
    CGRect frame = self.picker.frame;
    frame.origin.y = self.view.frame.size.height;
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.picker.frame = frame;
        self.contentView.frame = self.view.frame;
    } completion:^(BOOL finished) {
        self.picker.hidden = YES;

    }];
}

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSLog(@"keyboard frame raw %@", NSStringFromCGRect(keyboardFrame));

    CGRect frame = self.gradientView.frame;
    frame.size.height = self.view.frame.size.height - keyboardFrame.size.height;
    
    [UIView animateWithDuration:0.28 animations:^{
        self.gradientView.frame = frame;
    }];
}

-(void)keyboardWillHide
{
    [UIView animateWithDuration:0.28 animations:^{
        self.gradientView.frame = self.view.frame;
    }];
}


- (IBAction)gradientCalcButtonTapped:(id)sender
{
    float gradientValue = 0;
    if ([self.gradientCalcButton.titleLabel.text isEqualToString:@"Рассчитать"])
    {
        if (self.gradientSegmentedControl.selectedSegmentIndex)
        {
            gradientValue = 713*[self.gradientFIO2Field.text floatValue] - [self.gradientPaCO2Field.text floatValue] - [self.gradientPaO2Field.text floatValue];
        } else {
            gradientValue = 93*[self.gradientFIO2Field.text floatValue] - [self.gradientPaCO2Field.text floatValue] - [self.gradientPaO2Field.text floatValue];
        }
        
        [self.gradientResultLabel setText:[NSString stringWithFormat:@"Градиент (А-а)О2 - %f",gradientValue ]];
        [self.gradientCalcButton setTitle:@"Закрыть" forState:UIControlStateNormal];
        
        self.gradientFIO2Field.text = @"";
        self.gradientPaCO2Field.text = @"";
        self.gradientPaO2Field.text = @"";
        
        self.gradientResultLabel.hidden = NO;
        self.gradientFIO2Field.hidden = YES;
        self.gradientPaCO2Field.hidden = YES;
        self.gradientPaO2Field.hidden = YES;
        self.gradientSegmentedControl.hidden = YES;
        self.gradientSegmentLabel.hidden = YES;
        self.gradientClearButton.hidden = YES;

    } else {
        [self.gradientCalcButton setTitle:@"Рассчитать" forState:UIControlStateNormal];

        self.gradientResultLabel.hidden = YES;
        self.gradientFIO2Field.hidden = NO;
        self.gradientPaCO2Field.hidden = NO;
        self.gradientPaO2Field.hidden = NO;
        self.gradientSegmentedControl.hidden = NO;
        self.gradientSegmentLabel.hidden = NO;
        self.gradientClearButton.hidden = NO;
        [self.gradientView removeFromSuperview];
    }
}
- (IBAction)gradientClearButtonTapped:(id)sender {
    self.gradientFIO2Field.text = @"";
    self.gradientPaCO2Field.text = @"";
    self.gradientPaO2Field.text = @"";
}

-(void)updateGradient
{
    DHApacheQuestionModel *ansFIO2 = [self.questionsArray objectAtIndex:7];
    DHApacheQuestionModel *ansMetrics = [self.questionsArray objectAtIndex:8];
    
    if ((ansFIO2.answer)&&(ansMetrics.answer))
    {
        BOOL ans1 = ansFIO2.answer.isSelected;
        BOOL ans2 = ansMetrics.answer.isSelected;
        DHApacheQuestionModel *quest8 = [self.questionsArray objectAtIndex:10];
        DHApacheQuestionModel *quest9= [self.questionsArray objectAtIndex:11];
        DHApacheQuestionModel *quest10 = [self.questionsArray objectAtIndex:12];
        DHApacheQuestionModel *quest11 = [self.questionsArray objectAtIndex:13];

        quest8.isHidden = YES;
        quest9.isHidden = YES;
        quest10.isHidden = YES;
        quest11.isHidden = YES;
        
        if ((ans1)&&(ans2))
        {
            quest11.isHidden = NO;
        } else if ((ans1)&&(!ans2)) {
            quest9.isHidden = NO;
        } else if ((!ans1)&&(ans2)) {
            quest10.isHidden = NO;
        } else if ((!ans1)&&(!ans2)) {
            quest8.isHidden = NO;
        }
        [self.tableView reloadData];
    }
    
}

-(void)updatePHQuestions:(NSNotification *)notification
{
    DHApacheQuestionModel *quest = notification.object;
    

    DHApacheQuestionModel *question = [self.questionsArray objectAtIndex:[self.questionsArray indexOfObject:quest]+1];
    DHApacheQuestionModel *nextQuestion = [self.questionsArray objectAtIndex:[self.questionsArray indexOfObject:quest]+2];
    question.isHidden = YES;
    nextQuestion.isHidden = YES;
    
    if (quest.answer.isSelected)
    {
        nextQuestion.isHidden = NO;
    } else {
        question.isHidden = NO;
    }
    
    [self.tableView reloadData];
}

-(void)showGradient
{
    UIWindow* currentWindow = [UIApplication sharedApplication].keyWindow;
    self.gradientView.frame = self.view.frame;
    [currentWindow addSubview:self.gradientView];
}

- (IBAction)calc:(id)sender
{
    int aSum = 0;
    float sad = 0;
    float dad = 0;
    
    bool cont = YES;
    
    int i = 0;
    while (cont) {
        DHApacheQuestionModel *quest = self.questionsArray[i];
        
        if (!quest.isHidden)
        {
            if (quest.answer.points)
            {
                aSum += quest.answer.points;
            }else if ([quest.title isEqualToString:@"Систолическое АД"])
            {
                sad = [quest.answer.title floatValue];
            } else if ([quest.title isEqualToString:@"Диастолическое АД"])
            {
                dad = [quest.answer.title floatValue];
            }
            
            if ([quest.title isEqualToString:@"Лейкоциты, 10^3/мл"])
            {
                cont = NO;
                break;
            }
        }
        i++;
    }
    
   
    sad = (dad*2+sad)/3;
    
    if (sad > 160){
        aSum +=4;
    } else if ((130 < sad)&&(sad <= 159))
    {
        aSum +=3;
        
    }else if ((110 < sad)&&(sad <= 129)){
        aSum +=2;
    } else if ((70 < sad)&&(sad <= 109))
    {
        aSum+=0;
    } else if ((55 < sad)&&(sad <= 69))
    {
        aSum+=2;
    }
    
    
    
    
    //БЛОК В
    id object = nil;
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"title == %@", @"Открывание глаз"];
    

    NSArray *result = [self.questionsArray filteredArrayUsingPredicate:pred];
    if(result && result.count > 0)
    {
        object = [result objectAtIndex:0];
    }
    
    
    i = [self.questionsArray indexOfObject:object];
    
    cont = YES;
    
    int bSum = 15;
    while (cont) {
        DHApacheQuestionModel *quest = self.questionsArray[i];

        if (!quest.isHidden)
        {
            if (quest.answer.points)
            {
                bSum -= quest.answer.points;
            }
            
            if (([quest.title isEqualToString:@"Без интубации"])||([quest.title isEqualToString:@"На интубации"]))
            {
                break;
            }
        }
        i++;

    }
    
    
    //Блок С
    object = nil;
    pred = [NSPredicate predicateWithFormat:@"title == %@", @"Печень: подтвержденный цирроз с портальной гипертензией или эпизоды печеночной недостаточности/энцефалопатии/комы"];
    

        result = [self.questionsArray filteredArrayUsingPredicate:pred];
        if(result && result.count > 0)
        {
            object = [result objectAtIndex:0];
        }
    
    
    i = [self.questionsArray indexOfObject:object];
    
    cont = YES;
    
    int cSum = 0;
    bool isChecked = NO;
    
    while (cont) {
        DHApacheQuestionModel *quest = self.questionsArray[i];
        
        if (!quest.isHidden)
        {
            if (quest.answer.isSelected)
            {
                isChecked = YES;
            }
            
            if ([quest.title isEqualToString:@"Выберите нужную категорию"])
            {
                if (([quest.answer.title isEqualToString:@"Не оперирован"])&&(isChecked))
                {
                    cSum += 5;
                } else if (([quest.answer.title isEqualToString:@"Плановая операция"])&&(isChecked))
                {
                    cSum += 2;
                }
                break;
            }
        }
        i++;

    }
    
    int allSum = aSum+bSum+cSum;
    float logit = -3.517+ allSum*0.146;
    double death = exp(logit)/(1+exp(logit));
    
    [self.deathLabel setText:[NSString stringWithFormat:@"Предположительный риск смерти – %.1f",death*100]];
    self.deathLabel.hidden = NO;
    [self.apacheLabel setText:[NSString stringWithFormat:@"Расчетное значение APACHE II - %i",allSum]];
    self.apacheLabel.hidden = NO;
    
    
    CGRect frame = self.footerView.frame;
    frame.size.height = 100;
    self.footerView.frame = frame;
    
    self.tableView.tableFooterView = self.footerView;
    
}

- (IBAction)clear:(id)sender {
    self.questionsArray = [DHApacheQuestionModel setQuestions];
    [self.tableView reloadData];
    [self clearFooter];
}

//=========----------------
#pragma mark - TableView Delegate
//=========----------------

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.questionsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DHApacheQuestionModel *question = self.questionsArray[indexPath.row];
    
    NSString *cellIdentifier = question.cellIdentifier;
    
    Class theClass = NSClassFromString(question.cellClass);
    id cell = [[theClass alloc] init];
    
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell setCellWithQuestion:question];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedQuestion = self.questionsArray[indexPath.row];
    
    if (self.selectedQuestion.answers.count > 0)
    {
        [self showPicker];
        [self.picker selectRow:0 inComponent:0 animated:NO];
        [self.picker reloadAllComponents];
    }
    
    if ([self.selectedQuestion.cellIdentifier isEqualToString:@"checkBoxCell"])
    {
        self.selectedQuestion.answer.isSelected = !self.selectedQuestion.answer.isSelected;
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
    [self hidePicker];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DHApacheQuestionModel *question = self.questionsArray[indexPath.row];
    if (question.isHidden == YES)
    {
        return 0;
    }
    
    if ([question.cellIdentifier isEqualToString:@"simpleCell"])
    {
        return 50;
    } else if ([question.cellIdentifier isEqualToString:@"textFieldCell"])
    {
        return 50;
    } else if ([question.cellIdentifier isEqualToString:@"segmentedControlCell"])
    {
        return 70;
    }else if ([question.cellIdentifier isEqualToString:@"checkBoxCell"])
    {
        return [self findHeightForText:question.title havingWidth:276 andFont:[UIFont systemFontOfSize:13]]+20;
    } else {
        return 50;
    }
}

- (CGFloat)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font
{
    CGFloat result = font.pointSize + 4;
    if (text)
    {
        CGSize textSize = { widthValue, CGFLOAT_MAX };       //Width and height of text area
        CGSize size;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            //iOS 7
            CGRect frame = [text boundingRectWithSize:textSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{ NSFontAttributeName:font }
                                              context:nil];
            size = CGSizeMake(frame.size.width, frame.size.height+1);
        }
        else
        {
            //iOS 6.0
            size = [text sizeWithFont:font constrainedToSize:textSize lineBreakMode:NSLineBreakByWordWrapping];
        }
        result = MAX(size.height, result); //At least one row
    }
    return result;
}

#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.selectedQuestion.answers.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    DHApacheAnswerModel *answer = self.selectedQuestion.answers[row];
    return answer.title;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    DHApacheAnswerModel *answer = self.selectedQuestion.answers[row];
    self.selectedQuestion.answer = answer;
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:[self.questionsArray indexOfObject:self.selectedQuestion] inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}


@end
