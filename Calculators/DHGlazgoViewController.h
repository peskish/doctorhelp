//
//  DHArthritisViewController.h
//  DoctorHelp
//
//  Created by Dmitry Venikov on 21/10/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCRadioButton.h"
#import "DHViewController.h"

@interface DHGlazgoViewController : DHViewController

@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;

- (IBAction)calc:(UIButton *)sender;
- (IBAction)clear:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextField *resultField;

@property (weak, nonatomic) IBOutlet VCRadioButton *btn1;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn4;

@property (weak, nonatomic) IBOutlet VCRadioButton *btn2_1;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2_2;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2_3;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2_4;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2_5;

@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_1;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_2;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_3;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_4;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_5;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_6;


@end
