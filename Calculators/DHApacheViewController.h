//
//  DHApacheViewController.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"

@interface DHApacheViewController : DHViewController <UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate>

@end
