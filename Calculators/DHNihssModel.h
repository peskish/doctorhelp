//
//  DHNihssModel.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 14.11.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DHNihssModel : NSObject

@property NSString *title;
@property NSString *info;
@property NSArray *answers;
@property NSInteger chosenAnswer;
@property NSString *additionalInfo;
@property BOOL needPicture;
+ (NSArray *)setQuestions;

@end
