//
//  DHApacheButtonTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import "DHApacheButtonTableViewCell.h"

@implementation DHApacheButtonTableViewCell

-(void)setCellWithQuestion:(DHApacheQuestionModel *)question
{
    self.questionButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.questionButton.titleLabel.numberOfLines = 0;
    [self.questionButton setTitle:question.title forState:UIControlStateNormal];
    self.questionButton.titleLabel.textAlignment = NSTextAlignmentCenter;
}

- (IBAction)showGradientButtonTapped:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"showGradient" object:nil];
}

@end
