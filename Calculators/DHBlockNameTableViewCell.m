//
//  DHBlockNameTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import "DHBlockNameTableViewCell.h"

@implementation DHBlockNameTableViewCell

-(void)setCellWithQuestion:(DHApacheQuestionModel *)question
{
    [self.questionTitleLabel setText:question.title];
}
@end
