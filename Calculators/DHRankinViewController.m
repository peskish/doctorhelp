//
//  DHRankinViewController.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 20.05.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHRankinViewController.h"

@interface DHRankinViewController ()
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
@property float defaultSize;
@end

@implementation DHRankinViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Rankin calculator"];
    
    self.defaultSize = 839;
    _btn1.groupName = @"group1";
    _btn2.groupName = @"group1";
    _btn3.groupName = @"group1";
    _btn4.groupName = @"group1";
    _btn5.groupName = @"group1";
    _btn6.groupName = @"group1";

    
    _btn1.selectedValue = @"1";
    _btn2.selectedValue = @"2";
    _btn3.selectedValue = @"3";
    _btn4.selectedValue = @"4";
    _btn5.selectedValue = @"5";
    _btn6.selectedValue = @"0";
    
    self.resultLabel.numberOfLines = 0;
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

- (IBAction)calcButtonTapped:(id)sender {
    NSInteger result = [_btn1.selectedValueForGroup integerValue];

    CGRect frame = self.contentView.frame;
    frame.size.height = self.defaultSize;
    self.contentView.frame = frame;
    
    switch (result) {
        case 0:
            [self.resultLabel setText:@"Результат:\nНет признаков инвалидизации"];
            break;
        case 1:
            [self.resultLabel setText:@"Результат:\nПервая степень инвалидизации. Предполагает отсутствие признаков инвалидности, больной в состоянии выполнять без посторонней помощи все действия по уходу за собой. Однако это не исключает у больного наличия мышечной слабости, расстройств чувствительности, нарушений речи или других неврологических функций. Эти нарушения выражены в незначительной степени и не ведут к ограничению активности"];
            break;
        case 2:
            [self.resultLabel setText:@"Результат:\nВторая степень инвалидизации. Предполагает наличие легких признаков инвалидности, но больной в состоянии ухаживать за собой без посторонней помощи. Например, не может вернуться к прежней работе, но способен обслуживать себя без постороннего присмотра"];
            break;
        case 3:
            [self.resultLabel setText:@"Результат:\nТретья степень инвалидизации. Умеренно выраженные признаки инвалидности, больной нуждается в некоторой посторонней помощи при одевании, гигиеническом уходе за собой; больной не в состоянии внятно читать или свободно общаться с окружающими. Больной может пользоваться ортопедическими приспособлениями или тростью"];
            break;
        case 4:
            [self.resultLabel setText:@"Результат:\nЧетвертая степень инвалидизации. Предполагает наличие выраженных признаков инвалидности. Больной не в состоянии ходить и ухаживать за собой без посторонней помощи, он нуждается в круглосуточном присмотре и в ежедневной посторонней помощи. При этом он в состоянии самостоятельно или при минимальной помощи со стороны выполнять какую-то часть мероприятий по уходу за собой"];
            break;
        case 5:
            [self.resultLabel setText:@"Результат:\nПятая степень инвалидизации. Сильно выраженные признаки инвалидности. Больной прикован к постели, неопрятен и нуждается в постоянном уходе и наблюдении"];
            break;
        default:
            break;
    }
 
    frame = self.resultLabel.frame;
    frame.size.width = self.btn1.frame.size.width - 10;
    self.resultLabel.frame = frame;
    [self.resultLabel sizeToFit];
    self.resultLabel.center = CGPointMake(self.btn1.center.x, self.resultLabel.center.y);
    
    frame = self.contentView.frame;
    frame.size.height += self.resultLabel.frame.size.height;
    self.contentView.frame = frame;
    self.mainScroll.contentSize = self.contentView.bounds.size;
    
}
- (IBAction)clearButtonTapped:(id)sender {
    [self.btn1 setSelected:NO];
    [self.btn2 setSelected:NO];
    [self.btn3 setSelected:NO];
    [self.btn4 setSelected:NO];
    [self.btn5 setSelected:NO];
    [self.btn6 setSelected:NO];

    [self.resultLabel setText:@""];
    CGRect frame = self.contentView.frame;
    frame.size.height = self.defaultSize;
    
    self.contentView.frame = frame;
    self.mainScroll.contentSize = self.contentView.bounds.size;

}

@end
