//
//  DHApacheSegmentedTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import "DHApacheSegmentedTableViewCell.h"

@implementation DHApacheSegmentedTableViewCell

-(void)setCellWithQuestion:(DHApacheQuestionModel *)question
{
    self.question = question;
    
    [self.questionTitleLabel setText:question.title];
    
    [self.answersSegmentedControl removeAllSegments];
    
    for (DHApacheAnswerModel *answer in question.answers)
    {
        [self.answersSegmentedControl insertSegmentWithTitle:answer.title atIndex:[question.answers indexOfObject:answer] animated:NO];
    }
    
    if (question.answer)
    {
        [self.answersSegmentedControl setSelectedSegmentIndex:[question.answers indexOfObject:question.answer]];
    }
}

- (IBAction)indexChanged:(UISegmentedControl *)sender {
    self.question.answer = [self.question.answers objectAtIndex:sender.selectedSegmentIndex];
    self.question.answer.isSelected = sender.selectedSegmentIndex;
    [[NSNotificationCenter defaultCenter]postNotificationName:@"updateGradient" object:nil];
}


@end
