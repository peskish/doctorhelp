//
//  DHApacheCheckBoxTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import "DHApacheCheckBoxTableViewCell.h"

@implementation DHApacheCheckBoxTableViewCell

-(void)setCellWithQuestion:(DHApacheQuestionModel *)question
{
    [self.questionTitleLabel setText:question.title];
    [self.questionTitleLabel sizeToFit];
    
    self.questionTitleLabel.center = CGPointMake(self.questionTitleLabel.center.x, self.frame.size.height/2);
    
    if (question.answer.isSelected)
    {
        [self.checkboxImageView setImage:[UIImage imageNamed:@"check_filled"]];
    } else {
        [self.checkboxImageView setImage:[UIImage imageNamed:@"check"]];
    }
}

@end
