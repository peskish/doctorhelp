//
//  DHApacheSimpleTableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DHApacheSimpleTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *questionTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerTitleLabel;

-(void)setCellWithQuestion:(DHApacheQuestionModel *)question;

@end
