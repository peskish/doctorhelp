//
//  DHBASDAIViewController.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 28.05.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHBASDAIViewController.h"
#import "DHBasdaiQuestionTableViewCell.h"
#import "STAlertView.h"
#import "BASDAI.h"
#import "Patient.h"
#import "DHAppDelegate.h"
#import "UIColor+DHfromHex.h"

@interface DHBASDAIViewController ()
@property NSMutableArray *questionsArray;
@property double result;
@property STAlertView *nameAlertView;
@property STAlertView *alertView2;
@property NSMutableArray *patientsArray;
@property NSArray *resultsArray;
@property NSArray *resultsArray2;
@property (strong, nonatomic) IBOutlet UIView *sectionHeaderView;
@property (weak, nonatomic) IBOutlet UILabel *sectionHeaderTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *sectionHeaderRemoveButton;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet UIPickerView *picker2;

@end

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


@implementation DHBASDAIViewController
@synthesize managedObjectContext;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(clearBASDAI:) name:@"clearBASDAI" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(newPatientBASDAI:) name:@"newPatientBASDAI" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(calcBASDAI:) name:@"calcBASDAI" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addResultBASDAI:) name:@"addResultBASDAI" object:nil];
    
    
    self.resultsArray = [[NSMutableArray alloc]initWithObjects:@"1 балл",@"2 балла",@"3 балла",@"4 балла",@"5 баллов",@"6 баллов",@"7 баллов",@"8 баллов",@"9 баллов",@"10 баллов", nil];
    self.resultsArray2 = [[NSMutableArray alloc]initWithObjects: @"0 баллов - 0 мин",@"1 балл",@"2 балла",@"3 балла - 30 минут",@"4 балла",@"5 баллов - 1 час",@"6 баллов",@"7 баллов",@"8 баллов - 1,5 часа",@"9 баллов",@"10 баллов - 2 часа", nil];
    
    if (managedObjectContext == nil)
    {
        managedObjectContext = [(DHAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    }
    
    self.result = 0;
    
    self.questionsArray = [[NSMutableArray alloc]initWithArray:[DHBASDAIModel setBasdaiObjects]];
    
    self.navigationItem.title = @"Калькулятор";
    
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"BASDAI calculator"];
    
    self.patientsArray = [NSMutableArray new];
    
    [self updatePatients];
    
    self.picker.hidden = YES;
    self.picker.delegate = self;
    self.picker.dataSource = self;
    [self.picker setShowsSelectionIndicator:YES];
    
    self.picker2.hidden = YES;
    self.picker2.delegate = self;
    self.picker2.dataSource = self;
    [self.picker2 setShowsSelectionIndicator:YES];
    
    self.tableView.allowsMultipleSelectionDuringEditing = NO;

    
}

//=====================
#pragma mark - PICKER
//=====================
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == self.picker)
    {
        return self.resultsArray.count;
    } else {
        return self.resultsArray2.count;
    }
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView == self.picker)
    {
        return self.resultsArray[row];
    } else {
        return self.resultsArray2[row];
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == self.picker)
    {
        DHBASDAIModel *basdaiObject = self.questionsArray[pickerView.tag];
        basdaiObject.answer = row+1;
        
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:pickerView.tag inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    } else {
        DHBASDAIModel *basdaiObject = self.questionsArray[pickerView.tag];
        basdaiObject.answer = row;
        
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:pickerView.tag inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }

}

//=====================
#pragma mark - NOTIFICATIONS
//=====================

- (void)clearBASDAI:(NSNotification *)notification
{
    self.result = 0;
    for (DHBASDAIModel *basdaiObject in self.questionsArray)
    {
        basdaiObject.answer = 0;
    }
    
    [self.questionsArray enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(DHBASDAIModel *p, NSUInteger index, BOOL *stop) {
        if ([p.question isEqualToString:@"Индекс BASDAI"]) {
            [self.questionsArray removeObjectAtIndex:index];
        }
    }];
    
    [self.tableView reloadData];
}

- (void)newPatientBASDAI:(NSNotification *)notification
{
    self.nameAlertView = [[STAlertView alloc]initWithTitle:@"Фамилия И.О." message:nil textFieldHint:@"Фамилия И.О." textFieldValue:nil cancelButtonTitle:@"Отмена" otherButtonTitle:@"ОК" cancelButtonBlock:^{
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"acceptBASDAI"];

    } otherButtonBlock:^(NSString *result) {
        self.result = 0;
        for (DHBASDAIModel *basdaiObject in self.questionsArray)
        {
            basdaiObject.answer = 0;
        }
        
        [self.questionsArray enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(DHBASDAIModel *p, NSUInteger index, BOOL *stop) {
            if ([p.question isEqualToString:@"Индекс BASDAI"]) {
                [self.questionsArray removeObjectAtIndex:index];
            }
        }];
        
        [self.tableView reloadData];
        [self savePatient:result];
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"acceptBASDAI"];
    }];
    
    [self.nameAlertView show];

}

- (void)calcBASDAI:(NSNotification *)notification
{
    [self calc];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"acceptBASDAI"]boolValue])
    {
    } else {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Внимание"
                                                           message:@"Наибольшая ценность индекса BASDAI проявляется при динамическом наблюдении. Вы согласны сохранить результат, чтобы потом вернуться к сравнению?"
                                                          delegate:self
                                                 cancelButtonTitle:@"Отмена"
                                                 otherButtonTitles:@"ОК", nil];
        
        [alertView show];
    }
}

- (void)addResultBASDAI:(NSNotification *)notification
{
    if (self.result)
    {
        NSDictionary *userInfo = notification.userInfo;
        NSInteger index = [userInfo[@"index"] integerValue];
        
        Patient *patient = self.patientsArray[index-1];
        BASDAI *basdai = [NSEntityDescription insertNewObjectForEntityForName:@"BASDAI" inManagedObjectContext:managedObjectContext];
        basdai.ball = [NSNumber numberWithDouble:self.result];
        basdai.date = [NSDate date];
        [patient addBasdaiObject:basdai];
        
        NSError *error;
        if (![managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        
        [self updatePatients];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Внимание"
                                                            message:@"Произведите расчет"
                                                           delegate:self
                                                  cancelButtonTitle:@"ОК"
                                                  otherButtonTitles: nil];
        [alertView show];
    }
    
}


//===================
#pragma mark - EVENTS
//===================

-(void)calc
{
    double result = 0;
    for (int i = 0 ; i < self.questionsArray.count ; i++)
    {
        DHBASDAIModel *basdaiObject = self.questionsArray[i];
        if ([basdaiObject.question isEqualToString:@"Индекс BASDAI"])
        {
            
        }else {
            if (i < 4)
            {
                result +=basdaiObject.answer;
            } else {
                result +=basdaiObject.answer/2;
            }
        }
    }
    
    self.result = result/5;
    
    if (self.result)
    {
        DHBASDAIModel *basdaiObject = self.questionsArray[self.questionsArray.count-1];
        
        if ([basdaiObject.question isEqualToString:@"Индекс BASDAI"])
        {
            basdaiObject.answer = self.result;
        } else {
            DHBASDAIModel *resultObject = [[DHBASDAIModel alloc]init];
            resultObject.question = @"Индекс BASDAI";
            resultObject.answer = self.result;
            [self.questionsArray addObject:resultObject];
        }
        [self.tableView reloadData];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Внимание"
                                                            message:@"Произведите расчет"
                                                           delegate:self
                                                  cancelButtonTitle:@"ОК"
                                                  otherButtonTitles: nil];
        [alertView show];
    }
}


-(void)savePatient:(NSString *)patientName
{

    if (managedObjectContext == nil)
    {
        managedObjectContext = [(DHAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    }
    NSManagedObjectContext *context = managedObjectContext;
    Patient *newPatient = [NSEntityDescription insertNewObjectForEntityForName:@"Patient" inManagedObjectContext:context];
    newPatient.name = patientName;
    if (self.result)
    {
        
        BASDAI *basdai = [NSEntityDescription insertNewObjectForEntityForName:@"BASDAI" inManagedObjectContext:context];
        basdai.date = [NSDate date];
        basdai.ball = [NSNumber numberWithDouble:self.result];
        [newPatient addBasdaiObject:basdai];
        
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }

    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Внимание"
                                                            message:@"Произведите расчет"
                                                           delegate:self
                                                  cancelButtonTitle:@"ОК"
                                                  otherButtonTitles: nil];
        [alertView show];
    }
    [self updatePatients];

}

-(void)updatePatients
{
    if (managedObjectContext == nil)
    {
        managedObjectContext = [(DHAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext];
    }
    NSManagedObjectContext *context = managedObjectContext;
    
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:@"Patient"];
    
    NSError *error = nil;
    
    [self.patientsArray removeAllObjects];
    [self.patientsArray addObjectsFromArray: [context executeFetchRequest:request error:&error]];
    
    [self.tableView reloadData];
    
}

-(void)removePatient:(UIButton *)sender
{
    Patient *patient = self.patientsArray[sender.tag-1];

    self.alertView2 = [[STAlertView alloc]initWithTitle:@"Внимание" message:@"Вы действительно хотите удалить пациента?" cancelButtonTitle:@"Отмена" otherButtonTitle:@"ОК" cancelButtonBlock:^{
        
    } otherButtonBlock:^{
        [self.managedObjectContext deleteObject:patient];
        [self updatePatients];
        
        if (self.patientsArray.count == 0)
        {
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"acceptBASDAI"];
        }
    }];
    
    [self.alertView2 show];

}

//======================================
#pragma mark - AlertView Delegate
//======================================

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        self.nameAlertView = [[STAlertView alloc]initWithTitle:@"Фамилия И.О." message:nil textFieldHint:@"Фамилия И.О." textFieldValue:nil cancelButtonTitle:@"Отмена" otherButtonTitle:@"ОК" cancelButtonBlock:^{
            
        } otherButtonBlock:^(NSString *result) {
            [self savePatient:result];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"acceptBASDAI"];
        }];
        
        [self.nameAlertView show];
        
    }
}


//=========================================================
#pragma mark - TableViewDelegate
//=========================================================

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1 + self.patientsArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return self.questionsArray.count+1;
    } else {
        Patient *patient = self.patientsArray[section-1];
        return patient.basdai.count + 1;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 35)];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    [titleLabel setTextColor:[UIColor whiteColor]];
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    
    headerView.backgroundColor = [UIColor colorwithHexString:@"4CD964" alpha:1.0f];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self
               action:@selector(removePatient:)
     forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:nil forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"remove"] forState:UIControlStateNormal];
    button.frame = CGRectMake(320-50, 0, 35, 35);
    button.tag = section;
    
    [headerView addSubview:button];
    [headerView addSubview:titleLabel];
    
    if (section == 0) {
        [titleLabel setText:@"Расчет индекса"];
        button.hidden = YES;
    } else
    {
        Patient *patient = self.patientsArray[section-1];
        [titleLabel setText:patient.name];
    }
    
    [titleLabel sizeToFit];
    titleLabel.center = CGPointMake(headerView.frame.size.width/2, headerView.frame.size.height/2);
    button.center = CGPointMake(button.center.x, headerView.frame.size.height/2);
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row < self.questionsArray.count)
        {
            DHBASDAIModel *basdaiObject = self.questionsArray[indexPath.row];
            CGFloat height = [self findHeightForText:basdaiObject.question havingWidth:251 andFont:[UIFont systemFontOfSize:14]];
            return height+20;
        } else {
            return 44;
        }
    } else {
        return 44;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"basdaiQuestionCell";
    
    DHBasdaiQuestionTableViewCell *cell = (DHBasdaiQuestionTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    DHBASDAIModel *basdaiObject = [[DHBASDAIModel alloc]init];
   
    if (indexPath.row < self.questionsArray.count)
    {
        basdaiObject = self.questionsArray[indexPath.row];
    }
    cell.patientsArray = self.patientsArray;
    [cell setCell:basdaiObject index:indexPath array:self.questionsArray];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.picker.hidden = YES;
    self.picker2.hidden = YES;
    
    if (indexPath.section == 0)
    {
        if (indexPath.row < self.questionsArray.count-1)
        {
            [self.picker selectRow:0 inComponent:0 animated:NO];
            self.picker.tag = indexPath.row;
            self.picker.hidden = NO;
        } else if (indexPath.row == self.questionsArray.count-1)
        {
            [self.picker2 selectRow:0 inComponent:0 animated:NO];
            self.picker2.tag = indexPath.row;
            self.picker2.hidden = NO;
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section != 0)
    {
        Patient *patient = self.patientsArray[indexPath.section-1];
        NSArray *array = [patient.basdai allObjects];
        
        if (indexPath.row == array.count)
        {
            return NO;
        } else {
            return YES;

        }
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Patient *patient = self.patientsArray[indexPath.section-1];
        NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO]];
        
        NSArray *array = [[patient.basdai allObjects] sortedArrayUsingDescriptors:sortDescriptors];
        
        BASDAI *basdai = array[indexPath.row];
        [self.managedObjectContext deleteObject:basdai];
      
        NSError *error;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        }
        [self updatePatients];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    self.picker.hidden = YES;
    self.picker2.hidden = YES;
}

- (CGFloat)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font
{
    CGFloat result = font.pointSize + 4;
    if (text)
    {
        CGSize textSize = { widthValue, CGFLOAT_MAX };       //Width and height of text area
        CGSize size;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            //iOS 7
            CGRect frame = [text boundingRectWithSize:textSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{ NSFontAttributeName:font }
                                              context:nil];
            size = CGSizeMake(frame.size.width, frame.size.height+1);
        }
        else
        {
            //iOS 6.0
            size = [text sizeWithFont:font constrainedToSize:textSize lineBreakMode:NSLineBreakByWordWrapping];
        }
        result = MAX(size.height, result); //At least one row
    }
    return result;
}
@end
