//
//  DHChildBirthCalcViewController.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 27.01.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHChildBirthCalcViewController.h"

@interface DHChildBirthCalcViewController ()

@property (weak, nonatomic) IBOutlet UIView *segmentedView;
@property BOOL isResult;
@end

@implementation DHChildBirthCalcViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    self.selector.selectedSegmentIndex = 0;
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"ChildBirth calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    UIDatePicker *picker1   = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 210, 320, 216)];
    [picker1 setDatePickerMode:UIDatePickerModeDate];
    picker1.backgroundColor = [UIColor whiteColor];
    [picker1 addTarget:self action:@selector(startDateSelected:) forControlEvents:UIControlEventValueChanged];

    self.firstField.inputView  = picker1;
    
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    self.divider.hidden = YES;
    self.isResult = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)startDateSelected:(UIDatePicker *)picker
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd.MM.yyyy"];

    NSString *stringFromDate = [formatter stringFromDate:picker.date];

    [self.firstField setText:stringFromDate];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)layoutContentView
{
    CGRect frame = self.contentView.frame;
    
    if (self.selector.selectedSegmentIndex == 0)
    {
        if (self.isResult)
        {
            frame.size.height = 375;
        } else {
            frame.size.height = 275;
        }
    } else {
        if (self.isResult)
        {
            frame.size.height = 450;
        } else {
            frame.size.height = 350;
        }
    }
    
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.contentView.frame = frame;
                     }
                     completion:^(BOOL finished) {
                         self.mainScroll.contentSize = self.contentView.bounds.size;
                     }];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];

    [self layoutContentView];

}
- (IBAction)changeSegmentedControlValue:(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex)
    {
        self.secondLabel.hidden = NO;
        self.weeksField.hidden = NO;
        self.daysField.hidden = NO;
   
        [self.firstField setText:@""];
        [self.firstLabel setText:@"Введите дату УЗИ"];
        

    } else
    {
        self.secondLabel.hidden = YES;
        self.weeksField.hidden = YES;
        self.daysField.hidden = YES;
   
        [self.firstField setText:@""];
        [self.firstLabel setText:@"Введите дату первого дня последней менструации"];
    
    }

    [self layoutContentView];
}

- (IBAction)calc:(id)sender
{
    if ([self.firstField.text length])
    {
        self.isResult = YES;
        [self.view endEditing:YES];
        self.divider.hidden = NO;
        if (self.selector.selectedSegmentIndex == 0)
        {
            NSDateComponents *kekDateComponents = [NSDateComponents new];
            kekDateComponents.week = 40;
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd.MM.yyyy"];
            
            NSDate *inputDate = [formatter dateFromString:self.firstField.text];
            
            NSDate *birthDate = [[NSCalendar currentCalendar]dateByAddingComponents:kekDateComponents
                                                                             toDate:inputDate
                                                                            options:0];
            
            NSString *longDateString = [NSDateFormatter localizedStringFromDate:birthDate
                                                                      dateStyle:NSDateFormatterLongStyle
                                                                      timeStyle:NSDateFormatterNoStyle];
            NSDateFormatter *weekday = [[NSDateFormatter alloc] init];
            [weekday setDateFormat: @"EEEE"];
            longDateString = [longDateString stringByAppendingString:[NSString stringWithFormat:@", %@", [weekday stringFromDate:birthDate]]];
            
            [self.firstResultLabel setText:[NSString stringWithFormat:@"Предполагаемая дата родов - %@",longDateString]];
            
            NSInteger numberOfDays = [self daysBetweenDate:inputDate andDate:[NSDate date]];
            int weeks = numberOfDays/7;
            int days = numberOfDays - weeks*7;
            NSArray *daysEndings = [[NSArray alloc]initWithObjects:@"день",@"дня",@"дней", nil];
            NSArray *weeksEndings = [[NSArray alloc]initWithObjects:@"неделя",@"недели",@"недель", nil];
            
            NSString *daysFormat = [self format:days weeksOrDays:daysEndings];
            NSString *weeksFormat = [self format:weeks weeksOrDays:weeksEndings];
            
            [self.secondResultLabel setText:[NSString stringWithFormat:@"Срок беременности на текущую дату составляет %i %@ %i %@",weeks,weeksFormat,days,daysFormat]];
            
        } else {
            int numberOfDays = [self.weeksField.text intValue]*7 + [self.daysField.text intValue];
           
            NSDateComponents *dateComponents = [NSDateComponents new];
            dateComponents.day = -numberOfDays;
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd.MM.yyyy"];
            
            NSDate *inputDate = [formatter dateFromString:self.firstField.text];
            
            NSDate *birthDate = [[NSCalendar currentCalendar]dateByAddingComponents:dateComponents
                                                                             toDate:inputDate
                                                                            options:0];
            
            dateComponents.week = 40;
            birthDate = [[NSCalendar currentCalendar]dateByAddingComponents:dateComponents
                                                                     toDate:birthDate
                                                                    options:0];
            
            NSString *longDateString = [NSDateFormatter localizedStringFromDate:birthDate
                                                                      dateStyle:NSDateFormatterLongStyle
                                                                      timeStyle:NSDateFormatterNoStyle];
            NSDateFormatter *weekday = [[NSDateFormatter alloc] init];
            [weekday setDateFormat: @"EEEE"];
            longDateString = [longDateString stringByAppendingString:[NSString stringWithFormat:@", %@", [weekday stringFromDate:birthDate]]];
            
            [self.firstResultLabel setText:[NSString stringWithFormat:@"Предполагаемая дата родов - %@",longDateString]];
            
            NSArray *daysEndings = [[NSArray alloc]initWithObjects:@"день",@"дня",@"дней", nil];
            NSArray *weeksEndings = [[NSArray alloc]initWithObjects:@"неделя",@"недели",@"недель", nil];
            
            NSString *daysFormat = [self format:[self.daysField.text intValue] weeksOrDays:daysEndings];
            NSString *weeksFormat = [self format:[self.weeksField.text intValue] weeksOrDays:weeksEndings];
            
            [self.secondResultLabel setText:[NSString stringWithFormat:@"Срок беременности на текущую дату составляет %i %@ %i %@",[self.weeksField.text intValue],weeksFormat,[self.daysField.text intValue],daysFormat]];
        }
    }
}

- (NSInteger)daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime
{
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate
                 interval:NULL forDate:fromDateTime];
    [calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate
                 interval:NULL forDate:toDateTime];
    
    NSDateComponents *difference = [calendar components:NSDayCalendarUnit
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [difference day];
}

-(NSString *)format:(NSInteger)daysCount weeksOrDays:(NSArray *)aEndings
{
    NSString *sEnding;
    daysCount = daysCount % 100;
    if (daysCount>=11 && daysCount<=19) {
        sEnding=aEndings[2];
    }
    else {
        int i = daysCount % 10;
        switch (i)
        {
            case (1): sEnding = aEndings[0]; break;
            case (2):
            case (3):
            case (4): sEnding = aEndings[1]; break;
            default: sEnding = aEndings[2];
        }
    }
    return sEnding;
}

- (IBAction)clearButton:(id)sender
{
    [self.firstField setText:@""];
    [self.weeksField setText:@""];
    [self.daysField setText:@""];
    [self.firstResultLabel setText:@""];
    [self.secondResultLabel setText:@""];
    self.isResult = NO;
    [self.view endEditing:YES];
    [self layoutContentView];
    self.divider.hidden = YES;

}

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSLog(@"keyboard frame raw %@", NSStringFromCGRect(keyboardFrame));
    self.keyboardHeight = keyboardFrame.size.height;
    CGRect rect = fullScreenView;
    rect.size.height -= (self.keyboardHeight);
    
    self.mainScroll.frame = rect;
    
}

-(void)keyboardWillHide
{

    
    self.mainScroll.frame = fullScreenView;
    
}
@end
