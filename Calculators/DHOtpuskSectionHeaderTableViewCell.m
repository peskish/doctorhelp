//
//  DHOtpuskSectionHeaderTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 18.01.16.
//  Copyright © 2016 Ptenster. All rights reserved.
//

#import "DHOtpuskSectionHeaderTableViewCell.h"

@implementation DHOtpuskSectionHeaderTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)setCell:(DHOtpuskViewModel *)headerModel
{
    [self.headerTitleLabel setText:headerModel.text];
}

@end
