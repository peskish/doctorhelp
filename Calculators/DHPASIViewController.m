//
//  DHPASIViewController.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 25.05.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHPASIViewController.h"
#import "DHPASIObject.h"
#import "DHPASITableViewCell.h"
#import "UIColor+DHfromHex.h"

@interface DHPASIViewController ()
@property NSArray *pasiObjectsArray;
@property DHPASIObject *choosedPasiObject;
@property NSInteger indexOfParam;
@end

@implementation DHPASIViewController

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"PASI calculator"];
    
    self.pasiObjectsArray = [[NSArray alloc]initWithArray:[DHPASIObject setPASIObjects]];
    [self.tableView reloadData];
    
    self.tableView.frame = CGRectMake(0, 69+40+60, self.view.frame.size.width, self.view.frame.size.height - 69 - 40 - 60);
    self.tableView.tableFooterView = self.footerView;
    self.tableView.tableHeaderView = self.headerView;
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
}

//============================
#pragma mark - Events
//============================
- (IBAction)clearButtonTapped:(id)sender
{
    [self clear];
}


- (IBAction)calcButtonTapped:(id)sender
{
    [self calc];
}

-(void)clear
{
    for (DHPASIObject *pasiObject in self.pasiObjectsArray)
    {
        pasiObject.needToShowParams = NO;
        pasiObject.affectedArea = 99;
        pasiObject.itch = 0;
        pasiObject.hyperemia = 0;
        pasiObject.thickening = 0;
        pasiObject.peeling = 0;
        
        for (UILabel *resultLabel in self.footerView.subviews)
        {
            if (resultLabel.tag == 89)
            {
                [resultLabel removeFromSuperview];
                break;
            }
        }
        
        CGRect frame = self.footerView.frame;
        frame.size.height = self.calcButton.frame.size.height + 20;
        self.footerView.frame = frame;
        self.tableView.tableFooterView = self.footerView;

    }
    [self.tableView reloadData];
}

-(void)calc
{
    for (UILabel *resultLabel in self.footerView.subviews)
    {
        if (resultLabel.tag == 89)
        {
            [resultLabel removeFromSuperview];
            break;
        }
    }
    
    CGRect frame = self.footerView.frame;
    frame.size.height = self.calcButton.frame.size.height + 20;
    self.footerView.frame = frame;
    self.tableView.tableFooterView = self.footerView;
    
    float headPASI = 0;
    float bodyPASI = 0;
    float handPASI = 0;
    float legsPASI = 0;
    for (int i = 0 ; i <self.pasiObjectsArray.count ; i++)
    {
        DHPASIObject *pasiObject = self.pasiObjectsArray[i];
        switch (i)
        {
            case 0:
                headPASI = 0.1 * pasiObject.affectedArea * (pasiObject.itch + pasiObject.hyperemia + pasiObject.peeling + pasiObject.thickening);
                break;
            case 1:
                bodyPASI = 0.1 * pasiObject.affectedArea * (pasiObject.itch + pasiObject.hyperemia + pasiObject.peeling + pasiObject.thickening);
                break;
            case 2:
                handPASI = 0.1 * pasiObject.affectedArea * (pasiObject.itch + pasiObject.hyperemia + pasiObject.peeling + pasiObject.thickening);
                break;
            case 3:
                legsPASI = 0.1 * pasiObject.affectedArea * (pasiObject.itch + pasiObject.hyperemia + pasiObject.peeling + pasiObject.thickening);
                break;
        }

    }
    
    float resultPASI = headPASI + bodyPASI + handPASI + legsPASI;
    
    NSString *resultStr = [NSString stringWithFormat:@"PASI:\nГолова:%.02f\n Туловище:%.02f\n Руки:%.02f\n Ноги:%.02f\n Итого PASI:%.02f",headPASI,bodyPASI,handPASI,legsPASI,resultPASI];
    
    UILabel *resultLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    resultLabel.numberOfLines = 0;
    resultLabel.font = [UIFont systemFontOfSize:14];
    resultLabel.textAlignment = NSTextAlignmentCenter;
    resultLabel.tag = 89;
    resultLabel.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    [resultLabel setText:resultStr];
    frame = resultLabel.frame;
    frame.size.height = [self findHeightForText:resultStr havingWidth:320 andFont:[UIFont systemFontOfSize:14]];
    frame.size.width = 320;
    resultLabel.frame = frame;
    resultLabel.center = CGPointMake(self.footerView.center.x, resultLabel.center.y);
    
    
    frame = self.footerView.frame;
    frame.size.height += resultLabel.frame.size.height;
    self.footerView.frame = frame;
    
    [self.footerView addSubview:resultLabel];
    self.tableView.tableFooterView = self.footerView;

}

- (CGFloat)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font
{
    CGFloat result = font.pointSize + 4;
    if (text)
    {
        CGSize textSize = { widthValue, CGFLOAT_MAX };       //Width and height of text area
        CGSize size;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            //iOS 7
            CGRect frame = [text boundingRectWithSize:textSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{ NSFontAttributeName:font }
                                              context:nil];
            size = CGSizeMake(frame.size.width, frame.size.height+1);
        }
        else
        {
            //iOS 6.0
            size = [text sizeWithFont:font constrainedToSize:textSize lineBreakMode:NSLineBreakByWordWrapping];
        }
        result = MAX(size.height, result); //At least one row
    }
    return result;
}
//============================
#pragma mark - TableViewDelegate
//============================

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.pasiObjectsArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    DHPASIObject *pasiObject = self.pasiObjectsArray[section];
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 10)];
    view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;

    UILabel *label = [[UILabel alloc]init];
    [label setText:pasiObject.name];
    [label sizeToFit];
    [label setTextColor:[UIColor whiteColor]];
    label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin ;
    label.center = CGPointMake(tableView.frame.size.width/2, 15);
    view.backgroundColor = [UIColor colorwithHexString:@"4CD964" alpha:1.0f];
    [view addSubview:label];
    return view;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    DHPASIObject *pasiObject = self.pasiObjectsArray[section];
    if (pasiObject.needToShowParams)
    {
        return 7 + 4;
    } else {
        return 7;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"pasiCell";
    
    DHPASITableViewCell *cell = (DHPASITableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

    
    DHPASIObject *pasiObject = self.pasiObjectsArray[indexPath.section];
    
    cell.choosedImaged.hidden = YES;
    [cell.result setText:@""];

    switch (indexPath.row)
    {
        case 0:
            [cell.name setText:@"0%"];
            break;
        case 1:
            [cell.name setText:@"менее 10%"];
            break;
        case 2:
            [cell.name setText:@"10-29%"];
            break;
        case 3:
            [cell.name setText:@"30-49%"];
            break;
        case 4:
            [cell.name setText:@"50-69%"];
            break;
        case 5:
            [cell.name setText:@"70-89%"];
            break;
        case 6:
            [cell.name setText:@"90-100%"];
            break;
        case 7:
            [cell.name setText:@"Зуд"];
            switch (pasiObject.itch)
        {
            case 0:
                [cell.result setText:@"нет"];
                break;
            case 1:
                [cell.result setText:@"слабо"];
                break;
            case 2:
                [cell.result setText:@"умеренно"];
                break;
            case 3:
                [cell.result setText:@"значительно"];
                break;
            case 4:
                [cell.result setText:@"очень сильно"];
                break;
                
        }
            break;
        case 8:
            [cell.name setText:@"Гиперемия"];
            switch (pasiObject.hyperemia)
        {
            case 0:
                [cell.result setText:@"нет"];
                break;
            case 1:
                [cell.result setText:@"слабо"];
                break;
            case 2:
                [cell.result setText:@"умеренно"];
                break;
            case 3:
                [cell.result setText:@"значительно"];
                break;
            case 4:
                [cell.result setText:@"очень сильно"];
                break;
                
        }
            break;
        case 9:
            [cell.name setText:@"Шелушение"];
            switch (pasiObject.peeling)
        {
            case 0:
                [cell.result setText:@"нет"];
                break;
            case 1:
                [cell.result setText:@"слабо"];
                break;
            case 2:
                [cell.result setText:@"умеренно"];
                break;
            case 3:
                [cell.result setText:@"значительно"];
                break;
            case 4:
                [cell.result setText:@"очень сильно"];
                break;
                
        }
            break;
        case 10:
            [cell.name setText:@"Утолщение"];
            switch (pasiObject.thickening)
        {
            case 0:
                [cell.result setText:@"нет"];
                break;
            case 1:
                [cell.result setText:@"слабо"];
                break;
            case 2:
                [cell.result setText:@"умеренно"];
                break;
            case 3:
                [cell.result setText:@"значительно"];
                break;
            case 4:
                [cell.result setText:@"очень сильно"];
                break;
                
        }
            break;
    }
    

    
    if ((indexPath.row == pasiObject.affectedArea)&&(indexPath.row < 7))
    {
        cell.choosedImaged.hidden = NO;
    } else {
        cell.choosedImaged.hidden = YES;
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DHPASIObject *pasiObject = self.pasiObjectsArray[indexPath.section];
    if (indexPath.row < 7)
    {
        pasiObject.needToShowParams = YES;
        pasiObject.affectedArea = indexPath.row;
        [self.tableView reloadData];

    } else {
        self.choosedPasiObject = pasiObject;
        self.indexOfParam = indexPath.row;
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Степень выраженности"
                                                           message:nil
                                                          delegate:self
                                                 cancelButtonTitle:@"Нет"
                                                 otherButtonTitles:@"Слабо",@"Умеренно",@"Значительно",@"Очень сильно", nil];
        
        alertView.tintColor = [UIColor colorwithHexString:@"4CD964" alpha:1.0f];
        [alertView show];
    }
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (self.indexOfParam)
    {
        case 7:
            self.choosedPasiObject.itch = buttonIndex;
            break;
        case 8:
            self.choosedPasiObject.hyperemia = buttonIndex;
            break;
        case 9:
            self.choosedPasiObject.peeling = buttonIndex;
            break;
        case 10:
            self.choosedPasiObject.thickening = buttonIndex;
            break;
    }
    [self.tableView reloadData];
}
@end
