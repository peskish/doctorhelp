//
//  DHTempDisabilityViewController.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 16.01.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHTempDisabilityViewController.h"
#import "DHTemDisabilityTableViewCell.h"
#import "CKCalendarView.h"
#import <EventKit/EventKit.h>
#import "STAlertView.h"

@interface DHTempDisabilityViewController ()
@property NSMutableArray *firstBlockQuestions;
@property NSMutableArray *secondBlockQuestions;
@property NSMutableArray *thirdBlockQuestions;

@property NSMutableArray *firstBlockDates;
@property NSMutableArray *secondBlockDates;
@property NSMutableArray *thirdBlockDates;

@property BOOL needToShowAllFirstBlock;
@property BOOL needToShowAllSecondBlock;
@property BOOL needToShowAllThirdBlock;

@property BOOL needToShowSecondBlock;

@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@property (nonatomic, strong) NSIndexPath *datePickerIndexPath;

@property BOOL calendarShown;
@property BOOL calendar2Shown;
@property BOOL calendar3Shown;

@property (weak, nonatomic) IBOutlet UIView *socialView;

@property NSDate *openDate;
@property NSDate *endDate;

@property STAlertView *alertView;
@end

@implementation DHTempDisabilityViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Калькулятор";
    
    self.firstBlockQuestions = [[NSMutableArray alloc]initWithArray:@[@"Дата выдачи б/листа (госпитализации):",
                                                                     @"",
                                                                     @"На б/листе",
                                                                     @"Всего"]];
    
    self.firstBlockDates = [[NSMutableArray alloc]initWithObjects:@"Редактировать",@"",@"",@"", nil];
    
    
    self.secondBlockQuestions = [[NSMutableArray alloc]initWithArray: @[@"Дата ВК:",
                                                                       @"",
                                                                       @"До ВК осталось",
                                                                       @"ВК состоится",
                                                                       @"Добавить в календарь?"]];
    
    self.secondBlockDates = [[NSMutableArray alloc]initWithObjects:@"",@"",@"",@"",@"", nil];

    
    self.thirdBlockQuestions = [[NSMutableArray alloc]initWithArray:@[@"Дата закрытия б/листа:",
                                                                     @"",
                                                                     @"Длительность б/листа составит",
                                                                     @"До закрытия б/листа осталось",
                                                                      @"Число койко-дней составит"]];
    
    self.thirdBlockDates = [[NSMutableArray alloc]initWithObjects:@"Редактировать",@"",@"Редактировать",@"",@"", nil];

    
    self.needToShowAllFirstBlock = NO;
    self.needToShowAllSecondBlock = NO;
    self.needToShowAllThirdBlock = NO;
    
    self.calendarShown = NO;
    self.calendar2Shown = NO;
    self.calendar3Shown = NO;
    
    self.needToShowSecondBlock = NO;
    
    _clearButton.layer.cornerRadius = 5;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    [self updateTableViewHeight];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

-(CGFloat)heightForTableView
{
    CGSize size;
    CGFloat height;
    for (int i = 0 ; i <self.firstBlockQuestions.count ; i++)
    {
        NSString *string = self.firstBlockQuestions[i];
        if (self.needToShowAllFirstBlock != NO)
        {
            if ([string length])
            {
                size = [string sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(211, 999)];
                height += size.height+23;
            }
        } else if (i == 0)
        {
            size = [string sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(211, 999)];
            height += size.height+23;
        }
    }
    if (self.needToShowSecondBlock != NO)
    {
        height += 10;
        for (int i = 0 ; i <self.secondBlockQuestions.count ; i++)
        {
            NSString *string = self.secondBlockQuestions[i];
           
                if (self.needToShowAllSecondBlock != NO)
                {
                    if ([string length])
                    {
                        size = [string sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(211, 999)];
                        height += size.height+23;
                    }
                } else if ((i==0)||(i == 2))
                {
                    size = [string sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(211, 999)];
                    height += size.height+23;
                }
            }
            
    }
    if (self.needToShowSecondBlock != NO)
    {
        height +=10;
        for (int i = 0 ; i <self.thirdBlockQuestions.count ; i++)
        {
            NSString *string = self.thirdBlockDates[i];
           if (([string length])&&([self.thirdBlockQuestions[i] length]))
           {
                if (self.needToShowAllThirdBlock != NO)
                {
                    if ([string length])
                    {
                        size = [string sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(320, 999)];
                        height += size.height+23;
                    }
                } else if ((i==0)||(i == 2))
                {
                    size = [string sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(320, 999)];
                    height += size.height+23;
                }
           }
        }
            
    }
    
    if (self.calendarShown||self.calendar2Shown||self.calendar3Shown)
    {
        height +=200;
    }
    
    return height+10;
}

-(void)updateTableViewHeight
{
    CGRect frame = self.dateTableView.frame;
    frame.size.height = [self heightForTableView];
    self.dateTableView.frame = frame;
//    [self.dateTableView layoutIfNeeded];

    self.contentView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.dateTableView.frame.size.height+self.dateTableView.frame.origin.y + self.socialView.frame.size.height + 10);
    
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

#pragma mark - TableView Delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0: return self.firstBlockQuestions.count;break;
        case 1: return self.secondBlockQuestions.count; break;
        case 2: return self.thirdBlockQuestions.count; break;
    
        default:
            break;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"dateCell";
    
    DHTemDisabilityTableViewCell *cell = (DHTemDisabilityTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DHTemDisabilityTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.datePicker.hidden = YES;
    [cell.answerLabel setText:@""];
    cell.calendarIcon.hidden = YES;
    switch (indexPath.section) {
        case 0:
        {
            [cell.questionLabel setText:self.firstBlockQuestions[indexPath.row]];
            [cell.dateCell setText:self.firstBlockDates[indexPath.row]];
            if (indexPath.row > 1)
            {
                [cell.answerLabel setText:[NSString stringWithFormat:@"%@ %@",self.firstBlockQuestions[indexPath.row],self.firstBlockDates[indexPath.row]]];
                [cell.dateCell setText:@""];
                [cell.questionLabel setText:@""];

            }
            if (![self.firstBlockQuestions[indexPath.row] length])
            {
                [cell setCell];
                [cell.datePicker addTarget:self
                                    action:@selector(datePickerValueChanged:)
                          forControlEvents:UIControlEventValueChanged];
                cell.dateCell.hidden = YES;
            }
            break;
        } case 1:
        {

            [cell.questionLabel setText:self.secondBlockQuestions[indexPath.row]];
            NSLog(@"%i",indexPath.row);
            [cell.dateCell setText:self.secondBlockDates[indexPath.row]];
            if (indexPath.row > 1)
            {
                [cell.answerLabel setText:[NSString stringWithFormat:@"%@ %@",self.secondBlockQuestions[indexPath.row],self.secondBlockDates[indexPath.row]]];
                
                [cell.dateCell setText:@""];
                [cell.questionLabel setText:@""];
                if (indexPath.row == 4)
                {
                    cell.calendarIcon.hidden = NO;
                    cell.calendarIcon.center = CGPointMake(50, cell.answerLabel.center.y);
                }
                
            }

            if (![self.secondBlockQuestions[indexPath.row] length])
            {
                [cell setCell];

                [cell.datePicker addTarget:self
                                    action:@selector(datePicker2ValueChanged:)
                          forControlEvents:UIControlEventValueChanged];
                cell.dateCell.hidden = YES;
            }
            

            break;
        } case 2:
        {

            [cell.questionLabel setText:self.thirdBlockQuestions[indexPath.row]];
            [cell.dateCell setText:self.thirdBlockDates[indexPath.row]];
            if (indexPath.row == 0)
            {
                if (![self.thirdBlockDates[0] isEqualToString:@"Редактировать"])
                {
                    [cell.answerLabel setText:[NSString stringWithFormat:@"Дата закрытия %@",self.thirdBlockDates[0]]];
                    [cell.questionLabel setText:@""];
                    [cell.dateCell setText:@""];
                }
            }else if (indexPath.row == 2)
            {
                if (![self.thirdBlockDates[2] isEqualToString:@"Редактировать"])
                {
                    [cell.answerLabel setText:[NSString stringWithFormat:@"%@ %@",self.thirdBlockQuestions[indexPath.row],self.thirdBlockDates[indexPath.row]]];
                    [cell.dateCell setText:@""];
                    [cell.questionLabel setText:@""];
                } else {
                    [cell.questionLabel setText:@"Длительность б/листа"];

                }
            }
            else if (indexPath.row > 1)
            {
                [cell.answerLabel setText:[NSString stringWithFormat:@"%@ %@",self.thirdBlockQuestions[indexPath.row],self.thirdBlockDates[indexPath.row]]];
                [cell.dateCell setText:@""];
                [cell.questionLabel setText:@""];
                
            }

            if (![self.thirdBlockQuestions[indexPath.row] length])
            {
                [cell setCell];

                [cell.datePicker addTarget:self
                                    action:@selector(datePicker3ValueChanged:)
                          forControlEvents:UIControlEventValueChanged];
                
            }
            break;
        }
        default:
            break;
    }
    
    cell.dateCell.hidden = NO;
    

    CGSize size = [cell.questionLabel.text sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(211, 999)];
    CGRect frame = cell.questionLabel.frame;
    frame.size = size;
    cell.questionLabel.frame = frame;
    
    cell.dateCell.center = CGPointMake(cell.dateCell.center.x, cell.questionLabel.center.y);
    cell.clipsToBounds = YES;
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize size;

    switch (indexPath.section) {
        case 0:
        {
            size = [self.firstBlockQuestions[indexPath.row] sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(211, 999)];
            if (![self.firstBlockQuestions[indexPath.row] length])
            {
                if (self.calendarShown != NO)
                {
                    return 200;
                } else {
                    return 0;
                }
            }
            if ((!self.needToShowAllFirstBlock)&&(indexPath.row != 0))
            {
                return 0;
            }
            
            return size.height+23;

            break;
        } case 1:
        {
            size = [self.secondBlockQuestions[indexPath.row] sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(211, 999)];
            
            if (![self.firstBlockDates[2] length])
            {
                return 0;
            }
            
            if (![self.secondBlockQuestions[indexPath.row] length])
            {
                if (self.calendar2Shown != NO)
                {
                    return 200;
                } else {
                    return 0;
                }
            }
            if ((!self.needToShowAllSecondBlock)&&(indexPath.row != 0)&&(indexPath.row != 2))
            {
                return 0;
            }
            
            return size.height+23;

            break;
        } case 2:
        {
            size = [self.thirdBlockQuestions[indexPath.row] sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(211, 999)];
            if (![self.firstBlockDates[2] length])
            {
                return 0;
            }
            if (indexPath.row == 3)
            {
                if ([self.thirdBlockQuestions[3] isEqualToString:@""])
                {
                    return 0;
                }
            }
            if (![self.thirdBlockQuestions[indexPath.row] length])
            {
                if (self.calendar3Shown != NO)
                {
                    return 200;
                } else {
                    return 0;
                }
            }
            
            if ((!self.needToShowAllThirdBlock)&&(indexPath.row != 0)&&(indexPath.row != 2))
            {
                return 0;
            }
            size = [self.thirdBlockDates[indexPath.row] sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(320, 999)];
            return size.height+23;
            break;
        }
        default:
            break;
    }
    return 0;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
            if (indexPath.row == 0)
            {
                self.calendarShown = !self.calendarShown;
                self.calendar2Shown = NO;
                self.calendar3Shown = NO;
                self.needToShowAllFirstBlock = YES;
                self.needToShowSecondBlock = YES;
                if (![self.firstBlockDates[2] length])
                {
                    [self setDatesForFirstBlockStartDate:[NSDate date]];
                }
                [tableView beginUpdates];
                [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:0],[NSIndexPath indexPathForItem:2 inSection:0],[NSIndexPath indexPathForItem:3 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
                [tableView endUpdates];
                [self updateTableViewHeight];

            }
            break;
        case 1:
            if (indexPath.row == 0)
            {
                self.calendarShown = NO;
                self.calendar2Shown = !self.calendar2Shown;
                self.calendar3Shown = NO;
                self.needToShowAllSecondBlock = YES;
                [tableView beginUpdates];
                [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:1],[NSIndexPath indexPathForItem:2 inSection:1],[NSIndexPath indexPathForItem:3 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
                [tableView endUpdates];
                [self updateTableViewHeight];

            } else if (indexPath.row == 2)
            {
                self.calendarShown = NO;
                self.calendar2Shown = NO;
                self.calendar3Shown = NO;
                self.needToShowAllSecondBlock = YES;
                [tableView beginUpdates];
                [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:1],[NSIndexPath indexPathForItem:2 inSection:1],[NSIndexPath indexPathForItem:3 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
                [tableView endUpdates];
                [self updateTableViewHeight];
                self.alertView = [[STAlertView alloc] initWithTitle:@"Количество дней"
                                                          message:@"Введите количество дней до ВК"
                                                    textFieldHint:@"Редактировать"
                                                   textFieldValue:nil
                                                cancelButtonTitle:@"Отмена"
                                                 otherButtonTitle:@"Ок"
                                
                                                cancelButtonBlock:^{
                                                   
                                                } otherButtonBlock:^(NSString * result){
                                                    NSDateComponents *kekDateComponents = [NSDateComponents new];
                                                    kekDateComponents.day = [result intValue]+1;
                                                    NSDate *kekDate = [[NSCalendar currentCalendar]dateByAddingComponents:kekDateComponents
                                                                                                                   toDate:[NSDate date]
                                                                                                                  options:0];
                                                    [self setDatesForSecondBlockDate:kekDate];
                                                    [self.dateTableView reloadData];
                                                }];
                self.alertView.alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
                [[self.alertView.alertView textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeDecimalPad];
                
                [self.alertView show];

            }
            
            else if (indexPath.row == 4)
            {
                EKEventStore *store = [[EKEventStore alloc] init];
                [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
                    if (granted)
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self createEventAndPresentViewController:store];
                        });
                    }
                }];
            }
            
            break;
        case 2:
        {
            if (indexPath.row == 0)
            {
                self.calendarShown = NO;
                self.calendar2Shown = NO;
                self.calendar3Shown = !self.calendar3Shown;
                self.needToShowAllThirdBlock = YES;
                if (![self.thirdBlockDates[1] length])
                {
                    if ([self.thirdBlockDates[0] isEqualToString:@"Редактировать"])
                    {
                        [self setDatesForThirdBlockDate:[NSDate date]];
                    } else {
                        [self setDatesForThirdBlockDate:self.endDate];
                    }
                }
                [tableView beginUpdates];
                [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:2],[NSIndexPath indexPathForItem:2 inSection:2],[NSIndexPath indexPathForItem:3 inSection:2],[NSIndexPath indexPathForItem:4 inSection:2]] withRowAnimation:UITableViewRowAnimationAutomatic];
                [tableView endUpdates];
                [self updateTableViewHeight];
            } else if (indexPath.row == 2)
            {
                self.calendarShown = NO;
                self.calendar2Shown = NO;
                self.calendar3Shown = NO;
                self.needToShowAllThirdBlock = YES;
                [tableView beginUpdates];
                [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForItem:0 inSection:1],[NSIndexPath indexPathForItem:2 inSection:1],[NSIndexPath indexPathForItem:3 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
                [tableView endUpdates];
                [self updateTableViewHeight];
                self.alertView = [[STAlertView alloc] initWithTitle:@"Длительность б/листа"
                                                            message:@"Введите количество дней на б/листе"
                                                      textFieldHint:@"Редактировать"
                                                     textFieldValue:nil
                                                  cancelButtonTitle:@"Отмена"
                                                   otherButtonTitle:@"Ок"
                                  
                                                  cancelButtonBlock:^{
                                                      
                                                  } otherButtonBlock:^(NSString * result){
                                                      NSDateComponents *kekDateComponents = [NSDateComponents new];
                                                      kekDateComponents.day = [result intValue];
                                                      NSDate *kekDate = [[NSCalendar currentCalendar]dateByAddingComponents:kekDateComponents
                                                                                                                     toDate:self.openDate
                                                                                                                    options:0];
                                                      [self setDatesForThirdBlockDate:kekDate];
                                                      [self.dateTableView reloadData];
                                                      [self updateTableViewHeight];

                                                  }];
                self.alertView.alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
                [[self.alertView.alertView textFieldAtIndex:0] setKeyboardType:UIKeyboardTypeDecimalPad];
                
                [self.alertView show];
            }
            break;
                
        }
        default:
            break;
    }
    
//    [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];

}

#pragma mark - Calendar Event

- (void)createEventAndPresentViewController:(EKEventStore *)store
{
    EKEvent *event = [self findOrCreateEvent:store];
    
    EKEventEditViewController *controller = [[EKEventEditViewController alloc] init];
    controller.event = event;
    controller.eventStore = store;
    controller.editViewDelegate = self;
    
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)eventEditViewController:(EKEventEditViewController *)controller didCompleteWithAction:(EKEventEditViewAction)action
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (EKEvent *)findOrCreateEvent:(EKEventStore *)store
{
    NSString *title = @"Клинико-экспертная комиссия";
    
    EKEvent *event = [EKEvent eventWithEventStore:store];
    event.title = title;
    event.calendar = [store defaultCalendarForNewEvents];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd.MM.yy"];
    event.startDate = [dateFormat dateFromString:self.secondBlockDates[0]]; //today
    event.allDay = YES;
    event.endDate = [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
    
    return event;
}

-(void)clear:(UIButton *)sender
{
    [self.firstBlockDates removeAllObjects];
    [self.secondBlockDates removeAllObjects];
    [self.thirdBlockDates removeAllObjects];
    
    [self.firstBlockDates addObjectsFromArray:@[@"Редактировать",@"",@"",@""]];
    [self.secondBlockDates addObjectsFromArray:@[@"",@"",@"",@"",@""]];
    [self.thirdBlockDates addObjectsFromArray:@[@"Редактировать",@"",@"Редактировать",@"",@""]];

    self.needToShowAllFirstBlock = NO;
    self.needToShowAllSecondBlock = NO;
    self.needToShowAllThirdBlock = NO;
    
    self.calendarShown = NO;
    self.calendar2Shown = NO;
    self.calendar3Shown = NO;
    
    self.needToShowSecondBlock = NO;
    
    [self.dateTableView reloadData];
    [self updateTableViewHeight];


}

#pragma mark - Dates

-(void)datePickerValueChanged:(UIDatePicker *)datePicker
{
    if ([datePicker.date timeIntervalSinceNow] > 0)
    {
        datePicker.date = [NSDate date];
    } else {
        [self setDatesForFirstBlockStartDate:[datePicker date]];
        [self.dateTableView reloadData];
    }
}

-(void)datePicker2ValueChanged:(UIDatePicker *)datePicker
{
    [self setDatesForSecondBlockDate:[datePicker date]];
    [self.dateTableView reloadData];
}

-(void)datePicker3ValueChanged:(UIDatePicker *)datePicker
{
    [self setDatesForThirdBlockDate:[datePicker date]];
    [self.dateTableView reloadData];
    [self updateTableViewHeight];
}

-(void)setDatesForFirstBlockStartDate:(NSDate*)startDate
{
    
    NSString *dateString = [NSDateFormatter localizedStringFromDate:startDate
                                                          dateStyle:NSDateFormatterShortStyle
                                                          timeStyle:NSDateFormatterNoStyle];
    
    self.openDate = startDate;
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:[NSDate date]
                                                         options:0];
    NSInteger daysString = [components day] + 1;

    
    [self.firstBlockDates replaceObjectAtIndex:0 withObject:dateString];
    [self.firstBlockDates replaceObjectAtIndex:2 withObject:[NSString stringWithFormat:@"%li %@",(long)daysString,[self formatDay:daysString]]];
    [self.firstBlockDates replaceObjectAtIndex:3 withObject:[NSString stringWithFormat:@"%li койко-%@",(long)daysString-1,[self formatDay:daysString-1]]];
    
    NSDateComponents *kekDateComponents = [NSDateComponents new];
    kekDateComponents.day = 14;
    NSDate *kekDate = [[NSCalendar currentCalendar]dateByAddingComponents:kekDateComponents
                                                                   toDate: startDate
                                                                  options:0];
    [self setDatesForSecondBlockDate:kekDate];
}

-(void)setDatesForSecondBlockDate:(NSDate*)kekDate
{
    NSString *dateString = [NSDateFormatter localizedStringFromDate:kekDate
                                                          dateStyle:NSDateFormatterShortStyle
                                                          timeStyle:NSDateFormatterNoStyle];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:[NSDate date]
                                                          toDate:kekDate
                                                         options:0];
    NSInteger daysString = [components day];

    NSString *longDateString = [NSDateFormatter localizedStringFromDate:kekDate
                                                          dateStyle:NSDateFormatterLongStyle
                                                          timeStyle:NSDateFormatterNoStyle];
    NSDateFormatter *weekday = [[NSDateFormatter alloc] init];
    [weekday setDateFormat: @"EEE"];
    longDateString = [longDateString stringByAppendingString:[NSString stringWithFormat:@", %@", [weekday stringFromDate:kekDate]]];
    
    [self.secondBlockDates replaceObjectAtIndex:0 withObject:dateString];
    [self.secondBlockDates replaceObjectAtIndex:2 withObject:[NSString stringWithFormat:@"%li %@",(long)daysString,[self formatDay:daysString]]];
    [self.secondBlockDates replaceObjectAtIndex:3 withObject:longDateString];

}

-(void)setDatesForThirdBlockDate:(NSDate*)endDate
{

    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:self.openDate
                                                          toDate:endDate
                                                         options:0];
    NSInteger daysString = [components day] + 1;
    
    NSDateComponents *components2 = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:[NSDate date]
                                                          toDate:endDate
                                                         options:0];
    NSInteger endDaysString = [components2 day] + 1;
    self.endDate = endDate;
    NSString *longDateString = [NSDateFormatter localizedStringFromDate:endDate
                                                              dateStyle:NSDateFormatterLongStyle
                                                              timeStyle:NSDateFormatterNoStyle];
    NSDateFormatter *weekday = [[NSDateFormatter alloc] init];
    [weekday setDateFormat: @"EEE"];
    longDateString = [longDateString stringByAppendingString:[NSString stringWithFormat:@", %@", [weekday stringFromDate:endDate]]];
    if ([endDate timeIntervalSinceNow] - [[NSDate date]timeIntervalSinceNow] < 0)
    {
        [self.thirdBlockQuestions replaceObjectAtIndex:2 withObject:@"Сроки б/листа составили"];
        [self.thirdBlockQuestions replaceObjectAtIndex:3 withObject:@""];
        [self.thirdBlockQuestions replaceObjectAtIndex:4 withObject:@"Число койко дней составило"];
        
    } else {
        [self.thirdBlockQuestions replaceObjectAtIndex:2 withObject:@"Длительность б/листа составит"];
        [self.thirdBlockQuestions replaceObjectAtIndex:3 withObject:@"До закрытия б/листа осталось"];
        [self.thirdBlockQuestions replaceObjectAtIndex:4 withObject:@"Число койко-дней составит"];
    }
    
    [self.thirdBlockDates replaceObjectAtIndex:0 withObject:longDateString];
    [self.thirdBlockDates replaceObjectAtIndex:2 withObject:[NSString stringWithFormat:@"%li %@",(long)daysString,[self formatDay:daysString]]];
    [self.thirdBlockDates replaceObjectAtIndex:3 withObject:[NSString stringWithFormat:@"%li %@",(long)endDaysString,[self formatDay:endDaysString]]];
    [self.thirdBlockDates replaceObjectAtIndex:4 withObject:[NSString stringWithFormat:@"%i %@",daysString-1,[self formatDay:daysString-1]]];
}

-(NSString *)formatDay:(NSInteger)daysCount
{
    NSString *sEnding;
    NSArray *aEndings = [[NSArray alloc]initWithObjects:@"день",@"дня",@"дней", nil];
    daysCount = daysCount % 100;
    if (daysCount>=11 && daysCount<=19) {
        sEnding=aEndings[2];
    }
    else {
        int i = daysCount % 10;
        switch (i)
        {
            case (1): sEnding = aEndings[0]; break;
            case (2):
            case (3):
            case (4): sEnding = aEndings[1]; break;
            default: sEnding = aEndings[2];
        }
    }
    return sEnding;
}
@end
