//
//  DHApacheTextFieldTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import "DHApacheTextFieldTableViewCell.h"
#import "UIColor+DHfromHex.h"

@implementation DHApacheTextFieldTableViewCell

-(void)setCellWithQuestion:(DHApacheQuestionModel *)question
{
    
    self.question = question;
    
    self.textField.delegate = self;
    self.textField.floatingLabelActiveTextColor = [UIColor colorwithHexString:@"4CD964" alpha:1.0f];
    self.textField.floatingLabelInactiveTextColor = [UIColor lightGrayColor];
    self.textField.defaultPlaceholderColor = [UIColor lightGrayColor];
    
    if (question.answer)
    {
        [self.textField setText:question.answer.title];
    }
    
    [self.textField setPlaceholder:question.title];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length > 0)
    {
        DHApacheAnswerModel *answer = [DHApacheAnswerModel new];
        answer.title = textField.text;
        self.question.answer = answer;
    }
}
@end
