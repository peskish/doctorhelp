//
//  DHOtpuskViewModel.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 18.01.16.
//  Copyright © 2016 Ptenster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DHOtpuskViewModel : NSObject

@property NSString *cellType;
@property NSString *cellName;
@property CGFloat cellHeight;
@property NSString *text;
@property NSString *answer;

+(NSArray *)setDatesArray;

@end
