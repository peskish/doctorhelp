//
//  DHGRACEViewController.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 16.01.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"
#import "VCRadioButton.h"

@interface DHGRACEViewController : DHViewController

@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

- (IBAction)calc:(UIButton *)sender;
- (IBAction)clear:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextField *resultField;

@property (weak, nonatomic) IBOutlet VCRadioButton *btn1;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn4;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn5;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn6;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn7;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn8;

@property (weak, nonatomic) IBOutlet VCRadioButton *btn2_1;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2_2;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2_3;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2_4;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2_5;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2_6;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn2_7;

@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_1;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_2;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_3;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_4;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_5;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_6;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn3_7;

@property (weak, nonatomic) IBOutlet VCRadioButton *btn4_1;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn4_2;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn4_3;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn4_4;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn4_5;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn4_6;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn4_7;

@property (weak, nonatomic) IBOutlet VCRadioButton *btn5_1;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn5_2;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn5_3;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn5_4;

@property (weak, nonatomic) IBOutlet VCRadioButton *btn6_1;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn6_2;

@property (weak, nonatomic) IBOutlet VCRadioButton *btn7_1;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn7_2;

@property (weak, nonatomic) IBOutlet VCRadioButton *btn8_1;
@property (weak, nonatomic) IBOutlet VCRadioButton *btn8_2;
@end
