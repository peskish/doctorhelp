//
//  DHApacheCheckBoxTableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 19.12.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DHApacheCheckBoxTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *checkboxImageView;
@property (weak, nonatomic) IBOutlet UILabel *questionTitleLabel;

-(void)setCellWithQuestion:(DHApacheQuestionModel *)question;

@end
