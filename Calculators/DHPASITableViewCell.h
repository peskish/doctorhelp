//
//  DHPASITableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 26.05.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DHPASITableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *choosedImaged;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *result;

@end
