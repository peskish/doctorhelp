//
//  DHQtCorrectCalcViewController.m
//  DoctorHelp
//
//  Created by Aliona on 02/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHQtCorrectCalcViewController.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface DHQtCorrectCalcViewController () 

@property (weak, nonatomic) IBOutlet UITextField *qtResultTextField;

@end

@implementation DHQtCorrectCalcViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.title = @"Калькулятор";
    
    _switcher.selectedSegmentIndex = 0;
    _result.hidden = YES;
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    fullScreenView = _mainScroll.frame;
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Qt correct calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)correctQtForQt:(float)qt andTime:(float)time rr:(BOOL)rr
{
    double result;
    
    if (rr) {
        result = qt / sqrt(time);
    } else {
        result = qt / sqrt(60/time);
    }
    
    NSString *resultString = [NSString stringWithFormat:@"%.02f", result];
    self.qtResultTextField.text = resultString;
    _result.hidden = NO;
    _result.text = resultString;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{

}

- (IBAction)switchValue:(id)sender {
    if(_switcher.selectedSegmentIndex) {
        _time.placeholder = @"RR";
        _timeLabel.text = @"RR";
    } else {
        _time.placeholder = @"ЧСС";
        _timeLabel.text = @"ЧСС";
    }
}

- (IBAction)calc:(UIButton *)sender {
    double qt = [[_qt.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    double time = [[_time.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    BOOL rr = (BOOL)_switcher.selectedSegmentIndex;
    if(![self showAlert]) {
        [self correctQtForQt:qt andTime:time rr:rr];
    }
}

- (IBAction)clear:(UIButton *)sender {
    _qt.text = @"";
    _time.text = @"";
    _result.text = @"";
    _result.hidden = YES;
    self.qtResultTextField.text = @"";
    _switcher.selectedSegmentIndex = 0;
}

- (BOOL)showAlert
{
    DHAlert *alert = [DHAlert new];
    NSArray *array = [[NSArray alloc] initWithObjects:_qt.text, _time.text, nil];
    [alert initWithViewController:self andArray:array];
    return alert.alertShown;
}

// Venikov added

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#define kOFFSET_FOR_KEYBOARD 0;


-(void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSLog(@"keyboard frame raw %@", NSStringFromCGRect(keyboardFrame));
    self.keyboardHeight = keyboardFrame.size.height;
    CGRect rect = fullScreenView;
    rect.size.height -= (self.keyboardHeight);
    
    self.mainScroll.frame = rect;
    
}

-(void)keyboardWillHide
{
    
    self.mainScroll.frame = fullScreenView;
    
}

//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

@end
