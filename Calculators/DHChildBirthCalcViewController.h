//
//  DHChildBirthCalcViewController.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 27.01.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"

@interface DHChildBirthCalcViewController : DHViewController
@property (weak, nonatomic) IBOutlet UISegmentedControl *selector;

@property (weak, nonatomic) IBOutlet UILabel *firstLabel;
@property (weak, nonatomic) IBOutlet UITextField *firstField;

@property (weak, nonatomic) IBOutlet UILabel *secondLabel;
@property (weak, nonatomic) IBOutlet UITextField *weeksField;
@property (weak, nonatomic) IBOutlet UITextField *daysField;
@property (weak, nonatomic) IBOutlet UILabel *divider;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *firstResultLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondResultLabel;

@property (weak, nonatomic) IBOutlet UIButton *getResultButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;
@end
