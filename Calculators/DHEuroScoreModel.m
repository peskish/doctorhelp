//
//  DHEuroScoreModel.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 11.03.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHEuroScoreModel.h"

@implementation DHEuroScoreModel

+(NSArray *)fillEuroScoreModel
{
    NSMutableArray *questions = [[NSMutableArray alloc]init];
    
    DHEuroScoreModel *age = [[DHEuroScoreModel alloc]init];
    age.question = @"Возраст";
    age.answer = 0;
    [questions addObject:age];
    
    DHEuroScoreModel *gender = [[DHEuroScoreModel alloc]init];
    gender.question = @"Пол";
    gender.answer = 0;
    gender.maxMark = 1;
    gender.beta = 0.33;
    [questions addObject:gender];
    
    DHEuroScoreModel *question1 = [[DHEuroScoreModel alloc]init];
    question1.question = @"Хронические заболевания легких";
    question1.answer = 0;
    question1.help = @"Длительное использование бронходилататоров или стероидов при хронических заболеваниях легких";
    question1.maxMark = 1;
    question1.beta = 0.49;
    [questions addObject:question1];
    
    DHEuroScoreModel *question2 = [[DHEuroScoreModel alloc]init];
    question2.question = @"Поражения периферических артерий";
    question2.answer = 0;
    question2.help = @"Перемежающая хромота, окклюзия или стеноз сонной артерии >50%, перенесенные или планируемые операции на брюшной аорте, артериях нижних конечностей или сонных артериях";
    question2.maxMark = 2;
    question2.beta = 0.65;
    [questions addObject:question2];
    
    DHEuroScoreModel *question3 = [[DHEuroScoreModel alloc]init];
    question3.question = @"Серьезные неврологические нарушения";
    question3.answer = 0;
    question3.help = @"Существенно нарушающие повседневную активность больного";
    question3.maxMark = 2;
    question3.beta = 1;
    [questions addObject:question3];
    
    DHEuroScoreModel *question33 = [[DHEuroScoreModel alloc]init];
    question33.question = @"Предшествовавшие кардиохирургические вмешательства";
    question33.answer = 0;
    question33.help = @"Требовавшие вскрытия перикарда";
    question33.maxMark = 3;
    question33.beta = 1;
    [questions addObject:question33];
    
    DHEuroScoreModel *question4 = [[DHEuroScoreModel alloc]init];
    question4.question = @"Содержание сывороточного креатинина более 200 мкмоль/л";
    question4.answer = 0;
    question4.maxMark = 2;
    question4.beta = 0.65;
    [questions addObject:question4];
    
    DHEuroScoreModel *question5 = [[DHEuroScoreModel alloc]init];
    question5.question = @"Септический эндокардит";
    question5.answer = 0;
    question5.help = @"Требующий приема антибиотиков";
    question5.maxMark = 3;
    question5.beta = 1.10;
    [questions addObject:question5];
    
    DHEuroScoreModel *question6 = [[DHEuroScoreModel alloc]init];
    question6.question = @"Критическое состояние перед операцией";
    question6.answer = 0;
    question6.help = @"Желудочковая тахикардия, состояние после дифибриляции желудочков и реанимационных мероприятий. Предоперационный массаж сердца и вентиляция легких, предоперационная интропная поддержка, интрааортальная баллонная контропульсация, предоперационная острая почечная недостаточность";
    question6.maxMark = 3;
    question6.beta = 0.91;
    [questions addObject:question6];
    
    DHEuroScoreModel *question7 = [[DHEuroScoreModel alloc]init];
    question7.question = @"Нестабильная стенокардия";
    question7.answer = 0;
    question7.help = @"Стенокардия в состоянии покоя, требующая внутривенного ввода нитратов";
    question7.maxMark = 2;
    question7.beta = 0.57;
    [questions addObject:question7];
    
    DHEuroScoreModel *question8 = [[DHEuroScoreModel alloc]init];
    question8.question = @"Дисфункция левого желудочка средней тяжести (ФВ ЛЖ 30-50%)";
    question8.answer = 0;
    question8.maxMark = 1;
    question8.beta = 0.42;
    [questions addObject:question8];
    
    DHEuroScoreModel *question9 = [[DHEuroScoreModel alloc]init];
    question9.question = @"Дисфункция левого желудочка тяжелая (ФВ ЛЖ < 30%)";
    question9.answer = 0;
    question9.maxMark = 3;
    question9.beta = 1.1;
    [questions addObject:question9];
    
    DHEuroScoreModel *question10 = [[DHEuroScoreModel alloc]init];
    question10.question = @"Перенесенный инфаркт миокарда";
    question10.answer = 0;
    question10.help = @"Менее чем за 90 дней до вмешательства";
    question10.maxMark = 2;
    question10.beta = 0.55;
    [questions addObject:question10];
    
    DHEuroScoreModel *question11 = [[DHEuroScoreModel alloc]init];
    question11.question = @"Легочная гипертензия";
    question11.answer = 0;
    question11.help = @"Систолическое давление в легочной артерии< 60  мм р.с.";
    question11.maxMark = 2;
    question11.beta = 0.77;
    [questions addObject:question11];
    
    DHEuroScoreModel *question12 = [[DHEuroScoreModel alloc]init];
    question12.question = @"Необходимость неотложной операции";
    question12.answer = 0;
    question12.help = @"В течение дня после обращения больного";
    question12.maxMark = 2;
    question12.beta = 0.71;
    [questions addObject:question12];
    
    DHEuroScoreModel *question13 = [[DHEuroScoreModel alloc]init];
    question13.question = @"Необходимость выполнения других операций, кроме КШ";
    question13.answer = 0;
    question13.maxMark = 2;
    question13.beta = 0.54;
    [questions addObject:question13];
    
    DHEuroScoreModel *question14 = [[DHEuroScoreModel alloc]init];
    question14.question = @"Операция на грудном отделе аорты";
    question14.answer = 0;
    question14.help = @"При патологиях дуги, восходящего или нисходящего отдела аорты";
    question14.maxMark = 3;
    question14.beta = 1.16;
    [questions addObject:question14];
    
    DHEuroScoreModel *question15 = [[DHEuroScoreModel alloc]init];
    question15.question = @"Постинфарктный разрыв межжелудочковой перегородки";
    question15.answer = 0;
    question15.maxMark = 4;
    question15.beta = 1.46;
    [questions addObject:question15];
    
    return questions;
}
@end
