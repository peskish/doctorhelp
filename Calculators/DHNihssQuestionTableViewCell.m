//
//  DHNihssQuestionTableViewCell.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 14.11.15.
//  Copyright © 2015 Ptenster. All rights reserved.
//

#import "DHNihssQuestionTableViewCell.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@implementation DHNihssQuestionTableViewCell

- (void)awakeFromNib {
    self.showPictureButton.layer.cornerRadius = 4;
    self.showSentenceButton.layer.cornerRadius = 4;
}

-(void)setCell:(DHNihssModel *)question
{
    self.question = question;
    [self.questionTitle setText:question.title];
    if (question.info)
    {
        self.questionButton.hidden = NO;
    } else {
        self.questionButton.hidden = YES;
    }
    
    if (question.additionalInfo.length > 0)
    {
        self.showSentenceButton.hidden = NO;
    } else {
        self.showSentenceButton.hidden = YES;
    }
    
    CGRect frame = self.questionTitle.frame;
    frame.size.height = [self findHeightForText:question.title havingWidth:frame.size.width andFont:self.questionTitle.font];
    self.questionTitle.frame = frame;
    
    if ([question.title isEqualToString:@"Оцените наличие дизартрии"])
    {
        [self.showSentenceButton setTitle:@"Показать слова" forState:UIControlStateNormal];
    } else {
        [self.showSentenceButton setTitle:@"Показать предложения" forState:UIControlStateNormal];

    }
    
    self.showPictureButton.hidden = !question.needPicture;
}
- (IBAction)questionButtonTapped:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"showNihssQuestionInfo" object:self.question.info];
}
- (IBAction)showSentencesButtonTapped:(id)sender
{
    UIButton *btn = sender;
    if ([btn.titleLabel.text isEqualToString:@"Показать предложения"])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"showSentences" object:self.question.additionalInfo];
    } else {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"showWords" object:self.question.additionalInfo];

        }
}
- (IBAction)showPictureButtonTapped:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"showPicture" object:nil];
}

- (CGFloat)findHeightForText:(NSString *)text havingWidth:(CGFloat)widthValue andFont:(UIFont *)font
{
    CGFloat result = font.pointSize + 4;
    if (text)
    {
        CGSize textSize = { widthValue, CGFLOAT_MAX };       //Width and height of text area
        CGSize size;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            //iOS 7
            CGRect frame = [text boundingRectWithSize:textSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{ NSFontAttributeName:font }
                                              context:nil];
            size = CGSizeMake(frame.size.width, frame.size.height+1);
        }
        else
        {
            //iOS 6.0
            size = [text sizeWithFont:font constrainedToSize:textSize lineBreakMode:NSLineBreakByWordWrapping];
        }
        result = MAX(size.height, result); //At least one row
    }
    return result;
}

@end
