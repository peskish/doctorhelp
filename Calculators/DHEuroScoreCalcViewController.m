//
//  DHEuroScoreCalcViewController.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 11.03.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "DHEuroScoreCalcViewController.h"
#import "DHEuroScoreModel.h"
#import "DHEuroScoreCell.h"
#import "MONPromptView.h"
#import "UIColor+DHfromHex.h"

@interface DHEuroScoreCalcViewController ()
@property NSMutableArray *firstBlockArray;
@property NSMutableArray *secondBlockArray;
@property NSMutableArray *thirdBlockArray;

@end

@implementation DHEuroScoreCalcViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Калькулятор";

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showHelp:) name:@"showEuroScoreHelp" object:nil];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"EuroScore calculator"];
    
    self.questions = [DHEuroScoreModel fillEuroScoreModel];
    
    self.firstBlockArray = [[NSMutableArray alloc]init];
    self.secondBlockArray = [[NSMutableArray alloc]init];
    self.thirdBlockArray = [[NSMutableArray alloc]init];
    
    self.calcButton.layer.cornerRadius = 5;
    self.clearButton.layer.cornerRadius = 5;
    
    for (int i = 0 ; i < 9 ; i++ )
    {
        [self.firstBlockArray addObject:self.questions[i]];
    }
    
    for (int i = 9 ; i < 14 ; i++)
    {
        [self.secondBlockArray addObject:self.questions[i]];
    }
    for (int i = 14 ; i < self.questions.count ; i++)
    {
        [self.thirdBlockArray addObject:self.questions[i]];
    }
    
    
    
    [self.tableView reloadData];
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.delegate = self;
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    self.tableView.scrollEnabled = NO;
}


-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.view.frame.size.width, self.questions.count*128+160 + 44*3);
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, 320, self.questions.count*128 + 44*3);
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

-(void)showHelp:(NSNotification *)notification
{
    DHEuroScoreModel *question = notification.userInfo[@"question"];
    NSDictionary *attributes = @{ kMONPromptViewAttribDismissButtonBackgroundColor: [UIColor colorwithHexString:@"4CD964" alpha:1.0f],
                                  kMONPromptViewAttribDismissButtonTextColor : [UIColor whiteColor] };
    MONPromptView *promptView = [[MONPromptView alloc] initWithTitle:question.question
                                                             message:question.help
                                                  dismissButtonTitle:@"ОК"
                                                          attributes:attributes];
    [promptView showInView:self.navigationController.view];
}

//=========================
#pragma mark - TABLEVIEW DELEGATE
//=========================

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
//    UIView *viewForHeader = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 44)];
    UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    nameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    nameLabel.numberOfLines = 0;
    switch (section) {
        case 0:
            nameLabel.text = @"Данные пациента";
            break;
        case 1:
            nameLabel.text = @"Кардиологические факторы риска";
            break;
        case 2:
            nameLabel.text = @"Хирургические факторы риска";
            break;
        default:
            break;
    }
    [nameLabel sizeToFit];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.center = CGPointMake(self.tableView.frame.size.width/2, 22);
    NSLog(@"%f",self.tableView.frame.size.width);
//    [viewForHeader addSubview:nameLabel];
    return nameLabel;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return self.firstBlockArray.count;
            break;
         case 1:
            return self.secondBlockArray.count;
            break;
            case 2:
            return self.thirdBlockArray.count;
        default:
            break;
    }
    return self.questions.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"questionCell";
    
    DHEuroScoreCell *cell = (DHEuroScoreCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DHEuroScoreCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    switch (indexPath.section) {
        case 0:
            [cell setCell:self.questions[indexPath.row]];
            break;
       case 1:
            [cell setCell:self.secondBlockArray[indexPath.row]];
            break;
        case 2:
            [cell setCell:self.thirdBlockArray[indexPath.row]];
            break;
        default:
            break;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 128;
}




//=====================

- (IBAction)calcButtonTapped:(id)sender
{
    int marks = 0;
    double sumOfBetas = 0;
    for (DHEuroScoreModel* question in self.questions)
    {
        if ([question.question isEqualToString:@"Возраст"])
        {
            if (question.answer > 59)
            {
                marks = marks + (59 - question.answer + 1);
                sumOfBetas += (59 - question.answer + 1)*0.06;
            } else {
                marks += 1;
                sumOfBetas += 0.06;
            }
        } else {
            marks += question.answer;
            if (question.answer != 0)
            {
                sumOfBetas += question.beta;
            }
        }
    }
    
    sumOfBetas = exp(-4.79 + sumOfBetas)/(1+exp(-4.79 +  sumOfBetas));
    
    [self.resultLabel setText:[NSString stringWithFormat:@"EuroSCORE %i %@\nВероятность смертельного исхода %.01f%% ",marks,[self formatMarks:marks],sumOfBetas*100]];
    [self.resultLabel sizeToFit];
    CGRect frame = self.resultLabel.frame;
    frame.size.width = 310;
    self.resultLabel.frame = frame;
    self.resultLabel.center = CGPointMake(self.view.center.x + 5, self.resultLabel.center.y);
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.view.frame.size.width, self.questions.count*128+200+ 44*3);
    } completion:^(BOOL finished) {
        self.mainScroll.contentSize = self.contentView.bounds.size;
    }];

}

-(NSString *)formatMarks:(NSInteger)marks
{
    NSString *sEnding;
    NSArray *aEndings = [[NSArray alloc]initWithObjects:@"балл",@"балла",@"баллов", nil];
    marks = marks % 100;
    if (marks>=11 && marks<=19) {
        sEnding=aEndings[2];
    }
    else {
        int i = marks % 10;
        switch (i)
        {
            case (1): sEnding = aEndings[0]; break;
            case (2):
            case (3):
            case (4): sEnding = aEndings[1]; break;
            default: sEnding = aEndings[2];
        }
    }
    return sEnding;
}

- (IBAction)clearButtonTapped:(id)sender
{
    [self.resultLabel setText:@""];
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.view.frame.size.width, self.questions.count*128+160+ 44*3);
    } completion:^(BOOL finished) {
        self.mainScroll.contentSize = self.contentView.bounds.size;
    }];
    self.questions = [DHEuroScoreModel fillEuroScoreModel];
    [self.tableView reloadData];
}

@end
