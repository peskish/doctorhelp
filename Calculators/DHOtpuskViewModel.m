//
//  DHOtpuskViewModel.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 18.01.16.
//  Copyright © 2016 Ptenster. All rights reserved.
//

#import "DHOtpuskViewModel.h"

@implementation DHOtpuskViewModel

+(NSArray *)setDatesArray
{
    DHOtpuskViewModel *datesHeader = [DHOtpuskViewModel new];
    datesHeader.cellName = @"otpuskSectionHeader";
    datesHeader.cellType = @"DHOtpuskSectionHeaderTableViewCell";
    datesHeader.cellHeight = 44;
    datesHeader.text = @"Даты отпуска";
    
    DHOtpuskViewModel *start = [DHOtpuskViewModel new];
    start.cellName = @"otpuskCell";
    start.cellType = @"DHOtpuskTableViewCell";
    start.cellHeight = 44;
    start.text = @"Первый день отпуска";
    
    DHOtpuskViewModel *finish = [DHOtpuskViewModel new];
    finish.cellName = @"otpuskCell";
    finish.cellType = @"DHOtpuskTableViewCell";
    finish.cellHeight = 44;
    finish.text = @"Последний день отпуска";
    
    NSArray *datesArray = [[NSArray alloc] initWithObjects:datesHeader,start,finish, nil];
    return datesArray;
}

@end
