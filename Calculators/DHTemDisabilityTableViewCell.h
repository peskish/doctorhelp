//
//  DHTemDisabilityTableViewCell.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 16.01.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DHTemDisabilityTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateCell;
@property (strong, nonatomic) UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *calendarIcon;
-(void)setCell;
@end
