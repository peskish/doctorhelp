//
//  DHIdealMassViewController.h
//  DoctorHelp
//
//  Created by Dmitry Venikov on 21/10/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"
@interface DHIdealMassViewController : DHViewController

@property (weak, nonatomic) IBOutlet UITextField *height;
@property (weak, nonatomic) IBOutlet UITextField *weight;
@property (weak, nonatomic) IBOutlet UITextField *chestCirc;
@property (weak, nonatomic) IBOutlet UITextField *handCirc;
@property (weak, nonatomic) IBOutlet UIButton *handHelpButton;
@property (weak, nonatomic) IBOutlet UITextField *waistCirc;
@property (weak, nonatomic) IBOutlet UIButton *waistHelpButton;
@property (weak, nonatomic) IBOutlet UITextField *hipsCirc;
@property (weak, nonatomic) IBOutlet UIButton *hipsHelpButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gender;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UITextField *imtTextField;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;
@property (weak, nonatomic) IBOutlet UITextField *robinsonTextField;
@property (weak, nonatomic) IBOutlet UITextField *brockTextField;
@property (weak, nonatomic) IBOutlet UITextField *brongardTextField;
@property (weak, nonatomic) IBOutlet UITextField *waistTextField;
@property (weak, nonatomic) IBOutlet UITextField *waistHipsTextField;

- (IBAction)calc:(UIButton *)sender;
- (IBAction)clear:(UIButton *)sender;

@end
