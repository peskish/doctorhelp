//
//  DHCreatininumCalcViewController.m
//  DoctorHelp
//
//  Created by Aliona on 02/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHCreatininumCalcViewController.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface DHCreatininumCalcViewController ()

@property (weak, nonatomic) IBOutlet UITextField *resultTextField;
@property (weak, nonatomic) IBOutlet UITextField *detailResultTextField;

@end

@implementation DHCreatininumCalcViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    fullScreenView = _mainScroll.frame;
    
    _resultSkf.hidden = YES;
    _resultXbp.hidden = YES;
    _unitsBlood.selectedSegmentIndex = 0;
    _unitsUrina.selectedSegmentIndex = 0;
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"Creatininum calculator"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Creatininum calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

#pragma mark - TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{

}

-(void)calculateCreatininum:(double)bloodCreatininum urina:(double)urinaCreatininum mok:(double)mok mkmBlood:(BOOL)mkmBlood mkmUrina:(BOOL)mkmUrina
{
    if (mkmBlood) {
        bloodCreatininum = bloodCreatininum / 10 * 113.12 / 10;
    }
    if (mkmUrina) {
        urinaCreatininum = urinaCreatininum / 10 * 113.12 / 10;
    }
    
    double skf = urinaCreatininum / bloodCreatininum * mok;
    int xbp = [self skfToXbp:skf];
    
//    _resultSkf.hidden = NO;
//    _resultXbp.hidden = NO;
//    
//    _resultSkf.text = [NSString stringWithFormat:@"СКФ %.2f",skf];
//    _resultXbp.text = [NSString stringWithFormat:@"СКФ %i",xbp];
    self.resultTextField.text = [NSString stringWithFormat:@"СКФ %.2f", skf];
    self.detailResultTextField.text = [NSString stringWithFormat:@"СКФ %i",xbp];
}

- (int)skfToXbp:(double)skf {
    if (skf < 15) {
        return 5;
    } else if (skf >= 15 && skf < 30) {
        return 4;
    } else if (skf >= 30 && skf < 60) {
        return 3;
    } else if (skf >= 60 && skf < 90) {
        return 2;
    } else if (skf >= 90) {
        return 1;
    }
    return 0;
}

- (IBAction)calc:(UIButton *)sender {
    double bloodCreatininum = [[_bloodCreatininum.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    double urinaCreatininum = [[_urinaCreatininum.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    double mok = [[_mok.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    BOOL mkmBlood = (BOOL)_unitsBlood;
    BOOL mkmUrina = (BOOL)_unitsUrina;
    if (![self showAlert]) {
        [self calculateCreatininum:bloodCreatininum urina:urinaCreatininum mok:mok mkmBlood:mkmBlood mkmUrina:mkmUrina];
    }
}

- (IBAction)clear:(UIButton *)sender {
    _bloodCreatininum.text = @"";
    _urinaCreatininum.text = @"";
    _mok.text = @"";
    _resultSkf.text = @"";
    _resultXbp.text = @"";
    self.resultTextField.text = @"";
    self.detailResultTextField.text = @"";
    _resultSkf.hidden = YES;
    _resultXbp.hidden = YES;
    _unitsBlood.selectedSegmentIndex = 0;
    _unitsUrina.selectedSegmentIndex = 0;
}

- (BOOL)showAlert
{
    DHAlert *alert = [DHAlert new];
    NSArray *array = [[NSArray alloc] initWithObjects:_bloodCreatininum.text, _urinaCreatininum.text, _mok.text, nil];
    [alert initWithViewController:self andArray:array];
    return alert.alertShown;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#define kOFFSET_FOR_KEYBOARD 40.0

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyboardHeight = keyboardFrame.size.height;
    CGRect rect = fullScreenView;
    rect.size.height -= (self.keyboardHeight);
    
    self.mainScroll.frame = rect;
    
}


-(void)keyboardWillHide
{
    self.mainScroll.frame = fullScreenView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

@end
