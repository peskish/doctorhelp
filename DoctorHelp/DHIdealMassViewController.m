//
//  DHIdealMassViewController.m
//  DoctorHelp
//
//  Created by Dmitry Venikov on 21/10/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//
//
//#import "DHIdealMassViewController.h"
//
//@interface DHIdealMassViewController ()
//
//@end
//
//@implementation DHIdealMassViewController
//
//
//
//@end

#import "DHIdealMassViewController.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

#import "MONPromptView.h"
#import "UIColor+DHfromHex.h"

@interface DHIdealMassViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *resultTextField;

@end

@implementation DHIdealMassViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    _gender.selectedSegmentIndex = 0;
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"OCK calculator"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"OCK calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

#pragma mark - TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{

}

#pragma mark - Calculate

- (void)calulateForFemale:(BOOL)female
               withHeight:(float)height
                   weight:(float)weight
                chestCirc:(float)chestCirc
                waistCirc:(float)waistCirc
                 handCirc:(float)handCirc
                 hipsCirc:(float)hipsCirc
{
    //ИМТ
    if (height)
    {
        if (weight)
        {
            double heightmeters = height/100;
            heightmeters = pow(heightmeters, 2);
            double imt = weight / heightmeters;
            [self calcImtResult:imt];
        }
        
        
        //ДЕВАЙН-РОБИНСОН
        double resultDevine;
        double resultRobinson;
        if (female) {
            resultDevine = 45.5 + 2.3 * (0.394 * height - 60.0);
            resultRobinson = 49.0 + 1.7 * (0.394 * height - 60.0);
        } else {
            resultDevine = 50.0 + 2.3 * (0.394 * height - 60.0);
            resultRobinson = 52.0 + 1.9 * (0.394 * height - 60.0);
        }
        self.resultTextField.text = [NSString stringWithFormat:@"%.2f кг", resultDevine];
        self.robinsonTextField.text = [NSString stringWithFormat:@"%.2f кг", resultRobinson];
    }
    
    //БРОК
    if ((height)&&(handCirc))
    {
        double brockWeight = height - 100;
        if (female)
        {
            if (handCirc < 15)
            {
                brockWeight = brockWeight*0.9;
            }
            else if (handCirc > 20)
            {
                brockWeight = brockWeight*1.1;

            }
            
            brockWeight = brockWeight * 0.95;
        } else {
            if (handCirc < 18)
            {
                brockWeight = brockWeight*0.9;

            } else if (handCirc > 20)
            {
                brockWeight = brockWeight*1.1;

            }
        }
        [self.brockTextField setText:[NSString stringWithFormat:@"%.2f кг",brockWeight]];

    }
    
    
    //БРОНГАРД
    if ((height)&&(chestCirc))
    {
        double brongard = (height * chestCirc)/240;
        [self.brongardTextField setText:[NSString stringWithFormat:@"%.2f кг",brongard]];
    }
    
    //Окружность талии
    if (waistCirc)
    {
        if (female)
        {
            if (waistCirc < 80)
            {
                [self.waistTextField setText:@"Норма"];
            } else if (waistCirc > 88)
            {
                [self.waistTextField setText:@"Показатель значительно повышен"];
            } else {
                [self.waistTextField setText:@"Показатель умеренно повышен"];
            }
        } else {
            if (waistCirc < 94)
            {
                [self.waistTextField setText:@"Норма"];

            } else if (waistCirc > 102)
            {
                [self.waistTextField setText:@"Показатель значительно повышен"];

            } else {
                [self.waistTextField setText:@"Показатель умеренно повышен"];

            }
        }
        
        //талия к бедрам
        if (hipsCirc)
        {
            double k = waistCirc/hipsCirc;
            if (female)
            {
                if (k < 0.85)
                {
                    [self.waistHipsTextField setText:@"Норма"];
                } else {
                    [self.waistHipsTextField setText:@"Показатель повышен"];
                }
            } else {
                if (k < 1)
                {
                    [self.waistHipsTextField setText:@"Норма"];
                } else {
                    [self.waistHipsTextField setText:@"Показатель повышен"];
                }
            }
        }
    }
}

-(void)calcImtResult:(double)imt
{
    if (imt < 18.5)
    {
        [self.imtTextField setText:[NSString stringWithFormat:@"%.1f - Дефицит массы тела",imt]];
        
    } else if (imt < 24.9)
    {
        [self.imtTextField setText:[NSString stringWithFormat:@"%.1f - Нормальная масса тела",imt]];
        
    } else if (imt < 29.9)
    {
        [self.imtTextField setText:[NSString stringWithFormat:@"%.1f - Повышенная масса тела",imt]];
        
    } else if (imt < 34.9)
    {
        [self.imtTextField setText:[NSString stringWithFormat:@"%.1f - Ожирение I степени",imt]];
        
    } else if (imt < 39.9)
    {
        [self.imtTextField setText:[NSString stringWithFormat:@"%.1f - Ожирение II степени",imt]];
        
    } else
    {
        [self.imtTextField setText:[NSString stringWithFormat:@"%.1f - Ожирение III степени",imt]];
        
    }
}

- (IBAction)calc:(UIButton *)sender {
    float height = [[_height.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    BOOL female = (BOOL)_gender.selectedSegmentIndex;
    float weight = [[_weight.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    float chestCirc = [[_chestCirc.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    float waistCirc = [[_waistCirc.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    float handCirc = [[_handCirc.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    float hipsCirc = [[_hipsCirc.text stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];

    [self calulateForFemale:female
                 withHeight:height
                     weight:weight
                  chestCirc:chestCirc
                  waistCirc:waistCirc
                   handCirc:handCirc
                   hipsCirc:hipsCirc];
}

- (IBAction)clear:(UIButton *)sender {
    _height.text = @"";
    self.robinsonTextField.text = @"";
    self.resultTextField.text = @"";
    _gender.selectedSegmentIndex = 0;
    self.imtTextField.text = @"";
    self.brockTextField.text = @"";
    self.brongardTextField.text = @"";
    self.waistTextField.text = @"";
    self.waistHipsTextField.text = @"";
}

- (BOOL)showAlert
{
    DHAlert *alert = [DHAlert new];
    NSArray *array = [[NSArray alloc] initWithObjects:_height.text, nil];
    [alert initWithViewController:self andArray:array];
    return alert.alertShown;
}
- (IBAction)showHandHelp:(id)sender
{
    [self showHelp:@"Измеряют в самом тонком месте"];
}
- (IBAction)showWeistHelp:(id)sender
{
    [self showHelp:@"Измеряют на середине расстояния между подреберьем и тазовой костью, по срединно-подмышечной линии"];

}

- (IBAction)showHipsHelp:(id)sender
{
    [self showHelp:@"Измеряют ниже больших бедренных бугров"];

}

-(void)showHelp:(NSString *)helpText
{
    NSDictionary *attributes = @{ kMONPromptViewAttribDismissButtonBackgroundColor: [UIColor colorwithHexString:@"4CD964" alpha:1.0f],
                                  kMONPromptViewAttribDismissButtonTextColor : [UIColor whiteColor] };
    MONPromptView *promptView = [[MONPromptView alloc] initWithTitle:@"Подсказка"
                                                             message:helpText
                                                  dismissButtonTitle:@"ОК"
                                                          attributes:attributes];
    [promptView showInView:self.navigationController.view];

}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#define kOFFSET_FOR_KEYBOARD 0.0

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSLog(@"keyboard frame raw %@", NSStringFromCGRect(keyboardFrame));
    self.keyboardHeight = keyboardFrame.size.height;
    CGRect rect = fullScreenView;
    rect.size.height -= (self.keyboardHeight);
    
    self.mainScroll.frame = rect;
    
}


-(void)keyboardWillHide
{

        self.mainScroll.frame = fullScreenView;
}


//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

@end
