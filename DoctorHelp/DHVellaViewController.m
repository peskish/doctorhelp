//
//  DHVellaViewController.m
//  DoctorHelp
//
//  Created by Dmitry Venikov on 21/10/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHVellaViewController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface DHVellaViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *resultTextField;
@property (strong, nonatomic) NSArray *contentsArray;
@property (strong, nonatomic) NSMutableArray *pointsArray;
@property (strong, nonatomic) NSArray *multiplesArray;
@property (strong, nonatomic) NSMutableArray *switchesArray;

@end

@implementation DHVellaViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    self.contentsArray = @[@"Симптомы тромбоза глубоких вен",
                           @"Тахикардия с ЧСС более 100 в 1 мин",
                           @"Иммобилизация (3 и более дней) или хирургическая операция за последние 4 недели",
                           @"Тромбоз глубоких вен в анамнезе",
                           @"Кровохарканье",
                           @"Наличие злокачественной опухоли",
                           @"Отсутствует другой диагноз, лучше объясняющий симптомы"];
    self.multiplesArray = @[@3, @1.5, @1.5, @1.5, @1, @1, @3];
    self.pointsArray = [[NSMutableArray alloc] initWithObjects:@0, @0, @0, @0, @0, @0, @0, nil];
    self.switchesArray = [NSMutableArray array];
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"Kalium calculator"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Vella calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

- (IBAction)calc:(UIButton *)sender
{
    float result = 0.0;
    for (int index = 0; index < [self.contentsArray count]; index++) {
        result += [self.multiplesArray[index] floatValue] * [self.pointsArray[index] floatValue];
    }
    NSString *resultText = @"";
    if (result < 2.0) {
        resultText = @"Низкая вероятность";
    } else if (result < 7) {
        resultText = @"Средняя вероятность";
    } else {
        resultText = @"Высокая вероятность";
    }
    self.resultTextField.text = resultText;
}

- (IBAction)clear:(UIButton *)sender
{
    for (int index = 0; index < [self.switchesArray count]; index++) {
        UISwitch *switchView = self.switchesArray[index];
        [switchView setOn:NO animated:YES];
        self.pointsArray[index] = @0;
    }
    self.resultTextField.text = @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2) {
        return 54.0;
    }
    return 44.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contentsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reusableString = [NSString stringWithFormat:@"VellaCell %d", indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableString];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
        [self.switchesArray addObject:switchView];
        switchView.tag = indexPath.row;
        [switchView addTarget:self action:@selector(switchWasChangedFromEnabled:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = switchView;
        cell.textLabel.font = [UIFont systemFontOfSize:13.0];
        cell.textLabel.text = self.contentsArray[indexPath.row];
        cell.textLabel.numberOfLines = 3;
    }
    return cell;
}

- (void)switchWasChangedFromEnabled:(id)sender
{
    UISwitch *switchView = sender;
    NSInteger tag = switchView.tag;
    self.pointsArray[tag] = [NSNumber numberWithBool:switchView.on];
}

@end
