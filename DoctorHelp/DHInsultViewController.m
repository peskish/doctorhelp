//
//  DHInsultViewController.m
//  DoctorHelp
//
//  Created by Dmitry Venikov on 21/10/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHInsultViewController.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface DHInsultViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *resultTextField;
@property (strong, nonatomic) NSArray *contentsArray;
@property (strong, nonatomic) NSMutableArray *pointsArray;
@property (strong, nonatomic) NSArray *multiplesArray;
@property (strong, nonatomic) NSMutableArray *switchesArray;

@end

@implementation DHInsultViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    self.contentsArray = @[@"Возраст от 65 до 74 лет",
                           @"Возраст 75 и старше",
                           @"Женский пол",
                           @"Сердечная недостаточность  или умеренная или выраженная систолическая дисфункция ЛЖ",
                           @"Артериальная гипертония",
                           @"Сахарный диабет",
                           @"Инсульт, ТИА, системный эмболизм в анамнезе",
                           @"Поражение периферических артерий или инфаркт миокарда в анамнезе"];
    self.multiplesArray = @[@1, @2, @1, @1, @1, @1, @1, @1];
    self.pointsArray = [[NSMutableArray alloc] initWithObjects:@0, @0, @0, @0, @0, @0, @0, @0, nil];
    self.switchesArray = [NSMutableArray array];
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"Kalium calculator"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Insult calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

- (IBAction)calc:(UIButton *)sender
{
    int result = 0;
    for (int index = 0; index < [self.contentsArray count]; index++) {
        result += [self.multiplesArray[index] integerValue] * [self.pointsArray[index] integerValue];
    }
    NSNumber *resultNumber = [NSNumber numberWithInt:result];
    NSDictionary *resultDict = @{@0 : @"0,84 %",
                                @1 : @"1,3 %",
                                @2 : @"2,3 %",
                                @3 : @"3,2 %",
                                @4 : @"4,0 %",
                                @5 : @"6,7 %",
                                @6 : @"9,8 %",
                                @7 : @"9,6 %",
                                @8 : @"6,7 %",
                                @9 : @"15,2 %",
                                @10 : @"15,2 %",
                                @11 : @"15,2 %"};
    NSString *resultText = [NSString stringWithFormat:@"%@", resultDict[resultNumber]];
    self.resultTextField.text = resultText;
}

- (IBAction)clear:(UIButton *)sender
{
    for (int index = 0; index < [self.switchesArray count]; index++) {
        UISwitch *switchView = self.switchesArray[index];
        [switchView setOn:NO animated:YES];
        self.pointsArray[index] = @0;
    }
    self.resultTextField.text = @"";
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellText = _contentsArray[indexPath.row];
    CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
    CGSize labelSize = [cellText sizeWithFont:[UIFont systemFontOfSize:15.0]
                            constrainedToSize:constraintSize
                                lineBreakMode:NSLineBreakByWordWrapping];
    
    return labelSize.height + 20;

//    if ( indexPath.row == 5 || indexPath.row == 6 || indexPath.row == 7) {
//        return 54.0;
//    }
//    return 44.0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.contentsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reusableString = [NSString stringWithFormat:@"InsultCell %li", (long)indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusableString];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
        [self.switchesArray addObject:switchView];
        switchView.tag = indexPath.row;
        [switchView addTarget:self action:@selector(switchWasChangedFromEnabled:) forControlEvents:UIControlEventValueChanged];

        cell.accessoryView = switchView;
        cell.textLabel.font = [UIFont systemFontOfSize:15.0];
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.text = self.contentsArray[indexPath.row];
        
    }
    
    
    return cell;
}

- (void)switchWasChangedFromEnabled:(id)sender
{
    UISwitch *switchView = sender;
    
    NSInteger tag = switchView.tag;
    
    if (tag == 0 || tag == 1)
    {
        NSInteger index = 0;
        if (tag == 0) {
            index = 1;
        }
        
        UISwitch* otherView = [self.switchesArray objectAtIndex:index];
        
        if(otherView.on == NO && switchView.on == NO)
        {
           
        }else{

            BOOL value = !switchView.on;
            [otherView setOn:value animated:YES];
            self.pointsArray[index] = [NSNumber numberWithBool: value];
        }
    }
     self.pointsArray[tag] = [NSNumber numberWithBool:switchView.on];
}

@end
