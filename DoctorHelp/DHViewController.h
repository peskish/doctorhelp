//
//  DHViewController.h
//  DoctorHelp
//
//  Created by Aliona on 01/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "FHSTwitterEngine.h"
#import "Odnoklassniki.h"

@interface DHViewController : UIViewController 
@property (strong, nonatomic) IBOutlet UIButton* faceBookButton;
@property (strong, nonatomic) IBOutlet UIButton* twiterButton;
@property (strong, nonatomic) IBOutlet UIButton* vkkButton;
@property (strong, nonatomic) IBOutlet UIButton* okButton;

@property CGFloat keyboardHeight;

@property (strong, nonatomic) Odnoklassniki* okAPI;
@property NSString* calcID;

-(IBAction)tapFaceBook:(id)sender;
-(IBAction)tapTwitter:(id)sender;
-(IBAction)tapVk:(id)sender;
-(IBAction)tapOk:(id)sender;
-(void)addToFavorite:(UIButton*)sender;

@end
