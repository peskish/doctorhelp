//
//  DHFerrumCalcViewController.m
//  DoctorHelp
//
//  Created by Aliona on 02/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHFerrumCalcViewController.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface DHFerrumCalcViewController () 

@property (weak, nonatomic) IBOutlet UITextField *williamsTextField;
@property (weak, nonatomic) IBOutlet UITextField *williamsDextranTextField;
@property (weak, nonatomic) IBOutlet UITextField *bachTextField;
@property (weak, nonatomic) IBOutlet UITextField *bachDextranTextField;

@end

@implementation DHFerrumCalcViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    _units.selectedSegmentIndex = 0;
    _resultA.hidden = YES;
    _resultAFe.hidden = YES;
    _resultV.hidden = YES;
    _resultVFe.hidden = YES;
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"Ferrum calculator"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Ferrum calculator"];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

#pragma mark - TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{

}

-(void)calculateFerrum:(double)hbLevel weight:(double)weight dl:(BOOL)dl
{
    if(!dl) {
        hbLevel /= 10;
    }
    double resultAuerbach = 0.3 * weight * (100 - hbLevel * 100 / 14.8);
    double resultAuerbachFe = resultAuerbach / 50;
    double resultWilliam = (16 - hbLevel) / 100 * weight * 65  * 0.0034;
    double resultWilliamFe = resultWilliam / 50;
    
    _resultA.hidden = NO;
    _resultAFe.hidden = NO;
    _resultV.hidden = NO;
    _resultVFe.hidden = NO;
    
    self.williamsTextField.text = [NSString stringWithFormat:@"%.2f г", resultWilliam];
    self.williamsDextranTextField.text = [NSString stringWithFormat:@"%.2f мл", resultWilliamFe];
    self.bachTextField.text = [NSString stringWithFormat:@"%.2f г", resultAuerbach];
    self.bachDextranTextField.text = [NSString stringWithFormat:@"%.2f мл", resultAuerbachFe];
    
//    _resultA.text = [NSString stringWithFormat:@"Дефицит железа по методу Ауэрбаха %.2f г",resultAuerbach];
//    _resultAFe.text = [NSString stringWithFormat:@"Декстрана железа необходимо %.2f мл",resultAuerbachFe];
//    _resultV.text = [NSString stringWithFormat:@"Дефицит железа по методу Вильяма %.2f г",resultWilliam];
//    _resultVFe.text = [NSString stringWithFormat:@"Декстрана железа необходимо %.2f мл",resultWilliamFe];
    
}

- (IBAction)calc:(UIButton *)sender {
    double weight = [[_weight.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    double hbLevel = [[_hbLevel.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    BOOL dl = (BOOL)_units.selectedSegmentIndex;
    if (![self showAlert]) {
        [self calculateFerrum:hbLevel weight:weight dl:dl];
    }
}

- (IBAction)clear:(UIButton *)sender {
    _units.selectedSegmentIndex = 0;
    _resultA.hidden = YES;
    _resultAFe.hidden = YES;
    _resultV.hidden = YES;
    _resultVFe.hidden = YES;
    _weight.text = @"";
    _hbLevel.text = @"";
    self.williamsDextranTextField.text = @"";
    self.williamsTextField.text = @"";
    self.bachTextField.text = @"";
    self.bachDextranTextField.text = @"";
}

- (BOOL)showAlert
{
    DHAlert *alert = [DHAlert new];
    NSArray *array = [[NSArray alloc] initWithObjects:_hbLevel.text, _weight.text, nil];
    [alert initWithViewController:self andArray:array];
    return alert.alertShown;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#define kOFFSET_FOR_KEYBOARD 40.0

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSLog(@"keyboard frame raw %@", NSStringFromCGRect(keyboardFrame));
    self.keyboardHeight = keyboardFrame.size.height;
    CGRect rect = fullScreenView;
    rect.size.height -= (self.keyboardHeight);
    
    self.mainScroll.frame = rect;
    
}

-(void)keyboardWillHide
{

    
    self.mainScroll.frame = fullScreenView;
    
}


//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

@end
