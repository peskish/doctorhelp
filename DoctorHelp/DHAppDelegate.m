//
//  DHAppDelegate.m
//  DoctorHelp
//
//  Created by Aliona on 01/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHAppDelegate.h"
#import "Flurry.h"
#import "GAI.h"
#import "UIColor+DHfromHex.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Odnoklassniki.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

#define IS_OS_7  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

@implementation DHAppDelegate

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    [OKSession.activeSession handleOpenURL:url];
    return YES;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    Google Analytics
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    [GAI sharedInstance].dispatchInterval = 20;
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-12310611-7"];
    
//     Flurry
    [Flurry setCrashReportingEnabled:YES];
    [Flurry setEventLoggingEnabled:YES];
    [Flurry startSession:@"YQ8HDB87VT64FPPBXH3P"];
    
//  Crashlytics
    [Fabric with:@[CrashlyticsKit]];

    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSLog(@"%@",version);
    
    if (![[NSUserDefaults standardUserDefaults]objectForKey:@"oldVersion"])
    {
        [[NSUserDefaults standardUserDefaults]setObject:version forKey:@"oldVersion"];
    } else {
        if (![[[NSUserDefaults standardUserDefaults]objectForKey:@"oldVersion"] isEqualToString:version])
        {
            if ([[NSUserDefaults standardUserDefaults]objectForKey:@"haveSeenNewCalcs"]) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"haveSeenNewCalcs"];
            }
        }
    }

    
    
    if (![[NSUserDefaults standardUserDefaults]objectForKey:@"newCalcs"])
    {
#warning КАЖДЫЙ РАЗ ДОБАВЛЯТЬ НОВЫЕ КАЛЬКУЛЯТОРЫ
        NSArray *newCalcs = @[@"otpuskVC"
                              ];
        [[NSUserDefaults standardUserDefaults] setObject:newCalcs forKey:@"newCalcs"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else {
        if (![[NSUserDefaults standardUserDefaults]objectForKey:@"haveSeenNewCalcs4"])
        {
            NSArray *newCalcs = @[@"otpuskVC"
                                  ];
            [[NSUserDefaults standardUserDefaults] setObject:newCalcs forKey:@"newCalcs"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"haveSeenNewCalcs4"];
            [[NSUserDefaults standardUserDefaults] synchronize];

        }
    }
 
    if (![[NSUserDefaults standardUserDefaults] integerForKey:@"V2_FIRST_LAUNCH"]) {
        NSDate *date = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"V2_FIRST_DATE_LAUNCH"];
        [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"V2_FIRST_RATE_LAUNCH"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"V2_FIRST_LAUNCH"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
    
    if (![[NSUserDefaults standardUserDefaults] integerForKey:@"V2.3_FIRST_LAUNCH"]) {

        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"V2.3_FIRST_LAUNCH"];
        NSArray *newCalcs = @[@"otpuskVC"
                              ];
        [[NSUserDefaults standardUserDefaults] setObject:newCalcs forKey:@"newCalcs"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
    
    
    NSInteger l = [[NSUserDefaults standardUserDefaults] integerForKey:@"V2_NLAUNCHES"];
    [[NSUserDefaults standardUserDefaults] setInteger:l+1 forKey:@"V2_NLAUNCHES"];
    [[NSUserDefaults standardUserDefaults] synchronize];

   
    // Navigation bars appearance
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    if (IS_OS_7)
    {
        [[UINavigationBar appearance] setBarTintColor:[UIColor colorwithHexString:@"4CD964" alpha:1.0f]];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    } else
    {
        [[UINavigationBar appearance] setTintColor:[UIColor colorwithHexString:@"4CD964" alpha:1.0f]];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    }
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, nil]];

    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (NSManagedObjectContext *) managedObjectContext {
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DoctorHelp" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]
                                               stringByAppendingPathComponent: @"DoctorHelp.sqlite"]];
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                  initWithManagedObjectModel:[self managedObjectModel]];
    if(![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                 configuration:nil URL:storeUrl options:nil error:&error]) {
        /*Error for store creation should be handled in here*/
    }
    
    return persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

/* (...Existing Application Code...) */




@end
