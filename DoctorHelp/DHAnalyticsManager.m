//
//  DHAnalyticsManager.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 11.12.14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHAnalyticsManager.h"
#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "Flurry.h"


@implementation DHAnalyticsManager

+ (DHAnalyticsManager *)sharedManager
{
    static DHAnalyticsManager *manager;
    if (manager == nil) {
        manager = [[DHAnalyticsManager alloc] init];
        
    }
    return manager;
}

-(void)logEventPageViewWithName:(NSString *)pageName
{
    // Google Analytics
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:pageName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    
    // Flurry Analytics
    [Flurry logEvent:pageName];
}
@end
