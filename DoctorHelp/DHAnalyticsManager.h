//
//  DHAnalyticsManager.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 11.12.14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DHAnalyticsManager : NSObject

+ (DHAnalyticsManager *)sharedManager;

-(void)logEventPageViewWithName:(NSString *)pageName;

@end
