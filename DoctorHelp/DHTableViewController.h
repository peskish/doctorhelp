//
//  DHTableViewController.h
//  DoctorHelp
//
//  Created by Aliona on 01/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import  "DHAboutViewController.h"
@interface DHTableViewController : UITableViewController <UIAlertViewDelegate,UISearchDisplayDelegate>

@property (strong,nonatomic) IBOutlet UIButton* rihtInfoB;

@property (weak, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UILabel *installLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;
@property (weak, nonatomic) IBOutlet UIImageView *doctorFeedIcon;
@property UIAlertView *rateAlertView;
@property (nonatomic, strong) NSMutableArray *searchResult;
-(IBAction)pushAbout:(id)sender;

@end
