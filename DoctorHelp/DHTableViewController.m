//
//  DHTableViewController.m
//  DoctorHelp
//
//  Created by Aliona on 01/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHTableViewController.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"
#import "Flurry.h"
#import "DHViewController.h"

#define IS_OS_7  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)


@interface DHTableViewController ()

@end

@implementation DHTableViewController {
    NSArray *calculators;
    NSArray *controllerIds;
    NSMutableArray *favoriteCalculators;
    NSMutableArray* calcs;
    NSArray* sortedCalcs;
    BOOL extendAll;
    BOOL extendFavorites;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"extendAll"])
    {
        extendAll = [[[NSUserDefaults standardUserDefaults]objectForKey:@"extendAll"]boolValue];
    } else {
        extendAll = NO;
    }
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"extendFavorites"])
    {
        extendFavorites = [[[NSUserDefaults standardUserDefaults]objectForKey:@"extendFavorites"]boolValue];
    } else {
        extendFavorites = NO;
    }
    calcs = [[NSMutableArray alloc]initWithArray: @[@{@"Скорость клубочковой фильтрации": @"SkfCalcID"},
              @{@"Расчет суммарной дозы препаратов железа при железодефицитной анемии" : @"FerrumCalcID" },
              @{@"Скорость клубочковой фильтрации по концентрации креатинина в суточной моче" :  @"CreatininumCalcID"},
              @{@"Расчет должного количества калия в организме":@"KaliumCalcID"},
              @{@"Вычисление площади поверхности тела":@"BodyAreaCalcID"},
              @{@"Определение уровня основного обмена":@"MetabolismCalcID"},
              @{@"Общий объем циркулирующей крови":@"OckCalcID"},
              @{@"Оценка риска ИБС при высоком холестерине":@"IbsRiskCalcID"},
              @{@"Корректировка интервала QT по ЧСС":@"QtCorrectCalcID"},
              @{@"Прогноз вероятности зачатия при бесплодии":@"FertilityCalcID"},
              @{@"Расчет вероятности стеноза почечных артерий":@"StenosisCalcID"},
              @{@"Расчет показателей антропометрии": @"IdealMassID"},
              @{@"Оценка активности ревматоидного артрита":@"ArthritisID"},
              @{@"Критерии Велла для оценки вероятности ТЭЛА":@"VellaID"},
              @{@"Критерии классификации системной красной волчанки":@"VolchID"},
              @{@"Оценка активности системной волчанки":@"OcenVolchID"},
              @{@"Оценка риска инсульта при фибрилляции предсердий":@"InsultID"},
              @{@"Шкала комы Глазго":@"glazgo"},
              @{@"Шкала оценки рисков острого коронарного синдрома GRACE":@"grace"},
              @{@"Длительность временной нетрудоспособности" : @"tempDisability"},
              @{@"Расчет предполагаемой даты родов" : @"childBirth"},
              @{@"Определение объема щитовидной железы по УЗИ" : @"TireoCalc"},
              @{@"Оценка рисков кардиохирургического вмешательства EuroSCORE" : @"euroScore"},
              @{@"Шкала оценки степени инвалидизации после инсульта (Шкала Рэнкина)" : @"rankin"},
              @{@"Индекс охвата и тяжести псориаза PASI" : @"pasi"},
              @{@"Индекс активности и тяжести анкилозирующего спондилита (болезни Бехтерева) BASDAI" : @"basdai"},
              @{@"Шкала тяжести инсульта Национальных институтов здоровья CША NIHSS" : @"nihss"},
                                                    @{@"Шкала оценки тяжести состояния и прогноза APACHE 2" : @"apacheCalc"},
                                                    @{@"Расчет отпускных" : @"otpuskVC"}
              ]];
    
    
    calculators = [[NSArray alloc] initWithObjects:
                    @"Скорость клубочковой фильтрации",
                    @"Расчет суммарной дозы препаратов железа при железодефицитной анемии",
                    @"Скорость клубочковой фильтрации по концентрации креатинина в суточной моче",
                    @"Расчет должного количества калия в организме",
                    @"Вычисление площади поверхности тела",
                    @"Определение уровня основного обмена",
                    @"Общий объем циркулирующей крови",
                    @"Оценка риска ИБС при высоком холестерине",
                    @"Корректировка интервала QT по ЧСС",
                    @"Прогноз вероятности зачатия при бесплодии",
                    @"Расчет вероятности стеноза почечных артерий",
                    @"Расчет показателей антропометрии",
                    @"Оценка активности ревматоидного артрита",
                    @"Критерии Велла для оценки вероятности ТЭЛА",
                    @"Критерии классификации системной красной волчанки",
                    @"Оценка активности системной волчанки",
                    @"Оценка риска инсульта при фибрилляции предсердий",
                    @"Шкала комы Глазго",
                    @"Шкала оценки рисков острого коронарного синдрома GRACE",
                    @"Длительность временной нетрудоспособности",
                    @"Расчет предполагаемой даты родов",
                    @"Определение объема щитовидной железы по УЗИ",
                    @"Оценка рисков кардиохирургического вмешательства EuroSCORE",
                    @"Шкала оценки степени инвалидизации после инсульта (Шкала Рэнкина)",
                    @"Индекс охвата и тяжести псориаза PASI",
                    @"Индекс активности и тяжести анкилозирующего спондилита (болезни Бехтерева) BASDAI",
                   @"Шкала тяжести инсульта Национальных институтов здоровья CША NIHSS",
                   @"Шкала оценки тяжести состояния и прогноза APACHE 2",
                   @"Расчет отпускных",
                    nil];
    
    controllerIds = [[NSArray alloc] initWithObjects:
                     @"SkfCalcID",
                     @"FerrumCalcID",
                     @"CreatininumCalcID",
                     @"KaliumCalcID",
                     @"BodyAreaCalcID",
                     @"MetabolismCalcID",
                     @"OckCalcID",
                     @"IbsRiskCalcID",
                     @"QtCorrectCalcID",
                     @"FertilityCalcID",
                     @"StenosisCalcID",
                     @"IdealMassID",
                     @"ArthritisID",
                     @"VellaID",
                     @"VolchID",
                     @"OcenVolchID",
                     @"InsultID",
                     @"glazgo",
                     @"grace",
                     @"tempDisability",
                     @"childBirth",
                     @"TireoCalc",
                     @"euroScore",
                     @"rankin",
                     @"pasi",
                     @"basdai",
                     @"nihss",
                     @"apacheCalc",
                     @"otpuskVC",
                     nil];
    
    favoriteCalculators= [NSMutableArray new];
    
    // No title for the Back button
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    
    sortedCalcs = [calcs sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *p1, NSDictionary *p2){
        
        return [p1.allKeys[0] compare:p2.allKeys[0]];
        
    }];


    self.tableView.tableFooterView = self.footerView;
    
    [self.installLabel setFont:[UIFont fontWithName:@"Tahoma-Bold" size:16]];
    [self.bodyLabel setFont:[UIFont fontWithName:@"Tahoma" size:12]];
    self.doctorFeedIcon.layer.cornerRadius = 10.0f;
    self.doctorFeedIcon.clipsToBounds = YES;
    [Flurry logAllPageViewsForTarget:self.navigationController];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Calculator select screen"];
    
    self.searchResult = [NSMutableArray new];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"V2_RATE_ALERT_SHOWN"]) {
        BOOL show = NO;
        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"V2_NLAUNCHES"] == 10) {
            show = YES;
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"V2_NLAUNCHES"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        NSDate *flDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"V2_FIRST_RATE_LAUNCH"];
        NSDate *date = [NSDate date];
        NSTimeInterval interval = [date timeIntervalSinceDate:flDate];
        if (interval/60/60/24 > 5) {
            show = YES;
            NSDate *date = [NSDate date];
            [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"V2_FIRST_RATE_LAUNCH"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }
        if (show) {
            _rateAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Понравилось?", @"Понравилось?")
                                                        message:NSLocalizedString(@"Если Вам понравилось приложение, пожалуйста, оцените его", @"Если Вам понравилось приложение, пожалуйста, оцените его")
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Пропустить", @"Пропустить")
                                              otherButtonTitles:NSLocalizedString(@"Оценить", @"Оценить"), nil];
            [_rateAlertView show];
            
        }
    }
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if (IS_OS_7)
    {
        [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:76.0/255.0 green:217.0/255.0 blue:100.0/255.0 alpha:1.0]];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    } else
    {
        [self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:76.0/255.0 green:217.0/255.0 blue:100.0/255.0 alpha:1.0]];
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    }
    
    [favoriteCalculators removeAllObjects];
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"favoriteCalcs"])
    {
        NSMutableArray *favoriteCalcs = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"favoriteCalcs"]];
        for (NSString *calc in favoriteCalcs)
        {
            for (NSDictionary *calcFromAll in calcs)
            {
                for (NSString *p in calcFromAll)
                {
                    if ([calc isEqualToString:[calcFromAll objectForKey:p]])
                    {
                        [favoriteCalculators addObject:@{p : calc}];
                    }
                }
            }
        }
    }
//    [self.tableView setSeparatorColor:[UIColor clearColor]];
    [self.tableView reloadData];
    
  
}
-(IBAction)pushAbout:(id)sender
{
    UIViewController *selected = [self.storyboard instantiateViewControllerWithIdentifier:@"About"];
    [self.navigationController pushViewController:selected animated:YES];
}

#pragma mark - SEARCH DELEGATE
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [self.searchResult removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    
    self.searchResult = [NSMutableArray arrayWithArray: [calculators filteredArrayUsingPredicate:resultPredicate]];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    return YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return 1;
    }
    else
    {
        return 2;
    }
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *viewForHeader = [[UIView alloc]init];
    [viewForHeader setFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40)];
    [viewForHeader setBackgroundColor:[UIColor colorWithRed:0.94 green:0.94 blue:0.94 alpha:1]];
    
    UILabel *labelForHeader = [[UILabel alloc]init];
    [labelForHeader setFrame:CGRectMake(0, 0, self.view.bounds.size.width, 60)];
    [labelForHeader setFont: [UIFont boldSystemFontOfSize:17.0]];
    [labelForHeader setTextColor:[UIColor darkGrayColor]];
    [labelForHeader setBackgroundColor:[UIColor clearColor]];
    [viewForHeader addSubview:labelForHeader];
    
    
    switch (section) {
        case 0:
            [labelForHeader setText:@"Избранные калькуляторы"];
            break;
        case 1:
            [labelForHeader setText:@"Все калькуляторы"];
        default:
            break;
    }
    
    [labelForHeader sizeToFit];
    [labelForHeader setCenter:CGPointMake(viewForHeader.frame.size.width/2, viewForHeader.frame.size.height/2)];
    
    UIImageView *arrowImage = [[UIImageView alloc]init];
    switch (section) {
        case 0:
            if (extendFavorites) {
                [arrowImage setImage:[UIImage imageNamed:@"arrow_spinner"]];

            } else {
                [arrowImage setImage:[UIImage imageNamed:@"arrow_collapse"]];
            }
            break;
        case 1:
            if (extendAll) {
                [arrowImage setImage:[UIImage imageNamed:@"arrow_spinner"]];

            } else {
                [arrowImage setImage:[UIImage imageNamed:@"arrow_collapse"]];

            }
        default:
            break;
    }
    [arrowImage setFrame:CGRectMake(10, 0, 16, 16)];
    [arrowImage setCenter:CGPointMake(arrowImage.center.x, labelForHeader.center.y)];
    arrowImage.tag = 5;
    [viewForHeader addSubview:arrowImage];
    
    CGRect frame = labelForHeader.frame;
    frame.origin.x = CGRectGetMaxX(arrowImage.frame) + 5;
    labelForHeader.frame = frame;
    
    UIButton *extendButton = [[UIButton alloc]initWithFrame:viewForHeader.frame];
    [extendButton setTitle:@"" forState:UIControlStateNormal];
    [extendButton setBackgroundColor:[UIColor clearColor]];
    [extendButton addTarget:self action:@selector(extendTable:) forControlEvents:UIControlEventTouchUpInside];
    extendButton.tag = section;
    [viewForHeader addSubview:extendButton];
    
    
    return viewForHeader;
}

-(void)extendTable:(UIButton *)sender
{
    switch (sender.tag) {
        case 0:
            
            extendFavorites = !extendFavorites;
            [[NSUserDefaults standardUserDefaults]setObject:@(extendFavorites) forKey:@"extendFavorites"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            break;
        case 1:
            extendAll = !extendAll;
            [[NSUserDefaults standardUserDefaults]setObject:@(extendAll) forKey:@"extendAll"];
            [[NSUserDefaults standardUserDefaults]synchronize];
        default:
            break;
    }
    
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationFade];

}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView != self.searchDisplayController.searchResultsTableView)
    {
        if (favoriteCalculators.count > 0)
        {
            return 40;
        }
    }
    
    return 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [self.searchResult count];
    }
    else
    {
        switch (section) {
            case 0:
                if (extendFavorites) { return 0;} else {return favoriteCalculators.count;}
                break;
                
            case 1:
                if (extendAll) {return 0;} else {return calculators.count;}
            default:
                break;
        }
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellText = @"";
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        cellText = self.searchResult[indexPath.row];
    }
    else
    {
        cellText = [self getCalcName:indexPath];
    }
    CGSize constraintSize = CGSizeMake(self.view.bounds.size.width-40, MAXFLOAT);
    CGSize labelSize = [cellText sizeWithFont:[UIFont boldSystemFontOfSize:17.0] constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    int cellHeight = labelSize.height + 10;
    if(cellHeight < 44)
    {
        cellHeight = 44;
    }
    
    return cellHeight;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"calcCell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"calcCell"];
    }
    cell.textLabel.font = [UIFont boldSystemFontOfSize:17.0];
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.numberOfLines = 0;
    BOOL needToAddSeparator = NO;

    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        cell.textLabel.text = [self.searchResult objectAtIndex:indexPath.row];
        needToAddSeparator = NO;
    }
    else
    {
        cell.textLabel.text = [self getCalcName:indexPath];
        
    }
    cell.textLabel.highlightedTextColor = [UIColor whiteColor];
    
    UIImageView *newLabel = [[UIImageView alloc]init];
    
    for (UIImageView *label in cell.subviews)
    {
        if (label.tag == 90)
        {
            newLabel = label;
        }
    }
    
    newLabel.frame = CGRectMake(self.view.bounds.size.width-44, 0, 44, 44);
    [newLabel setImage:[UIImage imageNamed:@"label_new_red_right.png"]];
    newLabel.hidden = YES;
    newLabel.tag = 90;
    [cell addSubview:newLabel];
    
    NSDictionary *calcDict = sortedCalcs[indexPath.row];
    NSString *calcID = [calcDict objectForKey:calcDict.allKeys[0]];
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"newCalcs"])
    {
        NSArray *newCalcs = [[NSUserDefaults standardUserDefaults]objectForKey:@"newCalcs"];
        for (NSString *calc in newCalcs)
        {
            if ([calc isEqualToString:calcID])
            {
                newLabel.hidden = NO;
                break;
            } else
            {
                newLabel.hidden = YES;
            }
        }
    }
    
                        
    for (UIView *sep in cell.subviews)
    {
        if (sep.tag == 10)
        {
            needToAddSeparator = NO;
            sep.frame = CGRectMake(20, cell.bounds.size.height-1, self.view.bounds.size.width - 20, 1.0 / [UIScreen mainScreen].scale);
        }
    }
    
    if (needToAddSeparator == YES) {
        UIView *separator = [[UIView alloc]initWithFrame:CGRectMake(20, cell.bounds.size.height-1, self.view.bounds.size.width - 20, 1.0 / [UIScreen mainScreen].scale)];
        separator.backgroundColor = [UIColor lightGrayColor];
        separator.alpha = 0.4f;
        separator.tag = 10;
        [cell addSubview:separator];
    }
    
    UIView *selectedView = [[UIView alloc] initWithFrame:cell.frame];
    selectedView.backgroundColor = [UIColor colorWithRed:76.0/255.0 green:217.0/255.0 blue:100.0/255.0 alpha:1.0];
    cell.selectedBackgroundView = selectedView;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *controllerId = @"";
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        NSString *calcName = self.searchResult[indexPath.row];
        int i = [calculators indexOfObject:calcName];
        controllerId = controllerIds[i];
    } else
    {
        if (indexPath.section == 0)
        {
            controllerId = favoriteCalculators[indexPath.row][[self getCalcName:indexPath]];

        } else {
            controllerId = sortedCalcs[indexPath.row][[self getCalcName:indexPath]];

        }
    }
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"newCalcs"])
    {
        NSMutableArray *newCalcs = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"newCalcs"]];
        
        for (NSString *calc in newCalcs)
        {
            if ([calc isEqualToString:controllerId])
            {
                [newCalcs removeObject:calc];
                [[NSUserDefaults standardUserDefaults]setObject:newCalcs forKey:@"newCalcs"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                break;
            }
            
        }
    }
    
    DHViewController *selected = [self.storyboard instantiateViewControllerWithIdentifier:controllerId];
    selected.calcID = controllerId;
    [self.navigationController pushViewController:selected animated:YES];
    
    
}



-(NSString*)getCalcName:(NSIndexPath*)indexPath
{
    if (indexPath.section == 0) {
        NSDictionary* calc = favoriteCalculators[indexPath.row];
        return calc.allKeys[0];
    } else {
        NSDictionary* calc = sortedCalcs[indexPath.row];
        return calc.allKeys[0];
    }
    return  @"";
}

- (IBAction)installDoctorFeed:(UIButton *)sender
{
    NSString *iTunesLink = @"https://itunes.apple.com/us/app/lenta-vraca/id581482034?l=ru&ls=1&mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView == _rateAlertView) {
        if (buttonIndex == 1) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"V2_RATE_ALERT_SHOWN"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self goReview];
        } else {
            
        }
    }
}

- (void)goReview
{
    NSString *str = @"https://itunes.apple.com/us/app/pomos-vracu/id949885646?l=ru&ls=1&mt=8";
    
    NSURL *url = [NSURL URLWithString:str];
    if (url) {
        [[UIApplication sharedApplication] openURL:url];
    }
}

@end
