//
//  DHBodyAreaCalcViewController.m
//  DoctorHelp
//
//  Created by Aliona on 01/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHBodyAreaCalcViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface DHBodyAreaCalcViewController ()

@property (weak, nonatomic) IBOutlet UITextField *resultTextField;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@end

@implementation DHBodyAreaCalcViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    fullScreenView = _mainScroll.frame;
    keyBoardView = CGRectMake(fullScreenView.origin.x, fullScreenView.origin.y, fullScreenView.size.width, fullScreenView.size.height-KEYBOARD_HEIGHT);
    
    _result.hidden = YES;
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"Body area calculator"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"Body area calculator"];

    self.resultLabel.text = @"м\u00B2";
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{

}

-(void)calculateBodyAreaForHeight:(double)height andWeight:(double)weight andAge:(int)age
{
    double result;
    if (age >= 18) {
        result = 0.007184 * pow(weight, 0.425) * pow(height, 0.725);
    } else {
        result = pow(height * weight / 3600, 0.5);
    }
    NSString *resultString = [NSString stringWithFormat:@"%.02f", result];
    self.resultTextField.text = resultString;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
}

- (IBAction)calc:(UIButton *)sender {
    double height = [[_height.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    double weight = [[_weight.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    int age = [_age.text integerValue];
    _result.hidden = NO;
    if(![self showAlert]) {
        [self calculateBodyAreaForHeight:height andWeight:weight andAge:age];
    }
}

- (IBAction)clear:(UIButton *)sender {
    _height.text = @"";
    _weight.text = @"";
    self.resultTextField.text = @"";
    _age.text = @"";
    _result.text = @"";
    _result.hidden = YES;
}

- (IBAction)facebook:(UIButton *)sender {
//    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
//        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//        
//        [controller setInitialText:@"First post from my iPhone app"];
//        [self presentViewController:controller animated:YES completion:Nil];
//    }
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
        
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        SLComposeViewControllerCompletionHandler myBlock = ^(SLComposeViewControllerResult result){
            if (result == SLComposeViewControllerResultCancelled) {
                
                NSLog(@"Cancelled");
                
            } else
                
            {
                NSLog(@"Done");
            }
            
            [controller dismissViewControllerAnimated:YES completion:Nil];
        };
        controller.completionHandler =myBlock;
        
        //Adding the Text to the facebook post value from iOS
        [controller setInitialText:@"Test Post from mobile.safilsunny.com"];
        
        //Adding the URL to the facebook post value from iOS
        
        [controller addURL:[NSURL URLWithString:@"http://www.mobile.safilsunny.com"]];
        
        //Adding the Image to the facebook post value from iOS
        
        [controller addImage:[UIImage imageNamed:@"fb.png"]];
        
        [self presentViewController:controller animated:YES completion:Nil];
        
    }
    else{
        NSLog(@"UnAvailable");
    }
    
}

- (IBAction)tweet:(UIButton *)sender
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:@"Great fun to learn iOS programming at appcoda.com!"];
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
}

- (IBAction)vk:(UIButton *)sender
{}

- (IBAction)ok:(UIButton *)sender
{}

- (BOOL)showAlert
{
    DHAlert *alert = [DHAlert new];
    NSArray *array = [[NSArray alloc] initWithObjects:_height.text, _weight.text, _age.text, nil];
    [alert initWithViewController:self andArray:array];
    return alert.alertShown;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#define kOFFSET_FOR_KEYBOARD 40.0

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSLog(@"keyboard frame raw %@", NSStringFromCGRect(keyboardFrame));
    self.keyboardHeight = keyboardFrame.size.height;
    CGRect rect = fullScreenView;
    rect.size.height -= (self.keyboardHeight);
    
    self.mainScroll.frame = rect;
    
}

-(void)keyboardWillHide
{
    CGRect rect = self.mainScroll.frame;
    
    rect.size.height += (self.keyboardHeight);
    
    self.mainScroll.frame = fullScreenView;
    
}


//method to move the view up/down whenever the keyboard is shown/dismissed


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

@end
