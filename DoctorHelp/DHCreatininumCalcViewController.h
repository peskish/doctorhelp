//
//  DHCreatininumCalcViewController.h
//  DoctorHelp
//
//  Created by Aliona on 02/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"
@interface DHCreatininumCalcViewController : DHViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *bloodCreatininum;
@property (weak, nonatomic) IBOutlet UITextField *urinaCreatininum;
@property (weak, nonatomic) IBOutlet UITextField *mok;
@property (weak, nonatomic) IBOutlet UISegmentedControl *unitsBlood;
@property (weak, nonatomic) IBOutlet UISegmentedControl *unitsUrina;
@property (weak, nonatomic) IBOutlet UILabel *resultSkf;
@property (weak, nonatomic) IBOutlet UILabel *resultXbp;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIButton *calcButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;

- (IBAction)calc:(UIButton *)sender;
- (IBAction)clear:(UIButton *)sender;

@end
