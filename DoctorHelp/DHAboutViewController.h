//
//  DHAboutViewController.h
//  DoctorHelp
//
//  Created by Aliona on 01/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h> 
@interface DHAboutViewController : UIViewController <UIWebViewDelegate,MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webview;

@end
