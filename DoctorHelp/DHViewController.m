//
//  DHViewController.m
//  DoctorHelp
//
//  Created by Aliona on 01/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHViewController.h"

@interface DHViewController () <FHSTwitterEngineAccessTokenDelegate,OKSessionDelegate>
{

}
@property  UIBarButtonItem *favButton;
@property UIButton *button;
@end
#define IS_OS_7  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

@implementation DHViewController

@synthesize okAPI;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    [[FHSTwitterEngine sharedEngine]permanentlySetConsumerKey:@"NSe8Kk8DNuysVxiu89xZB1WnU" andSecret:@"fFN0XnsunweqqoTIEX0MlwiZyyTbGO8ABaGR0fbgksrZj5D9AQ"];
    [[FHSTwitterEngine sharedEngine]setDelegate:self];
    [[FHSTwitterEngine sharedEngine]loadAccessToken];
    
    
   okAPI = [[Odnoklassniki alloc] initWithAppId:@"1109656320" appSecret:@"CBAEAFCDEBABABABA" appKey:@"2B9724EEA9498C87DE76299D" delegate:self];
    
    
    
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.button setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
    self.button.frame=CGRectMake(0.0, 0.0, 30.0, 30.0);
    [self.button addTarget:self action:@selector(addToFavorite:) forControlEvents:UIControlEventTouchUpInside];
    
    self.favButton = [[UIBarButtonItem alloc]initWithCustomView:self.button];

    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"favoriteCalcs"])
    {
        NSMutableArray *favoriteCalcs = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"favoriteCalcs"]];
        for (NSString *calc in favoriteCalcs)
        {
            if ([calc isEqualToString:self.calcID])
            {
                [self.button setImage:[UIImage imageNamed:@"star_pressed"] forState:UIControlStateNormal];

                break;
            }
        }
    }
    self.navigationItem.rightBarButtonItem = self.favButton;
    
    if (!IS_OS_7)
    {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style: UIBarButtonItemStyleBordered target:self action:@selector(back)];
        self.navigationItem.leftBarButtonItem = backButton;
    }

}

-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)addToFavorite:(UIButton*)sender
{
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"favoriteCalcs"])
    {
        NSMutableArray *favoriteCalcs = [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults]objectForKey:@"favoriteCalcs"]];
        bool needToAdd = YES;
        
        for (NSString *calc in favoriteCalcs)
        {
            if ([calc isEqualToString:self.calcID])
            {
                needToAdd = NO;
                [favoriteCalcs removeObject:calc];
                [self.button setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];

                break;
            }
        }
        if (needToAdd != NO)
        {
            [favoriteCalcs addObject:self.calcID];
            [self.button setImage:[UIImage imageNamed:@"star_pressed"] forState:UIControlStateNormal];


        }
        
        [[NSUserDefaults standardUserDefaults] setObject:favoriteCalcs forKey:@"favoriteCalcs"];
        
    } else {
        NSArray *favoriteCalcs = @[self.calcID];
        [[NSUserDefaults standardUserDefaults] setObject:favoriteCalcs forKey:@"favoriteCalcs"];
    }
    
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)tapFaceBook:(id)sender
{
    NSString *url = @ "https://m.facebook.com/sharer.php?u=drhelp.doktornarabote.ru";
   
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
//
//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                   @"Sharing Doctorhelp", @"name",
//                                   @"Build great social apps and get more installs.", @"caption",
//                                   @"DoctorHelp description.", @"description",
//                                   @"http://google.com", @"link",
//                                   @"http://i.imgur.com/g3Qc1HN.png", @"picture",
//                                   nil];
//    
//    // Show the feed dialog
//    [FBWebDialogs presentFeedDialogModallyWithSession:nil
//                                           parameters:params
//                                              handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
//                                                  if (error) {
//                                                      // An error occurred, we need to handle the error
//                                                      // See: https://developers.facebook.com/docs/ios/errors
//                                                      NSLog(@"Error publishing story: %@", error.description);
//                                                  } else {
//                                                      if (result == FBWebDialogResultDialogNotCompleted) {
//                                                          // User cancelled.
//                                                          NSLog(@"User cancelled.");
//                                                      } else {
//                                                          // Handle the publish feed callback
//                                                          NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
//                                                          
//                                                          if (![urlParams valueForKey:@"post_id"]) {
//                                                              // User cancelled.
//                                                              NSLog(@"User cancelled.");
//                                                              
//                                                          } else {
//                                                              // User clicked the Share button
//                                                              NSString *result = [NSString stringWithFormat: @"Posted story, id: %@", [urlParams valueForKey:@"post_id"]];
//                                                              NSLog(@"result %@", result);
//                                                          }
//                                                      }
//                                                  }
//                                              }];
}

-(IBAction)tapTwitter:(id)sender
{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/intent/tweet?text=%D0%9F%D0%BE%D0%BC%D0%BE%D1%89%D1%8C%20%D0%B2%D1%80%D0%B0%D1%87%D1%83&url=http%3A%2F%2Fdrhelp.doktornarabote.ru%2F&original_referer="]];
//    BOOL isAuthorized = [[FHSTwitterEngine sharedEngine]isAuthorized];
//    if(isAuthorized == NO){
//        UIViewController *loginController = [[FHSTwitterEngine sharedEngine]loginControllerWithCompletionHandler:^(BOOL success) {
//           [[FHSTwitterEngine sharedEngine]postTweet:@"Test tweet"];
//            [self showAllert:@"Share complete"];
//        }];
//        [self presentViewController:loginController animated:YES completion:nil];
//    }else
//    {
//        [[FHSTwitterEngine sharedEngine]postTweet:@"twet test"];
//        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
//        
//        [self showAllert:@"Share complete"];
//       
//    }
}

-(IBAction)tapVk:(id)sender
{
   
    NSString* url = @"https://m.vk.com/share.php?url=http%3A%2F%2Fdrhelp.doktornarabote.ru%2F&title=%D0%9F%D0%BE%D0%BC%D0%BE%D1%89%D1%8C%20%D0%B2%D1%80%D0%B0%D1%87%D1%83&noparse=false";
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    
//    BOOL isLogged = [VKSdk isLoggedIn];
//    if(isLogged)
//    {
//        VKShareDialogController * shareDialog = [VKShareDialogController new]; //1
//        shareDialog.text = @"Your share text here"; //2
//               shareDialog.otherAttachmentsStrings = @[@"https://vk.com/dev/ios_sdk"]; //4
//        [shareDialog presentIn:self]; //5
//    }else
//    {
//        [VKSdk authorize:@[VK_PER_WALL] revokeAccess:NO forceOAuth:NO];
//    }

}

-(IBAction)tapOk:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=http://drhelp.doktornarabote.ru/&st.comments=%D0%9F%D0%BE%D0%BC%D0%BE%D1%89%D1%8C%20%D0%B2%D1%80%D0%B0%D1%87%D1%83"]];
    
//    if (!self.okAPI.isSessionValid) {
//        [self.okAPI authorizeWithPermissions:@[@"VALUABLE ACCESS"]];
//    } else {
//        [self.okAPI logout];
//    }
}

//fb

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}


//tweet

- (void)storeAccessToken:(NSString *)accessToken {
    [[NSUserDefaults standardUserDefaults]setObject:accessToken forKey:@"SavedAccessHTTPBody"];
}

- (NSString *)loadAccessToken {
    return [[NSUserDefaults standardUserDefaults]objectForKey:@"SavedAccessHTTPBody"];
}

#pragma mark - Odnoklassniki Delegate methods

- (void)okShouldPresentAuthorizeController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

- (void)okWillDismissAuthorizeControllerByCancel:(BOOL)canceled {
    NSLog(@"autorization canceled by user");
}


/*
 * Method will be called after success login ([_api authorize:])
 * Метод будет вызван после успешной авторизации ([_api authorize:])
 */
- (void)okDidLogin {
//    self.sessionStatusLabel.text = @"Logged in";
//    [self.authButton setTitle:@"Logout" forState:UIControlStateNormal];
//    
//    [self getUserInfo];
//    [self getFriends];
}

/*
 * Method will be called if login faild (cancelled == YES if user cancelled login, NO otherwise)
 * Метод будет вызван, если при авторизации произошла ошибка (cancelled == YES если пользователь прервал авторизацию, NO во всех остальных случаях)
 */
- (void)okDidNotLogin:(BOOL)canceled {
    
}

/*
 * Method will be called if login faild and server returned an error
 * Метод будет вызван, если сервер вернул ошибку авторизации
 */
- (void)okDidNotLoginWithError:(NSError *)error {
    
}

/*
 * Method will be called if [_api refreshToken] called and new access_token was got
 * Метод будет вызван в случае, если вызван [_api refreshToken] и получен новый access_token
 */
- (void)okDidExtendToken:(NSString *)accessToken {
    [self okDidLogin];
}

/*
 * Method will be called if [_api refreshToken] called and new access_token wasn't got
 * Метод будет вызван в случае, если вызван [_api refreshToken] и новый access_token не получен
 */
- (void)okDidNotExtendToken:(NSError *)error {
    
}

/*
 * Method will be called after logout ([_api logout])
 * Метод будет вызван после выхода пользователя ([_api logout])
 */
- (void)okDidLogout {
//    self.sessionStatusLabel.text = @"Not logged in";
//    [self.authButton setTitle:@"Login" forState:UIControlStateNormal];
//    [self clearUserInfo];
}

-(void)showAllert:(NSString*)text
{
//    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
//                              @"Result" message:text delegate:self
//                                             cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
//    [alertView show];
}



@end


