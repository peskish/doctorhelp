//
//  DHSkfCalcViewController.m
//  DoctorHelp
//
//  Created by Aliona on 02/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHSkfCalcViewController.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface DHSkfCalcViewController ()

@property (weak, nonatomic) IBOutlet UITextField *skfTextField;
@property (weak, nonatomic) IBOutlet UITextField *hpbStageTextField;
@property BOOL fullRect;
@end

@implementation DHSkfCalcViewController
{
    CGRect fullScreenView;
    CGRect keyBoardView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Калькулятор";
    
    fullScreenView = _mainScroll.frame;
    
    _xbp.hidden = YES;
    _result.hidden = YES;
    _gender.selectedSegmentIndex = 0;
    _units.selectedSegmentIndex = 0;
    _calcButton.layer.cornerRadius = 5;
    _clearButton.layer.cornerRadius = 5;
    
    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"SKF calculator"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"SKF calculator"];
    
    CGRect rect = self.contentView.frame;
    rect.size.height -= 180;
    self.contentView.frame = rect;
    self.fullRect = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.mainScroll layoutIfNeeded];
    self.mainScroll.contentSize = self.contentView.bounds.size;
    NSLog(@"%f",self.mainScroll.contentSize.height);
}


- (void)calcSkfForWeight:(double)weight height:(double)height age:(int)age creatininum:(double)creatininum mkm:(BOOL)mkm female:(BOOL)female
{
    double skf;
    
    if(mkm) {
        creatininum = creatininum / 1000 * 113.12 / 10;
    }
    
    if (age >= 18) {
        if (female) {
            skf = (140 - age) * weight / (72 * creatininum) * 0.85;
        } else {
            skf = (140 - age) * weight / (72 * creatininum);
        }
    } else {
        skf = (0.48 * height) / creatininum * 100;
    }
    int xbp = [self skfToXbp:skf];
    
    self.skfTextField.text = [NSString stringWithFormat:@"%.2f", skf];
    self.hpbStageTextField.text = [NSString stringWithFormat:@"%i", xbp];
    
//    _result.hidden = NO;
//    _result.text = [NSString stringWithFormat:@"СКФ %.2f",skf];
//    
//    _xbp.hidden = NO;
//    _xbp.text = [NSString stringWithFormat:@"Стадия ХБП %i",xbp];
}

- (int)skfToXbp:(double)skf {
    if (skf < 15) {
        return 5;
    } else if (skf >= 15 && skf < 30) {
        return 4;
    } else if (skf >= 30 && skf < 60) {
        return 3;
    } else if (skf >= 60 && skf < 90) {
        return 2;
    } else if (skf >= 90) {
        return 1;
    }
    return 0;
}

- (IBAction)unitSelect:(UISegmentedControl *)sender {
//    if(_units.selectedSegmentIndex) {
//        _creatininum.placeholder = @"Креатинин плазмы, мкмоль/л";
//    } else {
//        _creatininum.placeholder = @"Креатинин плазмы, мг/дл";
//    }
//    
}

- (IBAction)calc:(UIButton *)sender {
    double creatininum = [[_creatininum.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    double weight = [[_weight.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    double height = [[_height.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
    int age = [[_age.text stringByReplacingOccurrencesOfString:@"," withString:@"."] integerValue];
    BOOL female = (BOOL)_gender.selectedSegmentIndex;
    BOOL mkm = (BOOL)_units.selectedSegmentIndex;
    if (![self showAlert]) {
        [self calcSkfForWeight:weight height:height age:age creatininum:creatininum mkm:mkm female:female];
        [self calcSkfMdrdForAge:age creatininum:creatininum female:female mkm:mkm];
    }
}

- (void)calcSkfMdrdForAge:(int)age creatininum:(double)creatininum female:(BOOL)female mkm:(BOOL)mkm
{
    if(mkm) {
        creatininum = creatininum / 1000 * 113.12 / 10;
    }
    
    double result = 186*pow(creatininum, -1.154) * pow(age, -0.203);
    if (female)
    {
        result = result*0.742;
    }
    [self.skfMDRDresult setText:[NSString stringWithFormat:@"%f",result]];
    
    int stage = 0;
    
    if (result >= 90) {stage = 1;}
    else if (result >= 60) {stage = 2;}
    else if (result >= 30 ) {stage = 3;}
    else if (result >= 15) {stage = 4;}
    else {stage = 5;}
    
    switch (stage) {
        case 1:
            [self.hpbStageResult setText:@"I"];
            [self.hpbResultLabel setText:@"Характеристика:\nПризнаки повреждения почек с нормальной или повышенной СКФ\n\nТактика:\nНаблюдение у нефролога: диагностика и лечение основного заболевания, снижение риска развития сердечно-сосудистых осложнений"];
            break;
        case 2:
            [self.hpbStageResult setText:@"II"];
            [self.hpbResultLabel setText:@"Характеристика:\nПризнаки повреждения почек с начальным снижением\n\nТактика:\nОценка скорости прогрессирования ХБП, диагностика и лечение"];
            break;
        case 3:
            [self.hpbStageResult setText:@"III"];
            [self.hpbResultLabel setText:@"Характеристика:\nУмеренное снижение\n\nТактика:\nПрофилактика, выявление и лечение осложнений"];
            break;
        case 4:
            [self.hpbStageResult setText:@"IV"];
            [self.hpbResultLabel setText:@"Характеристика:\nВыраженное снижение СКФ\n\nТактика:\nПодготовка к заместительной терапии (выбор метода)"];
            break;
        case 5:
            [self.hpbStageResult setText:@"V"];
            [self.hpbResultLabel setText:@"Характеристика:\nТерминальная почечная недостаточность\n\nТактика:\nНачало заместительной почечной терапии"];
            break;
        default:
            break;
    }
    
    if (!self.fullRect)
    {
        CGRect rect = self.contentView.frame;
        rect.size.height += 180;
        self.contentView.frame = rect;
        self.fullRect = YES;
        [self viewDidLayoutSubviews];
    }
}

- (IBAction)clear:(UIButton *)sender {
    _age.text = @"";
    _xbp.text = @"";
    _height.text = @"";
    _weight.text = @"";
    _result.text = @"";
    self.skfTextField.text = @"";
    self.hpbStageTextField.text = @"";
    _creatininum.text = @"";
    _xbp.hidden = YES;
    _result.hidden = YES;
    _gender.selectedSegmentIndex = 0;
    _units.selectedSegmentIndex = 0;
    self.hpbResultLabel.text = @"";
    self.hpbStageResult.text = @"";
    self.skfMDRDresult.text = @"";
    
    if (self.fullRect)
    {
        CGRect rect = self.contentView.frame;
        rect.size.height -= 180;
        self.contentView.frame = rect;
        self.fullRect = NO;
        [self viewDidLayoutSubviews];

    }
    
}

- (BOOL)showAlert
{
    DHAlert *alert = [DHAlert new];
    NSArray *array = [[NSArray alloc] initWithObjects:_height.text, _weight.text, _creatininum.text, _xbp.text, _age.text, nil];
    [alert initWithViewController:self andArray:array];
    return alert.alertShown;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#define kOFFSET_FOR_KEYBOARD 40.0

-(void)keyboardWillShow:(NSNotification *)notification
{
    CGRect keyboardFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    self.keyboardHeight = keyboardFrame.size.height;
    CGRect rect = fullScreenView;
    rect.size.height -= (self.keyboardHeight);
    
    self.mainScroll.frame = rect;
    
}

-(void)keyboardWillHide
{
//    CGRect rect = self.mainScroll.frame;
//    
//    rect.size.height += (self.keyboardHeight);
    
    self.mainScroll.frame = fullScreenView;
    
}


//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
//    
//    CGRect rect = self.view.frame;
//    if (movedUp)
//    {
//        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
//        // 2. increase the size of the view so that the area behind the keyboard is covered up.
//        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
//        rect.size.height += kOFFSET_FOR_KEYBOARD;
//    }
//    else
//    {
//        // revert back to the normal state.
//        rect.origin.y += kOFFSET_FOR_KEYBOARD;
//        rect.size.height -= kOFFSET_FOR_KEYBOARD;
//    }
//    self.view.frame = rect;
//    
//    [UIView commitAnimations];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

@end
