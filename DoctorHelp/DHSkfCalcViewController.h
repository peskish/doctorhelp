//
//  DHSkfCalcViewController.h
//  DoctorHelp
//
//  Created by Aliona on 02/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"

@interface DHSkfCalcViewController : DHViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *creatininum;
@property (weak, nonatomic) IBOutlet UITextField *age;
@property (weak, nonatomic) IBOutlet UITextField *height;
@property (weak, nonatomic) IBOutlet UITextField *weight;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *units;
@property (weak, nonatomic) IBOutlet UILabel *result;
@property (weak, nonatomic) IBOutlet UILabel *xbp;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;
@property (weak, nonatomic) IBOutlet UITextField *skfMDRDresult;
@property (weak, nonatomic) IBOutlet UITextField *hpbStageResult;
@property (weak, nonatomic) IBOutlet UILabel *hpbResultLabel;

- (IBAction)unitSelect:(UISegmentedControl *)sender;

- (IBAction)calc:(UIButton *)sender;
- (IBAction)clear:(UIButton *)sender;

@end
