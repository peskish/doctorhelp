//
//  DHAboutViewController.m
//  DoctorHelp
//
//  Created by Aliona on 01/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHAboutViewController.h"

#import "GAI.h"
#import "GAIFields.h"
#import "GAIDictionaryBuilder.h"

@interface DHAboutViewController ()

@end

@implementation DHAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *html = @"<html><head><style type='text/css'>body{font-family:'Helvetica Neue Light', 'Helvetica Neue';font-size:17.0px;font-weight:bold;color:#494949; padding:10px;} a{color:#0b7bb5;}</style></head><body>Приложение создано командой крупнейшей в мире профессиональной сети русскоязычных врачей<br/><a href='http://www.doktornarabote.ru'>&laquo;Доктор на работе&raquo;</a>.<br/><br/>Мы регулярно добавляем новые калькуляторы. Напишите нам, какие предлагаете добавить вы. <br/><br/>Обратная связь: <a href='mailto:mobile@doktornarabote.ru'>mobile@doktornarabote.ru</a>.<br/><br/><center><img src='http://upload.wikimedia.org/wikipedia/ru/8/82/Логотип_Доктор_на_работе.png' style='width:275px;'></center></body></html>";
    [_webview loadHTMLString:html baseURL:nil];

    // Google Analytics
//    id tracker = [[GAI sharedInstance] defaultTracker];
//    [tracker set:kGAIScreenName
//           value:@"About screen"];
//    [tracker send:[[GAIDictionaryBuilder createAppView] build]];
    
    [[DHAnalyticsManager sharedManager]logEventPageViewWithName:@"About screen"];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"Loading URL :%@",request.URL.absoluteString);
    if([request.URL.absoluteString isEqualToString:@"http://www.doktornarabote.ru/"])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.doktornarabote.ru/"]];
        return NO;
    }
   
    if([request.URL.absoluteString isEqualToString:@"mailto:mobile@doktornarabote.ru"])
    {
        NSString *emailTitle = @"Email";
        // Email Content
        NSString *messageBody = @"";
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:@"mobile@doktornarabote.ru"];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        [mc setToRecipients:toRecipents];
        
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];

        return NO;
    }
   
    //return FALSE; //to stop loading
    return YES;
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
