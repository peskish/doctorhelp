//
//  DHBodyAreaCalcViewController.h
//  DoctorHelp
//
//  Created by Aliona on 01/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DHViewController.h"
@interface DHBodyAreaCalcViewController : DHViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *age;
@property (weak, nonatomic) IBOutlet UITextField *height;
@property (weak, nonatomic) IBOutlet UITextField *weight;
@property (weak, nonatomic) IBOutlet UILabel *result;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *calcButton;

- (IBAction)calc:(UIButton *)sender;
- (IBAction)clear:(UIButton *)sender;

@end
