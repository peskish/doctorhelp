//
//  Patient.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 01.06.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class NSManagedObject;

@interface Patient : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *basdai;
@end

@interface Patient (CoreDataGeneratedAccessors)

- (void)addBasdaiObject:(NSManagedObject *)value;
- (void)removeBasdaiObject:(NSManagedObject *)value;
- (void)addBasdai:(NSSet *)values;
- (void)removeBasdai:(NSSet *)values;

@end
