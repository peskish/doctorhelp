//
//  DHAlert.h
//  DoctorHelp
//
//  Created by Aliona on 08/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DHAlert : NSObject

@property BOOL alertShown;
- (void)initWithViewController:(UIViewController*)viewController andArray:(NSArray*)array;

@end
