//
//  UIColor+DHfromHex.h
//  DoctorHelp
//
//  Created by Dmitry Venikov on 16/10/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (DHfromHex)

+ (UIColor *)colorwithHexString:(NSString *)hexStr alpha:(CGFloat)alpha;

@end
