//
//  DHAlert.m
//  DoctorHelp
//
//  Created by Aliona on 08/09/14.
//  Copyright (c) 2014 Ptenster. All rights reserved.
//

#import "DHAlert.h"

@implementation DHAlert

- (void)initWithViewController:(UIViewController*)viewController andArray:(NSArray*)array
{
    UIAlertView *_alert = [[UIAlertView alloc] initWithTitle:@"Вы вводите неверные значения" message:@"Вводимые значения должны быть числами." delegate:viewController cancelButtonTitle:@"OK" otherButtonTitles:nil];
    if([self showAlert:array])
        [_alert show];
}

- (BOOL)showAlert:(NSArray*)array
{
    for(NSString *str in array) {
        NSError *error = NULL;
        NSRegularExpression *regex = [NSRegularExpression
                                      regularExpressionWithPattern:@"[0-9.,]+"
                                      options:NSRegularExpressionCaseInsensitive
                                      error:&error];
        NSRange range = NSMakeRange(0, str.length);
        NSArray *matches = [regex matchesInString:str options:NSMatchingProgress range:range];
        if (!matches.count) {
            _alertShown = YES;
            return YES;
        }
    }
    _alertShown = NO;
    return NO;
}

@end
