//
//  BASDAI.h
//  DoctorHelp
//
//  Created by Artem Peskishev on 02.06.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Patient;

@interface BASDAI : NSManagedObject

@property (nonatomic, retain) NSNumber * ball;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) Patient *patient;

@end
