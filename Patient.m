//
//  Patient.m
//  DoctorHelp
//
//  Created by Artem Peskishev on 01.06.15.
//  Copyright (c) 2015 Ptenster. All rights reserved.
//

#import "Patient.h"
#import "BASDAI.h"

@implementation Patient

@dynamic name;
@dynamic basdai;

@end
